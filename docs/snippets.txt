
// ***********************************************************************************************

// IDEA:
// Try out alternative to for structures, when a return value is needed:
//       let result = match IntoIterator::into_iter(iterator) {
//           mut iter => loop {
//               match iter.next() {
//                   None => break,
//                   Some(loop_variable) => { code(); },
//               };
//           },
//       };
//       result

// ***********************************************************************************************

// IDEA:
// Bind global object as proxy:
                                    // println!(" node_id: {}", global.id);
                                    // let node_proxy: pipewire::node::Node = registry_clone
                                    //     .bind(global)
                                    //     .expect("Failed to bind node proxy");
                                    // // println!("node_proxy: {:?}", node_proxy);
// Create listener on node_proxy to get mute status?:
// Or get data from a SPA control node???
// Mute is in params not in props???
// Need to put it in vec to detroy it on object_removed to keep it alive???
// type Spa:Pod:Object:Param:Props, id Spa:Enum:ParamId:Props (2)
// key Spa:Pod:Object:Param:Props:mute
                                    // let _node_listener = node_proxy
                                    //     .add_listener_local()
                                    //     .param(move |param1, param2, param3, param4| {
                                    //     // .info(move |info| {
                                    //         println!(
                                    //             "node params: {:?} | {:?} | {:?} | {:?}",
                                    //             param1,
                                    //             param2,
                                    //             param3,
                                    //             param4,
                                    //             // "node info: {:?}",
                                    //             // info,
                                    //             // info.props(),
                                    //             // info.state(),
                                    //         );
                                    //     })
                                    //     .register();
// store proxy and listener in HashMap
// destroy link listener in global_remove?

// ***********************************************************************************************

// For a new programming language, here is a very immature formatting idea:

put "hello world" into println!;

put (
    args_multi,
    Rc::clone(&option_channels_reversed_rc),
    Rc::clone(&option_map_string_rc),
) into handle_arguments_multi (
    ProgrammStatus
)

put program_status_start into match (
    ProgramStatus::Running => {
        put args_multi.len() into match {
            0 => ProgramStatus::Running,
            _ => ProgramStatus::Quit,
        }
    },
    ProgramStatus::Quit => ProgramStatus::Quit,
)
ProgrammStatus -> program_status_args;

put (
    option_channels_reversed_rc,
    option_map_string_rc,
    option_title_off_rc,
)
into main_options (
    MixerMapName -> mixer_map_name,
    bool -> option_channels_reversed,
    bool -> option_title_off,
    ProgramStatus -> program_status_options,
);

// Could be prettyfied with lots of '::', ':' and other symbols. Not that I will ever create a programming language, but why not talk about it.

// ***********************************************************************************************
