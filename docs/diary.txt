##### My motivation: #####

I had an Akai MIDImix for my DAW, but wanted to use it for PulseAudio.
So I wrote a couple of lines in a bash script, which got bigger and bigger over the months.
Then I installed PipeWire some day and wanted to learn more about its details. And I wanted to bring my script on another level.
But I also intended to learn Rust for quite some time. Why not do all in one?

There was sucess in learning the basics of Rust and success in learning some basics of PipeWire and success in having a much better midi mixer mapper. The beginning was really hard but the result after three months is acceptable to me. While doing this, some new goals emerged.
For example, when I look at what is described here on the [PipeWire MIDI page](https://docs.pipewire.org/page_midi.html). I completely failed in that. But there is still a lot of time. PipeWire is not at the end of its development. It is pretty young for its complexity. pw-midimix will grow with it (hopefully) bit by bit.

It could be better structured, as I didn't know, where this leads in the beginning. There are inconsistent naming shemes and sometimes I ignored it, when a function becomes confusingly long.
[Edit:] Lots of this has been fixed, but there is still a lot to do.

And I tried out some new formatting ideas. Maybe it is a little bit unorthodox and inconsequent.

I left many of the commented out debugging prints for other people to better understand, what is happening. And some of the old decisions to show, why I do it another way now.
[Edit:] [Version 1.4](https://gitlab.freedesktop.org/AdeptVeritatis/pw-midimix/-/commit/9f4162352b0587fc907d5fe2eddb240b873a494a) is the latest commit with all those commented out code examples. After it, some or most of them will be removed from time to time.

Overused some principles, which worked for me. Some of it may not be a typical Rust solution. Those solutions will come with more experience.

But I tried to keep it clean and avoid bad compromises. Types are (mostly) the types, which are needed.

Still have to learn A LOT.

But i like the program and am satisfied with it. I use it everyday.



##### Resting time: ######

Breaking up lots of barriers and digging through all the rubble was pretty exhausting. After the three months, I needed some rest.

First there was the hot weather, where I couldn't think straight. Just too hot. Than I was on vacation, than hot weather again. And after it, I felt a little bit daunted, discouraged and disheartened. That is typical for a certain kind of programmers. But luckily, it didn't go on like e.g. Cube World, because I took my break intentionally. Since day one of resting, I said to me, that I will continue, when I am ready for it.

It needed a lot of time to let sort everything out in my brain by itself. Now I have capacity to start again. And it was pretty easy and felt familiar to continue.
So if you feel, you are stuck somehow, think about taking a break.

[Edit:] Taking another break. I am trying out new ideas in a new project. Doesn't mean, I am no longer interested in pw-midimix. It is still pretty grippling as it has a good complexity. Not too simple but still manageable. A nice object to try out things I learned.
It feels a little bit, like a prototype is finished now and the real work only started.



##### Formatting: #####

I tried out a lot of different formatting ideas and am still not sure about a final setup. But I am pretty sure, that I want to avoid long lines and better deal with nested structures. When those get too complex, it is a warning sign, that the code needs to be split up into more functions. I will keep most of my formatting and use it as a tool to clarify the structure and to clean up my code. Maybe I will get used to a different style later, but for now I will stay with it.
[Edit:] Used 'cargo fmt' now amd will stick to this formatting. ... with a little touch of my own ideas. :-P (But within the rules.)

My formatting starts to collide with the structure of Rust a little bit. My eyes keep jumping from top to bottom, up, down, up, down ... Results are piped back to something in the beginning of a logical block. It is manageable but could be much better.
