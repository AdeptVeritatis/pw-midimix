<div align="center">
    <h1>pw-midimix</h1>
<h4> A mapper for hardware MIDI mixer controlling PipeWire audio streams </h4>

</div>


# Description
`pw-midimix` detects running PipeWire `audio streams` and assigns each of them to a `channel` of a `MIDI mixer`.

It uses the input from the `mixer` to change `volume` of the individual `audio streams`, `mute` them or switch the `output sink`.

It is written in `Rust`.

***

For me, it is WIP, as this is my first Rust project. I used some `pw-tools` and the `midir` crate to shorten things. Besides my lack of experience, the PipeWire Rust API is not complete at the moment.

Nonetheless, `pw-midimix` is working as intended and is pretty responsive. And awesome!

# Requirements
PipeWire instance running and PipeWire tools installed (`pw-cli` and `pw-dump`).

ALSA MIDI sequencer running.(?) This should be on by default.

Midi mixer. It is optimized for `Akai MIDImix`. But other devices / software work, too.

# Installation
A compiled binary is available in [./target/release/](https://gitlab.freedesktop.org/AdeptVeritatis/pw-midimix/-/blob/main/target/release/pw-midimix).

Don't forget to make it executable, if necessary.

## Building from source
To build `pw-midimix`, you will need to have `Rust` installed. The recommended way to install `Rust` is from the [official download page](https://www.rust-lang.org/tools/install), using `rustup`.

Clone the main branch.

```
git clone https://gitlab.freedesktop.org/AdeptVeritatis/pw-midimix.git
cd pw-midimix
cargo build --release
./target/release/pw-midimix
```

# Usage
## Getting started:
Plug in your `(USB) MIDI mixer`.

Start `pw-midimix`. It gives you extra information in the console.

`pw-midimix` takes the first `Akai MIDImix`, it can find, and connects to it.

If you use a different device, use the select option `-s` (--select).

Use `pw-midimix -h` for additional arguments.

***

Every detected audio stream will be assigned to a channel.

The corresponding `REC ARM LED` will light up red.

Use the `slider` to change the volume.


### MUTE buttons:
Use the `MUTE` button to mute and unmute a channel. The `MUTE LED` will light up yellow, depending on the state.

### SOLO + MUTE buttons:
Hold the `SOLO` button and press the `MUTE` button of an active channel to switch the audio stream to a different `sink`.

You can try it out with a running `EasyEffects`, which is an independent sink, if you don't have multiple outputs (perhaps a TV set connected over HDMI or bluetooth head-/earphones).

Watch, what is happening in one of the graph managers e.g. `Helvum` or `qpwgraph`.

### REC ARM buttons:
`Press` active channel `once` to print sink and mute state of the channel.

`Press` active channel `twice` (within 2 seconds) to print running streams to the console.

`Press` empty channel `once` to print running streams, too.

#### combination based (default):
`Hold` active channel and `press` an empty channel to move the channel.

`Hold` active channel and `press` another active channel to exchange places.

#### timer based:
Use `-a` (--active-timer) argument.

`Press` active channel `once` and `press` an empty channel (within 2 seconds) to move the channel.

`Press` active channel `once` and `press` another active channel (within 2 seconds) to exchange places.

### SEND ALL button:
Sends the values of all `sliders`(!) at once and sets the volumes of all channels to the preset values with that.

Comes in handy, if you moved around streams or changed the volume in an application and don't want to touch the sliders.

### BANK LEFT & BANK RIGHT buttons:
Despite the sink nodes, where you switch the output to, there are devices, too. A device has the main volume as one of the parameters.

You can change the main volume of your sound card with the `MASTER` slider.

This effects all channels, which output to that device. But the relative volumes of the streams don't change.\
You can see the differences in e.g. your volume mixer applet.

With the `BANK` buttons, you can switch through your different output devices.

## Mapping:
With the `-r` (--reversed) argument, you can change the direction, from which the channels will be filled on the mixer.

Default is from left to right.\
But when your mixer is on the left side of your table, it could be more comfortable to fill the channels from right to left.

With the `-m` (--map <name>) argument, you can change the mapping corresponding to your device.

At the moment, there are only three options.

### '-m midimix'
[Akai MIDImix](https://www.akaipro.com/midimix) (USB MIDI mixer)

Default mapping.

### '-m apc-key-25'
[Akai APC Key 25](https://www.akaipro.com/apc-key-25) (USB MIDI keyboard).

The three top rows of the button matrix are used for `MUTE` (1st), `REC ARM` (2nd) and `SOLO` + `MUTE` (3rd) functionality.

The knobs in the top right corner are used for the channel volumes.

The shift button is the `SOLO` button. But without function, as it is already handled with the 3rd line of the button matrix.

The arrow up & down on the left side are the `BANK` buttons.

No main volume available.

### '-m vmpk'
[VMPK](https://vmpk.sourceforge.io/) (software MIDI keyboard)

Your distro should have a "`vmpk`" package.

To get a grip of some of the functionality, if you don't own any hardware or other virtual devices.\
In addition, it is meant to be a showcase for the different connection possibilities.

There is only one knob (value / modulation) for the main volume. Channel volumes can not be changed.

The piano keys are the mixer buttons.

 | notes | | function |
 |--- |--- |---
 | A0 & B0 | | BANK RIGHT & BANK LEFT => switch device |
 | A#0 | | SOLO button => no function |
 | C# | + octaves | MUTE |
 | D | + octaves | REC ARM => active channels |
 | D# | + octaves | SOLO + MUTE => switch sink |

>If `vmpk` doesn't create a MIDI device, change the `Edit` -> `MIDI Connection` settings.
>
>Check `Enable MIDI input` and uncheck `Enable MIDI Thru on MIDI Output`.
>
>For `MIDI IN Driver` and `MIDI OUT Driver` select `ALSA`.

## Output:
The `-g` (--greeter-off) argument disables the active channel and mute LEDs to flash on startup.

The `-l` (--leds-off) argument disables all LEDs completely.

The `-t` (--title-off) argument disables media titles. This is meant for privacy reasons, as there can build up quite a long list of previously played media in the console, when using certain media players.

# Known shortcomings
The state of the `MUTE` button doesn't change, when a stream is (un)muted in an application itself or via some other software (taskbar, volume applet, mixer, ...).\
This is one of the features, I plan to add. I just didn't figure out a way to receive the SPA messages.\
Until then, you can press `REC ARM` button of an active channel to update the mute state.\
Or you can press the `MUTE` button and it will change the mute state of the stream to the set value, regardless of the actual mute state of the stream.

Links are not persistent. When switching the output device of a channel with the `SOLO` + `MUTE` buttons, the old links get deleted and a new set of links will be created. But when you run a playlist in e.g. `vlc player`, the old links get recreated, when the next playlist item starts.

There is no automated reconnect or monitoring of the links from and to the MIDI mixer.\
But you can always set your links easily with any of the graph managers (`helvum`, `qpwgraph`, etc.).

Midi is not handled the [PipeWire way](https://docs.pipewire.org/page_midi.html). I had to use midir to receive and send events.\
This will be changed as soon as the PipeWire Rust API allows it in a safe and easy way. Or I understand, how to do it.

Commands are spawned to run `pw-cli` and `pw-dump` several times. But only u32 and f32 types are passed without an obvious possibility to foister malicious strings.

Error handling got better over time but may still be completely changed later again.\
Starting with Rust was motivated by having to learn exactly all this stuff.

# Roadmap
Add more mappings of midi mixers.

Error handling, bug fixes.

Maybe localization.

Maybe an option to silence the whole output.

Changing the volume should be handled by SPA messages or something like this.

Receiving MIDI events should be done with a "simple filter".

To drop midir, a different kind of sending MIDI events (over the control channel?) needs to be implemented.

Get rid of the `pw-utils`.

I don't need the 3 x 8 knobs at the moment and don't intend to use them. They could be used for a 8 or 24-channel equalizer or something else. It shouldn't be that hard to add extra functionality. But I don't plan it.

No gui planned. No mapping on the fly via menu. No config file. But maybe you have a nice idea or need any of it really urgent.

# Support and contributing
As I am new to publishing my software, I am not familiar with all the habits.

But I am trying to be open for your remarks and wishes. And the issues tracker.

Matrix room: [#pw-midimix:tchncs.de](https://matrix.to/#/#pw-midimix:tchncs.de)

You can contact me on Mastodon [@AdeptVeritatis@social.tchncs.de](https://social.tchncs.de/@AdeptVeritatis).\
Or just open an issue. I'm fine with that, too.

# Naming
If you have any problem with any names or terms I used for whatever reason, please contact me.

I tried to find a suitable name for the program and I wanted to let room for others. So I kept it close to the hardware I use. Different names could possibly be pw-mixer or pw-midimixer or pw-midimapper.

## Device requests
If you have a device, which is not working, don't hesitate to ask. Sometimes, the midi notes and controller numbers are documented online.

To find out the midi notes for the buttons and the controller numbers for the sliders yourself, you can use
e.g. `pw-mididump` from the pipewire tools. But it converts the numbers to keys. Not a problem for me, but maybe you want to change the code yourself.

The output of the tool `aseqdump` shows the same numbers as used in `pw-midimix`. It is part of the `alsa-utils` package.\
And remember, you can always connect different MIDI nodes with a graph manager, too.

## Redistribution
This is free and open source software. As long as you adhere to the license, you can do whatever you want with it.

You don't like the name? Clone it and rename it!

You don't like, how I handle the project? Clone it and continue as you like!

You like the idea but want to implement it differently? Copy the idea!

You want to see a Flatpak repository of this? Do it yourself!

You want to sell this app? Stick to the license (especially the redistribution of modifications)!\
Maybe you want to hire someone to take care of the code.

# My motivation
Please read [./docs/diary.txt](https://gitlab.freedesktop.org/AdeptVeritatis/pw-midimix/-/blob/main/docs/diary.txt).

I moved everything personal there.

# Acknowledgment
Thanks to:

[PipeWire](https://pipewire.org/)

[pipewire_rs](https://gitlab.freedesktop.org/pipewire/pipewire-rs) and the [docs](https://pipewire.pages.freedesktop.org/pipewire-rs/pipewire/)

[midir](https://github.com/Boddlnagg/midir) and the [docs](https://docs.rs/midir/latest/midir/)

[serde_json](https://github.com/serde-rs/json) and the [docs](https://docs.rs/serde_json/latest/serde_json/)

[pw-viz](https://github.com/Ax9D/pw-viz) for your code example. It helped me very much in the beginning and for this documentation.

[The Rust Programming Language](https://doc.rust-lang.org/book/title-page.html) and the [docs](https://doc.rust-lang.org/stable/std/index.html)

# Project status
Still motivated.

But commit frequency will be lower. I need to read a lot more code from different people before I systematically rework my coding style here.
