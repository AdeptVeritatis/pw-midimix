pub mod help;
pub mod localization;
pub mod options;
pub mod print;

use crate::{
    app::{
        options::AppOptions,
        print::PrintTab,
},
    hardware::{
        midi::{
            MessageClass,
            MidiMessage,
        },
        MixerMapName,
    },
    impls::pipewire_impl::{
        mainloop::mainloop_audio::MainLoopAudio,
        PipeWireChannels,
    },
};
use midir::MidiOutputConnection;
// use pipewire as pw;
use pipewire::channel::Sender;
use std::{
    env,
    io::stdin,
    thread::{
        Builder,
        JoinHandle,
    },
};

// ----------------------------------------------------------------------------

// AppStatus:
#[derive(Debug, Clone)]
pub enum AppStatus {
    Running,
    Quit,
}

// ----------------------------------------------------------------------------

pub struct App {
    pub mixer_map_name: MixerMapName,
    pub options: AppOptions,
    pub print_tab: PrintTab,
    pub state: AppStatus,
}

impl Default for App {
    fn default() -> Self {
        // Print greeter:
        println!(
            "pw-midimix v{}",
            env!("CARGO_PKG_VERSION"),
        );
        println!();

        Self {
            mixer_map_name: MixerMapName::MIDImix,
            options: AppOptions::default(),
            print_tab: PrintTab::default(),
            state: AppStatus::Running,
        }
    }
}

impl App {
    pub fn run(
        &mut self,
        midir_out: MidiOutputConnection,
        pipewire_channels: PipeWireChannels,
    ) {
        // Start main threads and loops:
        // Print hint how to quit before mainloop starts running:
        println!("\n!! Enter `q` to quit ...\n");

        // Set up pipewire thread:
        let app_options: AppOptions = self
            .options
            .clone();
        let mixer_map_name: MixerMapName = self
            .mixer_map_name;
        let print_tab: PrintTab = self
            .print_tab
            .clone();

        let pw_thread_handle: JoinHandle<()> = match
            Builder::new()
                .name(
                    String::from("Pipewire")
                )
                .spawn(
                    move || {
                        MainLoopAudio::new(
                            midir_out,
                        )
                        .run(
                            &app_options,
                            mixer_map_name,
                            print_tab,
                            pipewire_channels
                                .receiver,
                        );
                    }
                )
        {
            Err(error) => panic!("\n!! problem creating pipewire thread:\n   error: {error:?}"),
            Ok(handle) => handle,
        };

        // Loop for user input until quit:
        loop {
            match
                stdin()
                    .lines()
                    .next()
            {
                None => println!("!! problem with empty input."),
                Some(input) => {
                    // println!("input lines: {:?}", input);
                    match input {
                        Err(error) => println!("!! problem reading input:\n   error: {error:?}"),
                        Ok(string) => {
                            self
                                .quit(
                                    pipewire_channels
                                        .sender
                                        .clone(),
                                    string,
                                );
                            match
                                self
                                    .state
                            {
                                AppStatus::Running => (),
                                AppStatus::Quit => {
                                    break;
                                },
                            };
                        },
                    };
                },
            };
        };

        // Check, if thread is finished:
        match
            pw_thread_handle
                .join()
        {
            Err(error) => println!("\n!! problem joining threads:\n   error: {error:?}"),
            Ok(_) => (),
        };
    }

    fn quit(
        &mut self,
        pw_sender: Sender<MidiMessage>,
        string: String,
    ) {
        match
            string
                .trim()
        {
            "q" | "Q" | "\"q\"" | "'q'" | "`q`" | "quit" => {
                let midi_message: MidiMessage = MidiMessage {
                    channel: 0,
                    class: MessageClass::Quit,
                    stamp: 0,
                    value: 0,
                };
                match
                    pw_sender
                        .send(
                            midi_message
                        )
                {
                    Err(error) => panic!("\n!! Problem sending midi message:\n   error: {error:?}"),
                    Ok(_) => (),
                };
                self.state = AppStatus::Quit;
            },
            _ => println!("!! Enter `q` to quit ..."),
        };
    }
}

impl App {
    pub fn set_options(
        &mut self,
    ) {
        // Get arguments:
        let mut args: Vec<String> = env::args()
            .collect();
        let _args_self: String = args
            .remove(
                0
            );

        let mut app_options: AppOptions = self
            .options
            .clone();
        // Single argument handling:
        args = app_options
            .handle_arguments_single(
                self,
                args,
            );
        // Two-piece and multi argument handling:
        match
            self
                .state
        {
            AppStatus::Quit => (),
            AppStatus::Running => {
                app_options
                    .handle_arguments_multi(
                        self,
                        args,
                    );

                // Print options to console:
                app_options
                    .show_options(
                        self,
                    );
            },
        };
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
