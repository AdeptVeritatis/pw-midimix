// Use traits for different I/O types?

use crate::{
    app::{
        localization::{
            AUTOSELECTING,
            MIDI_FEEDBACK_HINT,
            MIDI_FEEDBACK_WARNING,
            SELECT_INPUT,
            SELECT_OUTPUT,
        },
        print::tab_filler,
        App,
        AppStatus,
    },
    declarations::MIDIR_MIDIMIX_NAME,
    impls::{
        midir_impl::{
            connect_midi_port_in,
            connect_midi_port_out,
            create_midi_port_in,
            create_midi_port_out,
            list_midi_ports_in,
            list_midi_ports_out,
            select_midi_port,
        },
        pipewire_impl::{
            mainloop::mainloop_midi::create_midi_mainloop,
            PipeWireChannels,
        },
    },
};
use midir::{
    MidiInput,
    MidiInputConnection,
    MidiInputPort,
    MidiOutput,
    MidiOutputConnection,
    MidiOutputPort,
};
use pipewire::channel::Sender;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// MessageClass from midir sender to pipewire thread receiver:
#[derive(Debug, Clone)]
pub enum MessageClass {
    Button,
    Other,
    Quit,
    Release,
    Slider,
}

// ----------------------------------------------------------------------------

// MidiMessage:
#[derive(Debug, Clone)]
pub struct MidiMessage {
    pub channel: u8,
    pub class: MessageClass,
    pub stamp: u64,
    pub value: u8,
}

// ----------------------------------------------------------------------------

// // MidiPorts:
// #[derive(Debug, Clone)]
// pub struct MidiPorts {
//     pub device_in: u32,
//     pub device_out: u32,
//     pub midir_in: u32,
//     pub midir_out: u32,
// }

// ----------------------------------------------------------------------------

// #[derive(Clone)]
pub struct MidiDevice {
    pub input: MidiInputConnection<Sender<MidiMessage>>,
    pub output: MidiOutputConnection,
}

impl MidiDevice {
    pub fn new( // connect() ???
        app: &mut App,
        pipewire_channels: &PipeWireChannels,
    ) -> Option<Self> {
        // Create MIDI ports:
        let midir_input: MidiInput = create_midi_port_in();
        let midir_output: MidiOutput = create_midi_port_out();

        // Testing to drop midir in the future.
        // Create mainloop for MIDI handling:
        match create_midi_mainloop() {
            Ok(_) => (),
            Err(error) => panic!("\n!! Problem creating mainloop:\n error: {:?}", error,),
        }

        // List MIDI input ports:
        // println!();
        let (midir_input, midir_in_ports): (MidiInput, Vec<MidiInputPort>) = list_midi_ports_in(
            midir_input,
            app
                .print_tab
                .clone()
        );
        // Ask to select input:
        let select_input: Rc<RefCell<usize>> = Rc::new(RefCell::new(0));
        match
            app
                .options
                .ports_select
        {
            true => {
                print!("?? {SELECT_INPUT} [1]: ");
                let (input_port, program_status): (usize, AppStatus) =
                    select_midi_port(midir_in_ports.len());
                select_input.replace(input_port);
                app.state = program_status;
            }
            false => {
                // Or select first available MIDImix:
                for (num, port) in midir_in_ports.iter().enumerate() {
                    let port_name: String = match midir_input.port_name(port) {
                        Ok(name) => name,
                        Err(error) => {
                            panic!("\n!! Problem getting port in name:\n error: {:?}", error,)
                        }
                    };
                    if port_name.contains(MIDIR_MIDIMIX_NAME) {
                        println!(
                            "<> {AUTOSELECTING} {tab}{port_name}",
                            tab = tab_filler(AUTOSELECTING),
                        );
                        select_input.replace(num);
                        break;
                    };
                };
            }
        };

        match
            app
                .state
                .clone()
        {
            AppStatus::Quit => None,
            AppStatus::Running => {
                // List MIDI output ports:
                println!();
                let (midir_output, midir_out_ports): (MidiOutput, Vec<MidiOutputPort>) = list_midi_ports_out(
                    midir_output,
                    app
                        .print_tab
                        .clone()
                );
                // Ask to select output:
                let select_output: Rc<RefCell<usize>> = Rc::new(RefCell::new(0));
                match
                    app
                        .options
                        .ports_select
                {
                    true => {
                        print!("?? {SELECT_OUTPUT} [1]: ");
                        let (output_port, program_status): (usize, AppStatus) =
                            select_midi_port(midir_out_ports.len());
                        select_output.replace(output_port);
                        app.state = program_status;
                    },
                    false => {
                        // Or select first available MIDImix:
                        for (num, port) in midir_out_ports.iter().enumerate() {
                            let port_name: String = match midir_output.port_name(port) {
                                Ok(name) => name,
                                Err(error) => {
                                    panic!("\n!! Problem getting port out name:\n error: {:?}", error,)
                                }
                            };
                            if port_name.contains(MIDIR_MIDIMIX_NAME) {
                                println!(
                                    "<> {AUTOSELECTING} {tab}{port_name}",
                                    tab = tab_filler(AUTOSELECTING),
                                );
                                select_output.replace(num);
                                break;
                            }
                        }
                        // Check for feedbacks:
                        match select_input.borrow().to_owned() {
                            0 => {
                                match select_output.borrow().to_owned() {
                                    0 => {
                                        println!();
                                        println!("!! {MIDI_FEEDBACK_WARNING}");
                                        println!("!! {MIDI_FEEDBACK_HINT}");
                                        println!();
                                        app.state = AppStatus::Quit; //close sender/receiver?
                                    },
                                    _ => (),
                                };
                            },
                            _ => (),
                        };
                    },
                };

                // Connect MIDI ports:
                // use new struct MidiPorts!!!
                match
                    app
                        .state
                        .clone()
                {
                    AppStatus::Quit => None,
                    AppStatus::Running => {
                        let input: MidiInputConnection<Sender<MidiMessage>> = connect_midi_port_in(
                            midir_input,
                            midir_in_ports,
                            app
                                .print_tab
                                .clone(),
                            pipewire_channels
                                .sender
                                .clone(),
                            select_input.borrow().to_owned(),
                        );
                        let output: MidiOutputConnection = connect_midi_port_out(
                            midir_output,
                            midir_out_ports,
                            app
                                .print_tab
                                .clone(),
                            select_output
                                .borrow()
                                .to_owned(),
                        );
                        Some(
                            Self {
                                input,
                                output,
                            }
                        )
                    },
                }
            },
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
