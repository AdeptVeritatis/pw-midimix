
use crate::{
    app::{
        localization::{
            CHANNEL_VOLUME,
            MAIN_VOLUME,
        },
        print::{
            PrintState,
            PrintTab,
            tab_filler,
        },
    },
    declarations::OFFSET_SLIDER,
    hardware::{
        mapping::Mapping,
        midi::MidiMessage,
    },
    impls::pipewire_impl::{
        mainloop::mainloop_audio::MainLoopAudio,
        objects::{
            device::AudioDevice,
            node::stream::AudioStream,
        },
    },
};
use std::{
    cell::{RefCell, RefMut},
    collections::HashMap,
    io::{stdout, Write},
    process::{Command, Stdio},
    rc::Rc,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn slider_moved(
    mainloop_audio: MainLoopAudio,
    // audio_devices_clone: Rc<RefCell<Vec<AudioDevice>>>,
    // audio_streams: Rc<RefCell<HashMap<u8, StreamNode>>>,
    // channel_maps: ChannelMaps,
    mapping: Mapping,
    message: MidiMessage,
    // main_num: Rc<RefCell<usize>>,
    // next_line_rc: Rc<RefCell<PrintState>>,
    print_tab: PrintTab,
    // slider_master: u8,
) {
    let event_channel: u8 = message.channel;
    let event_stamp: u64 = message.stamp;
    let event_value: u8 = message.value;
    // println!("Slider moved");
    // println!(" stamp    - {:?}", event_stamp);
    // println!(" channel  - {:?}", event_channel);
    // println!(" value    - {:?}", event_value);

    let mut audio_streams_borrow: RefMut<HashMap<u8, AudioStream>> = mainloop_audio.audio_streams.borrow_mut();
    // Master volume:
    if event_channel == mapping.mixer_map.slider_master {
        let mut audio_devices_main: RefMut<Vec<AudioDevice>> = mainloop_audio.audio_devices.borrow_mut();
        let index_main: usize = mainloop_audio.audio_device_main.borrow().to_owned();
        let main_device_check: &AudioDevice = match audio_devices_main.iter().nth(index_main) {
            // better selection method?
            Some(device) => device,
            None => {
                println!(
                    "\n!! problem selecting audio device:\n   main: {:?}\n   audio_devices: {:?}",
                    index_main, audio_devices_main,
                );
                // select first? last? instead of panicking? solve it in listener_remove?
                match audio_devices_main.iter().last() {
                    Some(device) => device,
                    None => panic!("!! no audio device available"),
                }
            }
        };
        let check_stamp: u64 = main_device_check.stamp;
        // Check if offset is passed and always care about min or max volume:
        if event_stamp > check_stamp + OFFSET_SLIDER
            || event_value.to_string() == "0"
            || event_value.to_string() == "127"
        {
            // Remove device now, add it back later:
            let main_device: AudioDevice = audio_devices_main.remove(index_main);
            // Get main device values:
            let param_device: u32 = main_device.param_device;
            let param_index: u32 = main_device.param_index;
            let device_name: String = main_device.get_name().to_owned();
            let device: u32 = main_device.device;
            // println!("-- main node:      {}", main_node); // just for debugging
            // Parse volume:
            let volume_value: f32 = match event_value.to_string().parse::<f32>() {
                Ok(value) => value,
                Err(error) => panic!(
                    "\n!! Problem parsing number:\n event_value: {:?}\n error: {:?}",
                    event_value, error,
                ),
            };
            // Calculate volume to set:
            let volume_set: f32 = (volume_value / 127.0).powi(3);
            // Set volume:
            set_volume_main(
                device,
                device_name.clone(),
                event_value,
                mainloop_audio
                    .next_line,
                param_device,
                param_index,
                print_tab,
                volume_set,
                volume_value,
            );
            // Add device back again:
            let main_device_new: AudioDevice = AudioDevice {
                device,
                name: device_name,
                param_device,
                param_index,
                stamp: event_stamp,
            };
            audio_devices_main.insert(index_main, main_device_new);
        }
    // Channel volume:
    } else if audio_streams_borrow.contains_key(&event_channel) {
        let audio_stream: &AudioStream = match audio_streams_borrow.get(&event_channel) {
            Some(channel) => channel,
            None => panic!(
                "\n!! Problem getting audio stream:\n event_channel: {:?}\n audio_streams: {:?}",
                event_channel, audio_streams_borrow,
            ),
        };
        let check_stamp: u64 = audio_stream.stamp;
        // Check if offset is passed and always care about min or max volume:
        if event_stamp > check_stamp + OFFSET_SLIDER
            || event_value.to_string() == "0"
            || event_value.to_string() == "127"
        {
            // Get channel values:
            let id_node: u32 = audio_stream.id_node;
            // Parse volume:
            let volume_value: f32 = match event_value.to_string().parse::<f32>() {
                Ok(value) => value,
                Err(error) => panic!(
                    "\n!! Problem parsing number:\n event_value: {:?}\n error: {:?}",
                    event_value, error,
                ),
            };
            // Calculate volume to set:
            let volume_set: f32 = (volume_value / 127.0).powi(3);
            // Get channel from id:
            let map_channel_to_num: HashMap<u8, u8> = mapping.channel_maps.sliders_to_buttons.num;
            let channel: u8 = match map_channel_to_num.get(&event_channel) {
                Some(num) => num.to_owned(),
                None => 0,
            };
            // Set channel volume:
            set_volume_channel(
                channel,
                id_node,
                event_value,
                mainloop_audio
                    .next_line,
                print_tab,
                volume_set,
                volume_value,
            );
            // Change to streams:
            let audio_stream_new: AudioStream = AudioStream { // use mut !!!!!!!!!!!!!!!!!!!!!!
                id_node,
                mute: audio_stream.mute,
                name: audio_stream.clone().name,
                port_position: audio_stream.clone().port_position, // .clone() necessary???
                ports: audio_stream.clone().ports,
                sink_name: audio_stream.clone().sink_name,
                stamp: event_stamp,
            };
            audio_streams_borrow.insert(event_channel, audio_stream_new);
        }
    }
}

fn set_volume_channel(
    channel: u8,
    event_node: u32,
    event_value: u8,
    next_line_rc: Rc<RefCell<PrintState>>,
    print_tab: PrintTab,
    volume_set: f32,
    volume_value: f32,
) {
    // use 'save: true' as argument???
    // let _pw_channel_volume = Command::new("pw-cli")
    match Command::new("pw-cli")
        .arg("set-param")
        .arg(&event_node.to_string())
        .arg("Props")
        .arg("{")
        .arg("channelVolumes:")
        .arg("[")
        .arg(&volume_set.to_string())
        .arg(",")
        .arg(&volume_set.to_string())
        .arg("]}")
// Use piped stdout to check result???
        .stdout(Stdio::piped())
        .spawn() {
            Ok(mut process) => match process.wait() {
                Ok(_) => {
                    let next_line: PrintState = next_line_rc.borrow().to_owned();
                    if next_line == PrintState::Main {
                        println!();
                    };
                    if next_line != PrintState::Channel {
                        println!("<> {CHANNEL_VOLUME}:");
                        next_line_rc.replace(PrintState::Channel);
                    };
                    print!("\r{tab_pre}{channel}{tab_suf}{event_value} / {volume:.0}%    \r",
                        tab_pre = print_tab.prefix_str,
                        tab_suf = print_tab.suffix_str,
                        volume = (volume_value / 127.0 * 100.0).round(),
                    );
                    match stdout().flush() {
                        Ok(_) => (),
                        Err(error) => {
                            println!(
                                "\n!! Problem flushing output:\n error: {:?}\n",
                                error,
                            )
                        },
                    };
                },
                Err(error) => println!(
                    "\n!! Problem finishing command:\n error: {:?}",
                    error,
                ),
            },
            Err(error) => println!(
                "\n!! Problem sending channel volume command:\n node: {:?}\n volume: {:?}\n error: {:?}",
                event_node,
                volume_set,
                error,
            ),
        }
    // .spawn().expect("error: sending command")
    // .wait().expect("error: command not finished");
}

fn set_volume_main(
    device: u32,
    device_name: String,
    event_value: u8,
    next_line_rc: Rc<RefCell<PrintState>>,
    param_device: u32,
    param_index: u32,
    print_tab: PrintTab,
    volume_set: f32,
    volume_value: f32,
) {
    // use 'save: true' as argument???
    match Command::new("pw-cli")
        .arg("set-param")
        .arg(&device.to_string())
        .arg("Route")
        .arg("{")
        .arg("index:")
        .arg(&param_index.to_string())
        .arg(",")
        .arg("device:")
        .arg(&param_device.to_string())
        .arg(",")
        .arg("props:")
        .arg("{")
        .arg("channelVolumes:")
        .arg("[")
        .arg(&volume_set.to_string())
        .arg(",")
        .arg(&volume_set.to_string())
        .arg("]}}")
        // Use piped stdout to check result???
        .stdout(Stdio::piped())
        .spawn() {
            Ok(mut process) => match process.wait() {
                Ok(_) => {
                    let next_line: PrintState = next_line_rc.borrow().to_owned();
                    if next_line == PrintState::Channel {
                        println!();
                    };
                    // print!("\r<> {MAIN_VOLUME}:{tab}[ {device_name} ] {event_value} / {volume:.0}%    \r",
                    //     tab = tab_filler(MAIN_VOLUME),
                    //     volume = (volume_value / 127.0 * 100.0).round(),
                    // );
                    if next_line != PrintState::Main {
                        println!(
                            "<> {MAIN_VOLUME}:{tab}( {device_name} )",
                            tab = tab_filler(MAIN_VOLUME),
                        );
                        next_line_rc.replace(PrintState::Main);
                    };
                    print!("\r{tab}{event_value} / {volume:.0}%    \r",
                        tab = print_tab.full_str,
                        volume = (volume_value / 127.0 * 100.0).round(),
                    );
                    match stdout().flush() {
                        Ok(_) => (),
                        Err(error) => {
                            println!(
                                "\n!! Problem flushing output:\n error: {:?}\n",
                                error,
                            )
                        },
                    };
                    next_line_rc.replace(PrintState::Main);
                },
                Err(error) => println!(
                    "\n!! Problem finishing command:\n error: {:?}",
                    error,
                ),
            },
            Err(error) => println!(
                "\n!! Problem sending main volume command:\n node: {:?}\n index: {:?}\n device: {:?}\n volume: {:?}\n error: {:?}",
                device,
                param_index,
                param_device,
                volume_set,
                error,
            ),
        }
}
