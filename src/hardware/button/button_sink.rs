
use crate::{
    app::{
        localization::{
            SINK_CONNECTED,
            SINK_SWITCHED,
        },
        print::{
            PrintState,
            // PrintTab,
            next_line,
            tab_filler,
        },
    },
    hardware::mapping::ChannelMaps,
    impls::pipewire_impl::objects::{
        link::{
            LinkSide,
            create_link_audio,
            switch_link_audio,
        },
        node::{
            sink::AudioSink,
            stream::AudioStream,
        },
        port::port_out::StreamPort,
    },
};
use pipewire::{
    registry::Registry,
    core::Core,
};
use std::{
    cell::{
        Ref,
        RefCell,
    },
    collections::HashMap,
    iter::Enumerate,
    rc::Rc,
    slice::Iter,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn sink_button(
    audio_sinks: Rc<RefCell<Vec<AudioSink>>>,
    audio_streams: Rc<RefCell<HashMap<u8, AudioStream>>>,
    channel_maps: ChannelMaps,
    core: &Rc<Core>,
    channel: u8,
    next_line_rc: Rc<RefCell<PrintState>>,
    registry: Rc<Registry>,
) {
    // Change audio sink:
    // println!(" audio_sinks: {:?}", audio_sinks.borrow().to_owned());
    // Get stream source node_out from AudioStreams<channel, StreamNode>:
    let audio_streams_borrow: Ref<HashMap<u8, AudioStream>> = audio_streams
        .borrow();
    // Check, if channel exists in audio_streams:
    // let stream_node: StreamNode =
    match
        audio_streams_borrow
            .contains_key(
                &channel
            )
    {
        false => println!("!! Problem getting StreamNode."),
        true => {
            // Has key, get StreamNode from it:
            match audio_streams_borrow.get(&channel) {
                None => println!("\n!! Problem getting audio stream:\n event_channel: {channel:?}\n audio_streams_borrow: {audio_streams_borrow:?}\n"),
                Some(stream_node) => {
                    // stream_node.to_owned()

                    if
                        cfg!(debug_assertions)
                    {
                        dbg!(&stream_node);
                        dbg!(&audio_sinks.borrow());
                    };
                    // Get ports from StreamNode:
                    let ports: HashMap<LinkSide, StreamPort> = stream_node
                        .ports
                        .to_owned();
                    // Get StreamPort from stream_node.ports:
                    for
                        (
                            link_side,
                            stream_port,
                        )
                    in
                        ports
                            .iter()
                    {
                        let audio_sinks_borrow: Ref<Vec<AudioSink>> = audio_sinks
                            .borrow();
                        // is always empty after first link created???
                        match
                            stream_port
                                .links
                                .is_empty()
                        {
                            true => {
                                // does sink really exist???
                                let pos_new: usize = 0;
                                let sink_new_name: String = audio_sinks_borrow
                                    [pos_new]
                                    .name
                                    .to_owned();
                                let audio_sink_node: u32 = audio_sinks_borrow
                                    [pos_new]
                                    .id_node
                                    .to_owned();
                                let audio_sink_port: u32 = select_port_side(
                                    audio_sinks_borrow,
                                    link_side
                                        .to_owned(),
                                    pos_new,
                                );
                                create_link_audio(
                                    core,
                                    audio_sink_node,
                                    stream_node
                                        .id_node,
                                    audio_sink_port,
                                    stream_port
                                        .port_out,
                                );
                                next_line(next_line_rc.clone());
                                println!(
                                    ">< {SINK_CONNECTED}:{tab}{channel} -> [ {link_side:?} ] {{ {sink_new_name} }}",
                                    tab = tab_filler(SINK_CONNECTED),
                                    channel = get_channel_num(channel_maps.clone(), channel,),
                                );
                            }
                            false => {
                                // needs better selection strategy!!!
                                // let streamlink: StreamLink = stream_port.links[0];
                                match
                                    stream_port
                                        .links
                                        .first()
                                {
                                    None => println!(
                                        "!! Problem finding StreamLink.\n links: {:?}",
                                        stream_port
                                            .links,
                                    ),
                                    Some(streamlink) => {
                                        // List of all audio sinks:
                                        // let audio_sinks_borrow: Ref<Vec<AudioSink>> = audio_sinks.borrow();
                                        let audio_sinks_iter: Enumerate<Iter<AudioSink>> = audio_sinks_borrow
                                            .iter()
                                            .enumerate();
                                        // Get audio_sink and its position from the list:
                                        for
                                            (
                                                pos,
                                                audio_sink,
                                            )
                                        in
                                            audio_sinks_iter
                                        {
                                            // println!("available sink: {:?} - streamlink: link{:?} node{:?}", audio_sink.name, streamlink.link,  streamlink.node_in);
                                            // Check audio sink against old sink node_in:
                                            if
                                                audio_sink
                                                    .id_node
                                                ==
                                                streamlink
                                                    .id_node_in
                                            {
                                                // Select next audio sink from list of audio sinks:
                                                let pos_new: usize = match
                                                    pos + 1
                                                {
                                                    new
                                                        if
                                                            new
                                                            >=
                                                            audio_sinks_borrow
                                                                .len()
                                                    => 0,
                                                    new => new,
                                                };
                                                // Get name of new sink:
                                                let sink_new_name: String = audio_sinks_borrow
                                                    [pos_new]
                                                    .name
                                                    .to_owned();
                                                let audio_sink_node: u32 = audio_sinks_borrow
                                                    [pos_new]
                                                    .id_node
                                                    .to_owned();
                                                let audio_sink_port: u32 = select_port_side(
                                                    audio_sinks_borrow,
                                                    link_side
                                                        .to_owned(),
                                                    pos_new,
                                                );
                                                // Connect and disconnect port:
                                                switch_link_audio(
                                                    core,
                                                    streamlink
                                                        .id_link,
                                                    audio_sink_node,
                                                    // streamlink.node_out,
                                                    stream_node
                                                        .id_node,
                                                    audio_sink_port,
                                                    // streamlink.port_out,
                                                    stream_port
                                                        .port_out,
                                                    registry
                                                        .clone(),
                                                );
                                                next_line(next_line_rc.clone());
                                                println!(
                                                    ">< {SINK_SWITCHED}:{tab}{channel} -> [ {link_side:?} ] {{ {sink_new_name} }}",
                                                    tab = tab_filler(SINK_SWITCHED),
                                                    channel = get_channel_num(channel_maps.clone(), channel,),
                                                );
                                                break;
                                            };
                                        };
                                    },
                                };
                            },
                        };
                    };
                },
            }
        },
    };
}

pub fn get_channel_num(
    channel_maps: ChannelMaps,
    channel: u8,
) -> u8 {
    match
        channel_maps
            .sliders_to_buttons
            .num
            .get(
                &channel
            )
    {
        None => {
            panic!(
                "\n!! Problem getting channel num:\n event_channel: {:?}\n slider_map_num: {:?}\n",
                channel,
                channel_maps
                    .sliders_to_buttons
                    .num,
            );
        }
        Some(channel) => channel
            .to_owned(),
    }
}

fn select_port_side(
    audio_sinks_borrow: Ref<Vec<AudioSink>>,
    link_side: LinkSide,
    pos_new: usize,
) -> u32 {
    match link_side {
        LinkSide::FL => audio_sinks_borrow
            [pos_new]
            .port_left,
        LinkSide::FR => audio_sinks_borrow
            [pos_new]
            .port_right,
    }
}
