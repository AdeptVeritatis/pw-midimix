
use crate::{
    app::print::{
        PrintState,
        PrintTab,
        next_line,
    },
    hardware::{
        led::{
            LedState,
            switch_led,
        },
        mapping::Mapping,
    },
    impls::pipewire_impl::{
        objects::node::stream::AudioStream,
        utils::mute::set_mute,
    },
};
use midir::MidiOutputConnection;
use std::{
    cell::{
        RefCell,
        RefMut,
    },
    collections::HashMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn mute_button(
    audio_streams: Rc<RefCell<HashMap<u8, AudioStream>>>,
    event_channel: u8,
    event_led: u8,
    mapping: Mapping,
    midir_out: Rc<RefCell<MidiOutputConnection>>,
    next_line_rc: Rc<RefCell<PrintState>>,
    option_leds_off: bool,
    print_tab: PrintTab,
// ) -> Result<(), Box<dyn std::error::Error>> {
) {
    let mut audio_streams_borrow: RefMut<HashMap<u8, AudioStream>> = audio_streams.borrow_mut();
    if audio_streams_borrow.contains_key(&event_channel) {
        let audio_stream: &AudioStream = match audio_streams_borrow.get(&event_channel) {
            Some(stream) => stream,
            None => panic!(
                "\n!! Problem getting audio stream:\n event_channel: {:?}\n audio_streams: {:?}",
                event_channel, audio_streams_borrow,
            ),
        };
        let event_mute: LedState = audio_stream.mute;
        let id_node: u32 = audio_stream.id_node;
        let event_stamp: u64 = audio_stream.stamp;

        let map_channel_to_num: HashMap<u8, u8> = mapping.channel_maps.sliders_to_buttons.num;
        let channel_num: u8 = match map_channel_to_num.get(&event_channel) {
            Some(num) => num.to_owned(),
            None => panic!(
                "\n!! Problem getting map:\n event_channel: {:?}\n map_channel_to_num: {:?}\n",
                event_channel, map_channel_to_num,
            ),
        };
        // Set new mute state from actual state:
        let (event_mute_new, mute_state_new, mute_string_new): (LedState, bool, String) =
            match event_mute {
                LedState::Off => {
                    //                 println!("unmute -> mute");
                    (LedState::Mute, true, String::from("< muted >"))
                }
                LedState::Mute => {
                    //                 println!("mute -> unmute");
                    (LedState::Off, false, String::from("> loud <"))
                }
                LedState::Active => {
                    next_line(Rc::clone(&next_line_rc));
                    println!("!! Warning: invalid mute_state {:?}", LedState::Active);
                    (LedState::Off, false, String::from("loud"))
                }
                LedState::Flash => {
                    next_line(Rc::clone(&next_line_rc));
                    println!("!! Warning: invalid mute_state {:?}", LedState::Flash);
                    (LedState::Off, false, String::from("loud"))
                }
                LedState::Invalid => {
                    next_line(Rc::clone(&next_line_rc));
                    println!("!! Warning: invalid mute_state {:?}", LedState::Invalid);
                    (LedState::Off, false, String::from("loud"))
                }
            };

        // println!(" event_node: {:?}", &mute_state.borrow()[0].to_string());
        match set_mute(
            channel_num,
            id_node,
            mute_state_new,
            mute_string_new,
            next_line_rc,
            print_tab,
        ) {
            Ok(_) => (),
            Err(error) => panic!("\n!! Problem setting mute:\n error: {:?}", error,),
        }

        match option_leds_off {
            true => (),
            false => {
                switch_led(event_led, midir_out, &mapping.mixer_map, event_mute_new);
            }
        };
        let audio_stream_new: AudioStream = AudioStream {
            id_node,
            mute: event_mute_new,
            name: audio_stream.to_owned().name,
            port_position: audio_stream.to_owned().port_position,
            ports: audio_stream.to_owned().ports,
            sink_name: audio_stream.to_owned().sink_name,
            stamp: event_stamp,
        };
        audio_streams_borrow.insert(event_channel, audio_stream_new);
    }
}
