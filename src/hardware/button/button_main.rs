
use crate::{
    app::{
        localization::MAIN_DEVICE,
        print::{
            PrintState,
            next_line,
            tab_filler,
        },
    },
    impls::pipewire_impl::objects::device::AudioDevice,
};
use std::{
    cell::{
        Ref,
        RefCell,
    },
    rc::Rc,
};

// ----------------------------------------------------------------------------

// ButtonMain:
#[derive(Debug)]
pub enum ButtonMain {
    Next,
    Prev,
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn button_pressed_main(
    audio_device_main: Rc<RefCell<usize>>,
    audio_devices: Rc<RefCell<Vec<AudioDevice>>>,
    main_step: ButtonMain,
    next_line_rc: Rc<RefCell<PrintState>>,
) {
    let main_test: usize = audio_device_main
        .borrow()
        .to_owned();
    let audio_devices_borrow: Ref<Vec<AudioDevice>> = audio_devices.borrow();
    let audio_devices_len: usize = audio_devices_borrow.len();
    match main_step {
        ButtonMain::Next => {
            if main_test == 0 {
                audio_device_main.replace(audio_devices_len - 1);
            } else {
                audio_device_main.replace(main_test - 1);
            }
        }
        ButtonMain::Prev => {
            if main_test + 1 >= audio_devices_len {
                audio_device_main.replace(0);
            } else {
                audio_device_main.replace(main_test + 1);
            }
        }
    }
    let main_select: usize = audio_device_main.borrow().to_owned();
    let audio_device_name: &String = match audio_devices_borrow.iter().nth(main_select) {
        Some(devices) => devices,
        None => panic!(
            "\n!! Problem getting audio device:\n main_select: {:?}\n audio_devices: {:?}",
            main_select, audio_devices_borrow,
        ),
    }
    .get_name();
    next_line(Rc::clone(&next_line_rc));
    println!(
        "<> {MAIN_DEVICE}:{tab}( {audio_device_name} )",
        tab = tab_filler(MAIN_DEVICE),
    );
}
