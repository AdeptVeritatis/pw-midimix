
use crate::{
    app::{
        localization::{
            ACTIVE_CHANNEL,
            ACTIVE_STREAMS,
            MOVE_CHANNEL,
            SWAP_CHANNELS,
        },
        options::ActiveMethod,
        print::{
            PrintState,
            PrintTab,
            next_line,
            tab_filler,
        },
    },
    declarations::OFFSET_BUTTON_ACTIVE,
    hardware::{
        button::{
            button_sink::get_channel_num,
            ButtonActive,
        },
        led::{
            LedState,
            switch_led,
        },
        mapping::{
            ChannelMaps,
            Mapping,
        },
    },
    impls::pipewire_impl::{
        mainloop::mainloop_audio::MainLoopAudio,
        objects::node::stream::AudioStream,
        strings::{
            DUMP_PARAMS,
            NODE_PARAMS_MUTE,
            NODE_PARAMS_PROPS,
        },
        utils::dump_json::get_json_info,
    },

};
use midir::MidiOutputConnection;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn active_button(
    mainloop_audio: MainLoopAudio,
    event_channel: u8,
    event_stamp: u64,
    mapping: Mapping,
    option_active_method: ActiveMethod,
    option_leds_off: bool,
    print_tab: PrintTab,
) {
    let audio_streams: HashMap<u8, AudioStream> = mainloop_audio
        .audio_streams
        .borrow()
        .to_owned();
    let map_channel_to_num: HashMap<u8, u8> = mapping
        .channel_maps
        .sliders_to_buttons
        .num
        .clone();
    let button_active: ButtonActive = mainloop_audio
        .button_active
        .borrow()
        .to_owned();

    let prev_channel: u8 = button_active
        .channel;
// Check, if 1st button is released and 2nd button is from active channel:
    match
        (
            button_active
                .released,
            audio_streams
                .clone()
                .get(
                    &event_channel
                ),
        )
    {
        // 1st button still pressed, 2nd is active channel:
        (
            false,
            Some(_)
        ) => {
            match
                option_active_method
            {
                ActiveMethod::Combination => {
                    // Swap channel with another channel on the mixer:
                    set_button_active(
                        mainloop_audio
                            .button_active,
                        event_channel,
                        0,
                    );
                    swap_channels(
                        mainloop_audio
                            .audio_streams,
                        event_channel,
                        mapping,
                        mainloop_audio
                            .midir_out,
                        mainloop_audio
                            .next_line,
                        option_leds_off,
                        prev_channel,
                    )
                }
                ActiveMethod::Timer => {
                    println!("!! no combinations in timer based method available");
                }
            }
        },
        // 1st button still pressed, 2nd is empty channel:
        (false, None) => {
            match option_active_method {
                ActiveMethod::Combination => {
                    // Move channel to another channel on the mixer:
                    reset_button_active(
                        mainloop_audio
                            .button_active
                            .clone()
                    );
                    move_channel(
                        mainloop_audio
                            .audio_streams,
                        event_channel,
                        mapping,
                        mainloop_audio
                            .midir_out,
                        mainloop_audio
                            .next_line,
                        option_leds_off,
                        prev_channel,
                    )
                }
                ActiveMethod::Timer => {
                    println!("!! no combinations in timer based method available");
                }
            }
        },
        // 1st button released, 2nd is active channel:
        (true, Some(audio_stream_now)) => {
            // Check if within timer:
            match event_stamp {
                // Same button twice within timer:
                stamp
                    if (stamp < button_active.stamp + OFFSET_BUTTON_ACTIVE)
                        && (button_active.channel == event_channel) =>
                {
                    // Print active stream list:
                    set_button_active(
                        mainloop_audio
                            .button_active
                            .clone(),
                        event_channel,
                        0,
                    );
                    print_active_streams(
                        audio_streams,
                        map_channel_to_num,
                        mainloop_audio
                            .next_line,
                        print_tab,
                    );
                }
                // Other button within timer with timer method:
                stamp
                    if (stamp < button_active.stamp + OFFSET_BUTTON_ACTIVE)
                        && (option_active_method == ActiveMethod::Timer) =>
                {
                    // Swap channel with another channel on the mixer:
                    reset_button_active(mainloop_audio.button_active.clone());
                    swap_channels(
                        mainloop_audio
                            .audio_streams,
                        event_channel,
                        mapping,
                        mainloop_audio
                            .midir_out,
                        mainloop_audio
                            .next_line,
                        option_leds_off,
                        prev_channel,
                    )
                }
                // Some button but after timeout:
                _ => {
                    // Print active channel sink:
                    set_button_active(mainloop_audio.button_active.clone(), event_channel, event_stamp);
                    // Get mute state from pw-dump again and set it, if different:
                    let mute_string: String = get_mute_string(
                        audio_streams,
                        mainloop_audio
                            .audio_streams,
                        event_channel,
                        mapping
                            .clone(),
                        mainloop_audio
                            .midir_out,
                        mainloop_audio
                            .next_line
                            .clone(),
                        option_leds_off,
                    );
                    print_active_channel(
                        audio_stream_now
                            .to_owned(),
                        mapping
                            .channel_maps,
                        event_channel,
                        mute_string,
                        mainloop_audio
                            .next_line,
                        print_tab,
                    );
                }
            }
        },
        // 1st button released, 2nd is empty channel:
        (true, None) => {
            match option_active_method {
                ActiveMethod::Combination => {
                    // Print active stream list:
                    reset_button_active(mainloop_audio.button_active);
                    print_active_streams(
                        audio_streams,
                        map_channel_to_num,
                        mainloop_audio
                            .next_line,
                        print_tab,
                    );
                }
                ActiveMethod::Timer => {
                    // Check if within timer:
                    match event_stamp {
                        // Same button twice within timer:
                        stamp
                            if (stamp < button_active.stamp + OFFSET_BUTTON_ACTIVE)
                                && (button_active.channel == event_channel) =>
                        {
                            // Print active stream list:
                            reset_button_active(mainloop_audio.button_active);
                            print_active_streams(
                                audio_streams,
                                map_channel_to_num,
                                mainloop_audio
                                    .next_line,
                                print_tab,
                            );
                        }
                        stamp if (stamp < button_active.stamp + OFFSET_BUTTON_ACTIVE) => {
                            // Move channel to another channel on the mixer:
                            reset_button_active(mainloop_audio.button_active.clone());
                            move_channel(
                                mainloop_audio
                                    .audio_streams,
                                event_channel,
                                mapping,
                                mainloop_audio
                                    .midir_out,
                                mainloop_audio
                                    .next_line,
                                option_leds_off,
                                prev_channel,
                            )
                        }
                        _ => {
                            // Print active stream list:
                            reset_button_active(mainloop_audio.button_active);
                            print_active_streams(
                                audio_streams,
                                map_channel_to_num,
                                mainloop_audio
                                    .next_line,
                                print_tab,
                            );
                        }
                    }
                }
            }
        },
    };
}

// For cases with empty channel:
fn reset_button_active(
    button_active_rc: Rc<RefCell<ButtonActive>>,
    // released: bool,
) {
    button_active_rc.replace(ButtonActive {
        channel: 0,
        released: true,
        stamp: 0,
    });
}

// For cases with active channel:
fn set_button_active(
    button_active_rc: Rc<RefCell<ButtonActive>>,
    channel: u8,
    // released: bool,
    stamp: u64,
) {
    button_active_rc.replace(ButtonActive {
        channel,
        released: false,
        stamp,
    });
}

fn get_mute_string(
    audio_streams: HashMap<u8, AudioStream>,
    audio_streams_rc: Rc<RefCell<HashMap<u8, AudioStream>>>,
    event_channel: u8,
    mapping: Mapping,
    midir_out_rc: Rc<RefCell<MidiOutputConnection>>,
    next_line_rc: Rc<RefCell<PrintState>>,
    option_leds_off: bool,
) -> String {
    match audio_streams.get(&event_channel) {
        Some(audio_stream) => {
            let mute_state_real: LedState = match get_json_info(audio_stream.id_node) {
                Ok(info) => {
                    match info[DUMP_PARAMS][NODE_PARAMS_PROPS][0][NODE_PARAMS_MUTE].as_bool() {
                        Some(true) => LedState::Mute,
                        Some(false) => LedState::Off,
                        None => {
                            next_line(next_line_rc);
                            println!(
                                "!! Problem with irregular or empty mute value.\n dump: {:#}",
                                info,
                            );
                            LedState::Invalid
                        }
                    }
                }
                Err(error) => {
                    next_line(next_line_rc);
                    println!("!! Problem with dump from node:\n error: {error}");
                    LedState::Invalid
                }
            };
            if (mute_state_real != audio_stream.mute) && (mute_state_real != LedState::Invalid) {
                // Update audio_streams:
                audio_streams_rc.borrow_mut().insert(
                    event_channel,
                    AudioStream {
                        mute: mute_state_real,
                        ..audio_stream.to_owned()
                    },
                );
                match mapping.channel_maps.sliders_to_buttons.mute.get(&event_channel) {
                    Some(event_led) => match option_leds_off {
                        true => (),
                        false => {
                            // Switch mute led:
                            switch_led(
                                event_led.to_owned(),
                                midir_out_rc,
                                &mapping
                                    .mixer_map,
                                mute_state_real,
                            );
                        }
                    },
                    None => {}
                };
            }
            match mute_state_real {
                LedState::Mute => String::from("< muted >"),
                LedState::Off => String::from("> loud <"),
                _ => String::from("error"),
            }
        }
        None => {
            next_line(next_line_rc);
            println!(
                "!! Problem getting audio stream:\n event_channel: {:?}\n audio_streams: {:?}",
                event_channel, audio_streams,
            );
            String::from("error")
        }
    }
}

fn print_active_channel(
    audio_stream: AudioStream,
    channel_maps: ChannelMaps,
    event_channel: u8,
    mute_string: String,
    next_line_rc: Rc<RefCell<PrintState>>,
    print_tab: PrintTab,
) {
    next_line(Rc::clone(&next_line_rc));
    println!(
        "-- {ACTIVE_CHANNEL}:{tab}{{ {sink} }}",
        tab = tab_filler(ACTIVE_CHANNEL),
        sink = audio_stream.sink_name,
    );
    println!(
        // "{tab_pre}{channel}{tab_suf}[ {mute_string} ]",
        // "{tab_pre}{channel}{tab_suf}< {mute_string} >",
        "{tab_pre}{channel}{tab_suf}{mute_string}",
        tab_pre = print_tab.prefix_str,
        tab_suf = print_tab.suffix_str,
        channel = get_channel_num(channel_maps, event_channel),
    );
}

fn move_channel(
    audio_streams_rc: Rc<RefCell<HashMap<u8, AudioStream>>>,
    event_channel: u8,
    mapping: Mapping,
    midir_out_rc: Rc<RefCell<MidiOutputConnection>>,
    next_line_rc: Rc<RefCell<PrintState>>,
    option_leds_off: bool,
    prev_channel: u8,
) {
    // Move channel to another channel on the mixer:
    // println!("move channel");
    let mut audio_streams_borrow = audio_streams_rc.borrow_mut();
    match audio_streams_borrow.remove(&prev_channel) {
        Some(stream_node) => {
            audio_streams_borrow.insert(event_channel, stream_node.clone());
            move_channel_print(
                mapping.channel_maps.clone(),
                event_channel,
                next_line_rc,
                prev_channel,
            );
            if !option_leds_off {
                move_channel_leds(
                    event_channel,
                    mapping,
                    midir_out_rc,
                    stream_node.mute,
                    prev_channel,
                );
            }
        }
        None => {
            println!("problem removing prev_channel")
        }
    }
}

fn move_channel_print(
    channel_maps: ChannelMaps,
    event_channel: u8,
    next_line_rc: Rc<RefCell<PrintState>>,
    prev_channel: u8,
) {
    let map_channel_to_num: HashMap<u8, u8> = channel_maps.sliders_to_buttons.num;
    next_line(next_line_rc);
    println!(
        "<> {MOVE_CHANNEL}:{tab}{ch_prev} -> {ch_next}",
        tab = tab_filler(MOVE_CHANNEL),
        ch_prev = match map_channel_to_num.get(&prev_channel) {
            Some(channel) => channel.to_string(),
            None => String::from("error"),
        },
        ch_next = match map_channel_to_num.get(&event_channel) {
            Some(channel) => channel.to_string(),
            None => String::from("error"),
        },
    );
}

fn move_channel_leds(
    event_channel: u8,
    mapping: Mapping,
    midir_out: Rc<RefCell<MidiOutputConnection>>,
    mute_state: LedState,
    prev_channel: u8,
) {
    let map_channel_to_active: HashMap<u8, u8> = mapping.channel_maps.clone().sliders_to_buttons.active;
    let map_channel_to_mute: HashMap<u8, u8> = mapping.channel_maps.sliders_to_buttons.mute;
    // LED active old off:
    let prev_channel_active: u8 = match map_channel_to_active.get(&prev_channel) {
        Some(channel) => channel.to_owned(),
        None => panic!(
            // need to panic?
            "\n!! Problem getting channel:\n prev_channel: {:?}\n map: {:?}",
            prev_channel, map_channel_to_active,
        ),
    };
    switch_led(
        prev_channel_active,
        midir_out
            .clone(),
        &mapping
            .mixer_map,
        LedState::Off,
    );
    // LED active new on:
    let event_channel_active: u8 = match map_channel_to_active.get(&event_channel) {
        Some(channel) => channel.to_owned(),
        None => panic!(
            // need to panic?
            "\n!! Problem getting channel acitve:\n event_channel: {:?}\n map: {:?}",
            event_channel, map_channel_to_active,
        ),
    };
    switch_led(
        event_channel_active,
        midir_out
            .clone(),
        &mapping
            .mixer_map,
        LedState::Active,
    );
    if mute_state == LedState::Mute {
        // LED mute old off, if on:
        let prev_channel_mute: u8 = match map_channel_to_mute.get(&prev_channel) {
            Some(channel) => channel.to_owned(),
            None => panic!(
                // need to panic?
                "\n!! Problem getting channel mute:\n prev_channel: {:?}\n map: {:?}",
                prev_channel, map_channel_to_mute,
            ),
        };
        switch_led(
            prev_channel_mute,
            midir_out
                .clone(),
            &mapping
                .mixer_map,
            LedState::Off,
        );
        // LED mute new on, if on:
        let event_channel_mute: u8 = match map_channel_to_mute.get(&event_channel) {
            Some(channel) => channel.to_owned(),
            None => panic!(
                // need to panic?
                "\n!! Problem getting channel mute:\n event_channel: {:?}\n map: {:?}",
                event_channel, map_channel_to_mute,
            ),
        };
        switch_led(
            event_channel_mute,
            midir_out
                .clone(),
            &mapping
                .mixer_map,
            LedState::Mute,
        );
    }
}

fn print_active_streams(
    audio_streams: HashMap<u8, AudioStream>,
    map_channel_to_num: HashMap<u8, u8>,
    next_line_rc: Rc<RefCell<PrintState>>,
    print_tab: PrintTab,
) {
    // Print active stream list:
    next_line(Rc::clone(&next_line_rc));
    println!("-- {ACTIVE_STREAMS}:");
    print_channels(
        audio_streams,
        map_channel_to_num,
        Rc::clone(&next_line_rc),
        print_tab,
    );
}

fn print_channels(
    audio_streams: HashMap<u8, AudioStream>,
    map_channel_to_num: HashMap<u8, u8>,
    next_line_rc: Rc<RefCell<PrintState>>,
    print_tab: PrintTab,
) {
    // Print active stream list:
    let mut audio_streams_vec: Vec<(&u8, &AudioStream)> = audio_streams.iter().collect();
    audio_streams_vec.sort_by(|a, b| a.0.cmp(b.0));
    for (channel, audio_stream) in audio_streams_vec {
        let event_num: String = match map_channel_to_num.get(channel) {
            Some(event_num) => event_num.to_string(),
            None => String::from("error"),
        };
        next_line(Rc::clone(&next_line_rc));
        println!(
            "{tab_pre}{event_num}{tab_suf}{name}",
            tab_pre = print_tab.prefix_str,
            tab_suf = print_tab.suffix_str,
            name = audio_stream.name,
        );
    }
}

fn swap_channels(
    audio_streams_rc: Rc<RefCell<HashMap<u8, AudioStream>>>,
    event_channel: u8,
    mapping: Mapping,
    midir_out_rc: Rc<RefCell<MidiOutputConnection>>,
    next_line_rc: Rc<RefCell<PrintState>>,
    option_leds_off: bool,
    prev_channel: u8,
) {
    // Swap channel with another channel on the mixer:
    // println!("swap channels");
    let mut audio_streams_borrow = audio_streams_rc.borrow_mut();
    // Get 2nd StreamNode by deleting it:
    match audio_streams_borrow.remove(&event_channel) {
        Some(stream_node_now) => {
            // Get 1st StreamNode by deleting it:
            match audio_streams_borrow.remove(&prev_channel) {
                Some(stream_node_prev) => {
                    // Exchange both StreamNodes:
                    audio_streams_borrow.insert(prev_channel, stream_node_now.clone());
                    audio_streams_borrow.insert(event_channel, stream_node_prev.clone());
                    // Display changes:
                    swap_channels_print(
                        mapping.channel_maps.clone(),
                        event_channel,
                        Rc::clone(&next_line_rc),
                        prev_channel,
                    );
                    if !option_leds_off {
                        swap_channels_leds(
                            event_channel,
                            mapping,
                            midir_out_rc,
                            stream_node_now.mute,
                            stream_node_prev.mute,
                            prev_channel,
                        )
                    }
                }
                None => println!(
                    "\n!! Problem removing channel:\n event_channel: {:?}\n audio_streams: {:?}",
                    event_channel, audio_streams_borrow,
                ),
            }
        }
        None => (),
    }
}

fn swap_channels_print(
    channel_maps: ChannelMaps,
    event_channel: u8,
    next_line_rc: Rc<RefCell<PrintState>>,
    prev_channel: u8,
) {
    let map_channel_to_num: HashMap<u8, u8> = channel_maps.sliders_to_buttons.num;
    next_line(Rc::clone(&next_line_rc));
    println!(
        "<> {SWAP_CHANNELS}:{tab}{ch_one} <> {ch_two}",
        tab = tab_filler(SWAP_CHANNELS),
        ch_one = match map_channel_to_num.get(&prev_channel) {
            Some(channel) => channel.to_string(),
            None => String::from("error"),
        },
        ch_two = match map_channel_to_num.get(&event_channel) {
            Some(channel) => channel.to_string(),
            None => String::from("error"),
        },
    );
}

fn swap_channels_leds(
    event_channel: u8,
    mapping: Mapping,
    midir_out: Rc<RefCell<MidiOutputConnection>>,
    mute_state_now: LedState,
    mute_state_prev: LedState,
    prev_channel: u8,
) {
    if mute_state_prev != mute_state_now {
        let map_channel_to_mute: HashMap<u8, u8> = mapping.channel_maps.sliders_to_buttons.mute;
        // Change mute LED:
        let channel_mute_prev: u8 = match map_channel_to_mute.get(&prev_channel) {
            Some(channel) => channel.to_owned(),
            None => panic!(
                // need to panic?
                "\n!! Problem getting channel mute:\n prev_channel: {:?}\n map: {:?}",
                prev_channel, map_channel_to_mute,
            ),
        };
        // Swap states:
        switch_led(
            channel_mute_prev,
            midir_out
                .clone(),
            &mapping
                .mixer_map,
            mute_state_now,
        );
        let channel_mute_now: u8 = match map_channel_to_mute.get(&event_channel) {
            Some(channel) => channel.to_owned(),
            None => panic!(
                // need to panic?
                "\n!! Problem getting channel mute:\n event_channel: {:?}\n map: {:?}",
                event_channel, map_channel_to_mute,
            ),
        };
        // Swap states:
        switch_led(
            channel_mute_now,
            midir_out,
            &mapping
                .mixer_map,
            mute_state_prev,
        );
    }
}
