mod button_active;
mod button_main;
mod button_mute;
mod button_sink;

use crate::{
    app::{
        options::ActiveMethod,
        print::PrintTab,
    },
    hardware::{
        button::{
            button_active::active_button,
            button_main::{
                ButtonMain,
                button_pressed_main,
            },
            button_mute::mute_button,
            button_sink::sink_button,
        },
        mapping::{
            ChannelMaps,
            Mapping,
        },
        midi::MidiMessage,
    },
    impls::pipewire_impl::mainloop::mainloop_audio::MainLoopAudio,
};
use pipewire::{
    registry::Registry,
    core::Core,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// ButtonActive:
// Gets updated on active button (REC ARM) press.
// Store stamp for timer on 2nd key press.
// Only one for all channels is needed.
// The next press is either a 2nd (and gets reset) or a new 1st one.
#[derive(Debug, Clone)]
pub struct ButtonActive {
    pub channel: u8,
    pub released: bool,
    pub stamp: u64,
}

impl Default for ButtonActive {
    fn default() -> Self {
        Self {
            channel: 0,
            released: true,
            stamp: 0,
        }
    }
}

// ----------------------------------------------------------------------------

// // ButtonNonchannel:
// // All buttons which are not designated to a channel, like SOLO and BANK L & R.
// #[derive(Debug, Clone)]
// pub struct ButtonsNonchannel {
//     pub button_device_next: u8,
//     pub button_device_prev: u8,
//     pub button_shift: u8,
// }

// ----------------------------------------------------------------------------

// // ButtonsState for LED:
// // Struct to collect the button states of the device mapping from MixerMap.
// // Defines the colour of the LEDs for some mixer devices.
// // Needed to switch LEDs.
// #[derive(Debug, Clone, Copy)]
// pub struct ButtonsState {
//     pub active: u8,
//     pub mute: u8,
//     pub off: u8,
// }

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn button_pressed(
    core: &Rc<Core>,
    mainloop_audio: MainLoopAudio,
    mapping: Mapping,
    message: MidiMessage,
    option_active_method: ActiveMethod,
    option_leds_off: bool,
    print_tab: PrintTab,
    registry: Rc<Registry>,
) {
    let event_note: u8 = message
        .channel;
    let event_stamp: u64 = message
        .stamp;
    //     println!("Button pressed:");
    //     println!(" stamp    - {:?}", event_stamp);
    //     println!(" note     - {:?}", event_note);

    if
        event_note
        ==
        mapping
            .mixer_map
            .button_device_next
    {
        // println!(" button DEVICE NEXT pressed");
        button_pressed_main(
            mainloop_audio
                .audio_device_main,
            mainloop_audio
                .audio_devices,
            ButtonMain::Next,
            mainloop_audio
                .next_line,
        );
    } else if
        event_note
        ==
        mapping
            .mixer_map
            .button_device_prev
    {
        // println!(" button DEVICE PREV pressed");
        button_pressed_main(
            mainloop_audio
                .audio_device_main,
            mainloop_audio
                .audio_devices,
            ButtonMain::Prev,
            mainloop_audio
                .next_line,
        );
    // avoid unnecessary work
    } else if
        event_note
        ==
        mapping
            .mixer_map
            .button_shift
    {
        //         println!(" button SOLO pressed");
    } else {
        button_pressed_channels(
            core,
            event_note,
            event_stamp,
            mainloop_audio,
            mapping,
            option_active_method,
            option_leds_off,
            print_tab,
            registry,
        );
    };
}

// All buttons, which are connected to a channel.
fn button_pressed_channels(
    core: &Rc<Core>,
    event_note: u8,
    event_stamp: u64,
    mainloop_audio: MainLoopAudio,
    mapping: Mapping,
    option_active_method: ActiveMethod,
    option_leds_off: bool,
    print_tab: PrintTab,
    registry: Rc<Registry>,
) {
    //     println!(" event_note {:?}", event_note);
    match
        mapping
            .channel_maps
            .buttons_to_sliders
            .mute
            .get(
                &event_note
            )
    {
        None => {
            // println!("\n!! problem getting map:\n   event_note: {event_note:?}\n   map_mute_to_channel: {:?}", mapping.channel_maps.buttons_to_sliders.mute);
        },
        // Is mute button:
        Some(event_channel) => {
            // println!("mute button pressed");
            mute_button(
                mainloop_audio
                    .audio_streams
                    .clone(),
                event_channel
                    .to_owned(),
                event_note,
                mapping
                    .clone(),
                mainloop_audio
                    .midir_out
                    .clone(),
                mainloop_audio
                    .next_line
                    .clone(),
                option_leds_off,
                print_tab
                    .clone(),
            );
            return;
        },
    };
    match
        mapping
            .channel_maps
            .buttons_to_sliders
            .sink
            .get(
                &event_note
            )
    {
        None => {
            // println!("\n!! problem getting map:\n   event_note: {event_note:?}\n   map_sink_to_channel: {:?}", mapping.channel_maps.buttons_to_sliders.sink);
        },
        // Is sink button (SOLO + MUTE):
        Some(event_channel) => {
            // println!("sink button pressed");
            sink_button(
                mainloop_audio
                    .audio_sinks
                    .clone(),
                mainloop_audio
                    .audio_streams
                    .clone(),
                mapping
                    .channel_maps
                    .clone(),
                core,
                event_channel
                    .to_owned(),
                mainloop_audio
                    .next_line
                    .clone(),
                registry,
            );
            return;
        },
    };
    match
        mapping
            .channel_maps
            .buttons_to_sliders
            .active
            .get(
                &event_note
            )
    {
        None => {
            // println!("\n!! problem getting map:\n   event_note: {event_note:?}\n   map_active_to_channel: {:?}", mapping.channel_maps.buttons_to_sliders.active);
        },
        // Is button of active channel (REC ARM):
        Some(event_channel) => {
            // println!("active button pressed");
            active_button(
                mainloop_audio // split object up???
                    .clone(),
                event_channel
                    .to_owned(),
                event_stamp,
                mapping,
                option_active_method,
                option_leds_off,
                print_tab,
            );
        },
    };
}

pub fn button_released(
    button_active: Rc<RefCell<ButtonActive>>,
    channel_maps: ChannelMaps,
    message: MidiMessage,
) {
    // match
    //     button_active
    //         .borrow()
    //         .to_owned()
    // {
    //     None => (),
    //     Some(_button) => { // use object to check or is it useless ???
            let map_active_to_channel: HashMap<u8, u8> = channel_maps
                .buttons_to_sliders
                .active;
            if
                map_active_to_channel
                    .contains_key(
                        &message
                            .channel
                    )
            {
                // match map_active_to_channel.get(&message.channel) {
                //     Some(event_channel) => {
                //         if event_channel.to_owned() == button_active.channel {
                // println!("released");
                button_active
                    .replace(
                        ButtonActive::default(),
                        // None
                    );
                //         }
                //     },
                //     None => (),
                // }
            };
    //     },
    // };
}
