
use crate::{
    app::options::AppOptions,
    hardware::{
        MixerMap,
        MixerMapName,
    },
};
use std::collections::HashMap;

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct Mapping {
    pub channel_maps: ChannelMaps,
    pub mixer_map: MixerMap,
}

impl Mapping {
    pub fn new(
        app_options: &AppOptions,
        mixer_map_name: MixerMapName,
    ) -> Self {
        // Get mapping:
        let mixer_map: MixerMap = MixerMap::new(
            mixer_map_name,
        );

        // Mapping mixer channels:
        let channel_maps: ChannelMaps = ChannelMaps::new(
            mixer_map
                .clone(),
            app_options
                .channels_reversed,
        ); // mixer channel map

        Self {
            channel_maps,
            mixer_map,
        }
    }
}

// ----------------------------------------------------------------------------

// Maps to find the corresponding slider ids from button ids:
#[derive(Debug, Clone)]
pub struct MapButtonsToSliders {
    pub active: HashMap<u8, u8>,
    pub mute: HashMap<u8, u8>,
    pub sink: HashMap<u8, u8>,
}

// ----------------------------------------------------------------------------

// Maps to find the corresponding button ids from slider ids:
#[derive(Debug, Clone)]
pub struct MapSlidersToButtons {
    pub active: HashMap<u8, u8>,
    pub mute: HashMap<u8, u8>,
    pub num: HashMap<u8, u8>,
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct ChannelMaps {
    pub buttons_to_sliders: MapButtonsToSliders,
    // Vec to find the corresponding channel numbers:
    pub slider_channels: Vec<u8>,
    pub sliders_to_buttons: MapSlidersToButtons,
}

impl ChannelMaps {
    pub fn new(
        mixer_map: MixerMap,
        option_channels_reversed: bool,
    ) -> Self {
        let mut buttons_active: Vec<u8> = Vec::new();
        let mut buttons_mute: Vec<u8> = Vec::new();
        let mut buttons_sink: Vec<u8> = Vec::new();
        let mut channels_num: Vec<u8> = Vec::new();
        let mut slider_channels: Vec<u8> = Vec::new();

        if
            option_channels_reversed
        {
            for
                (
                    i,
                    channel,
                )
            in
                mixer_map
                    .slider_channels
                    .into_iter()
                    .rev()
                    .enumerate()
            {
                // let pos = sliders_channel.len() - 1 - i;
                // buttons_active.push(mixer_map.buttons_active[pos]);
                buttons_active
                    .insert(
                        0,
                        mixer_map
                            .buttons_active[i],
                    );
                buttons_mute
                    .insert(
                        0,
                        mixer_map
                            .buttons_mute[i],
                    );
                buttons_sink
                    .insert(
                        0,
                        mixer_map
                            .buttons_sink[i],
                    );
                channels_num
                    .insert(
                        0,
                        mixer_map
                            .channels_num[i],
                    );
                slider_channels
                    .push(
                        channel
                    );
            }
        } else {
            buttons_active = mixer_map
                .buttons_active
                .to_vec();
            buttons_mute = mixer_map
                .buttons_mute
                .to_vec();
            buttons_sink = mixer_map
                .buttons_sink
                .to_vec();
            channels_num = mixer_map
                .channels_num
                .to_vec();
            slider_channels = mixer_map
                .slider_channels
                .to_vec();
        };

        let map_channel_to_active: HashMap<u8, u8> = slider_channels
            .iter()
            .copied()
            .zip(
                buttons_active
                    .iter()
                    .copied()
             )
            .collect();
        let map_channel_to_mute: HashMap<u8, u8> = slider_channels
            .iter()
            .copied()
            .zip(
                buttons_mute
                    .iter()
                    .copied()
            )
            .collect();
        let map_channel_to_num: HashMap<u8, u8> = slider_channels
            .iter()
            .copied()
            .zip(
                channels_num
                    .iter()
                    .copied()
            )
            .collect();
        let map_active_to_channel: HashMap<u8, u8> = buttons_active
            .iter()
            .copied()
            .zip(
                slider_channels
                    .iter()
                    .copied()
            )
            .collect();
        let map_mute_to_channel: HashMap<u8, u8> = buttons_mute
            .iter()
            .copied()
            .zip(
                slider_channels
                    .iter()
                    .copied()
            )
            .collect();
        let map_sink_to_channel: HashMap<u8, u8> = buttons_sink
            .iter()
            .copied()
            .zip(
                slider_channels
                    .iter()
                    .copied()
            )
            .collect();

        /*
        println!("active {:?}", map_channel_to_active);
        println!("mute {:?}", map_channel_to_mute);
        println!("num {:?}", map_channel_to_num);
        println!("active {:?}", map_active_to_channel);
        println!("mute {:?}", map_mute_to_channel);
        println!("sink {:?}", map_sink_to_channel);
        println!("vec {:?}", slider_channels);
        */

        let sliders_to_buttons: MapSlidersToButtons = MapSlidersToButtons {
            active: map_channel_to_active,
            mute: map_channel_to_mute,
            num: map_channel_to_num,
        };
        let buttons_to_sliders: MapButtonsToSliders = MapButtonsToSliders {
            active: map_active_to_channel,
            mute: map_mute_to_channel,
            sink: map_sink_to_channel,
        };

        Self {
            buttons_to_sliders,
            slider_channels,
            sliders_to_buttons,
        }
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

/*
    let vec_channel: Vec<u8> = Rc::newSLIDERS_CHANNEL.to_vec();
    let map_channel_to_num: HashMap<u8, u32> = SLIDERS_CHANNEL.into_iter().zip(CHANNELS_NUM.into_iter()).collect();
    // map_channel_to_num_rc.replace(map_channel_to_num.clone());
    let map_channel_to_active: HashMap<u8, u8> = SLIDERS_CHANNEL.into_iter().zip(BUTTONS_ACTIVE.into_iter()).collect();
    let map_channel_to_mute: HashMap<u8, u8> = SLIDERS_CHANNEL.into_iter().zip(BUTTONS_MUTE.into_iter()).collect();
    let map_mute_to_channel: HashMap<u8, u8> = BUTTONS_MUTE.into_iter().zip(SLIDERS_CHANNEL.into_iter()).collect();
    let map_sink_to_channel: HashMap<u8, u8> = BUTTONS_SINK.into_iter().zip(SLIDERS_CHANNEL.into_iter()).collect();
    let map_active_to_channel: HashMap<u8, u8> = BUTTONS_ACTIVE.into_iter().zip(SLIDERS_CHANNEL.into_iter()).collect();
*/
