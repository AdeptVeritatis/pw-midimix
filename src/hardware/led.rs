
use crate::{
    app::options::AppOptions,
    declarations::{
        MESSAGE_BUTTON,
        SLEEP_MILLIS_FLASH_LEDS,
    },
    hardware::{
        mapping::Mapping,
        MixerMap,
    },
};
use midir::MidiOutputConnection;
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
    thread, time,
};

// ----------------------------------------------------------------------------

// LED state:
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum LedState {
    Active,
    Flash,
    Invalid,
    Mute,
    Off,
}

// ----------------------------------------------------------------------------

pub struct LED {
}

impl LED {
    pub fn greeter(
        app_options: &AppOptions,
        midir_out: Rc<RefCell<MidiOutputConnection>>,
        mixer_map: &MixerMap,
    ) {
        // Disable greeter, when greeter or LEDs off option is selected:
        match
            app_options
                .greeter_off
            |
            app_options
                .leds_off
        {
            true => {
                // Switch off all leds:
                switch_all_leds(
                    LedState::Off,
                    midir_out,
                    mixer_map,
                );
            },
            false => {
                // Flash all leds as a greeter:
                flash_all_leds(
                    midir_out,
                    mixer_map,
                );
            },
        };
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn switch_led(
    event_led: u8,
    midir_out: Rc<RefCell<MidiOutputConnection>>,
    mixer_map: &MixerMap,
    state: LedState,
) {
    let state: u8 = match
        state
    {
        LedState::Active => mixer_map
            .button_state_active,
        LedState::Mute => mixer_map
            .button_state_mute,
        LedState::Off => mixer_map
            .button_state_off,
        LedState::Flash => panic!("!! Problem switching LEDs. LedState:Flash used."), // fix!!!
        LedState::Invalid => panic!("!! Problem switching LEDs. LedState:Invalid used."), // fix!!!
    };
    // let mut midir_out_borrow: RefMut<MidiOutputConnection> = midir_out.borrow_mut();
    // Send midi message:
    match
        midir_out
            .borrow_mut()
            .send(
                &[
                    MESSAGE_BUTTON,
                    event_led,
                    state,

                ]
            )
    {
        Err(error) => panic!("\n!! Problem sending midi message:\n error: {error:?}"), // start midi selection option again?
        Ok(_) => (),
    };
}

pub fn switch_all_leds(
    led_state: LedState,
    midir_out: Rc<RefCell<MidiOutputConnection>>,
    mixer_map: &MixerMap,
) {
    match
        led_state
    {
        LedState::Off => {
            switch_all_led_types(
                mixer_map
                    .buttons_active,
                LedState::Off,
                midir_out
                    .clone(),
                mixer_map,
                0,
            );
            switch_all_led_types(
                mixer_map
                    .buttons_mute,
                LedState::Off,
                midir_out,
                mixer_map,
                0,
            );
        }
        LedState::Flash => {
            switch_all_led_types(
                mixer_map
                    .buttons_active,
                LedState::Active,
                midir_out
                    .clone(),
                mixer_map,
                SLEEP_MILLIS_FLASH_LEDS,
            );
            switch_all_led_types(
                mixer_map
                    .buttons_mute,
                LedState::Mute,
                midir_out
                    .clone(),
                mixer_map,
                SLEEP_MILLIS_FLASH_LEDS,
            );
            thread::sleep(time::Duration::from_millis(SLEEP_MILLIS_FLASH_LEDS));
            switch_all_led_types(
                mixer_map
                    .buttons_active,
                LedState::Off,
                midir_out
                    .clone(),
                mixer_map,
                SLEEP_MILLIS_FLASH_LEDS,
            );
            switch_all_led_types(
                mixer_map
                    .buttons_mute,
                LedState::Off,
                midir_out,
                mixer_map,
                SLEEP_MILLIS_FLASH_LEDS,
            );
        }
        LedState::Active => {
            switch_all_led_types(
                mixer_map
                    .buttons_active,
                LedState::Active,
                midir_out,
                mixer_map,
                0,
            );
        }
        LedState::Mute => {
            switch_all_led_types(
                mixer_map
                    .buttons_mute,
                LedState::Mute,
                midir_out,
                mixer_map,
                0,
            );
        }
        LedState::Invalid => (),
    }
}

pub fn switch_all_led_types(
    buttons: [u8; 8],
    led_state: LedState,
    midir_out: Rc<RefCell<MidiOutputConnection>>,
    mixer_map: &MixerMap,
    sleep_millis: u64,
) {
    for
        led
    in
        buttons
    {
        switch_led(
            led,
            midir_out
                .clone(),
            mixer_map,
            led_state,
        );
        thread::sleep(
            time::Duration::from_millis(
                sleep_millis
            )
        );
    };
}

pub fn flash_all_leds(
    midir_out: Rc<RefCell<MidiOutputConnection>>,
    mixer_map: &MixerMap,
) {
    switch_all_leds(
        LedState::Flash,
        midir_out,
        mixer_map,
    );
}

// ----------------------------------------------------------------------------

pub fn handle_stream_leds(
    mapping: Mapping,
    midir_out: Rc<RefCell<MidiOutputConnection>>,
    mixer_channel: u8,
    mute: LedState,
) {
    // Switch on active led of channel:
    let map_channel_to_active: HashMap<u8, u8> = mapping.channel_maps.clone().sliders_to_buttons.active;
    let event_led_active: u8 = match map_channel_to_active.get(&mixer_channel) {
        Some(led) => led.to_owned(),
        None => panic!(
            "\n!! Problem getting LED:\n mixer_channel: {:?}\n map: {:?}",
            mixer_channel, map_channel_to_active,
        ),
    };
    switch_led(
        event_led_active,
        midir_out
            .clone(),
        &mapping
            .mixer_map,
        LedState::Active,
    );
    // Switch mute led:
    let map_channel_to_mute: HashMap<u8, u8> = mapping.channel_maps.sliders_to_buttons.mute;
    // println!("{:?} - {:?}", map_channel_to_mute, mixer_channel);
    let event_led_mute: u8 = match map_channel_to_mute.get(&mixer_channel) {
        Some(led) => led.to_owned(),
        None => panic!(
            "\n!! Problem getting LED:\n mixer_channel: {:?}\n map: {:?}",
            mixer_channel, map_channel_to_mute,
        ),
    };
    switch_led(
        event_led_mute,
        midir_out
            .clone(),
        &mapping
            .mixer_map,
        mute,
    );
}

pub fn removed_stream_leds(
    mapping: Mapping,
    midir_out: Rc<RefCell<MidiOutputConnection>>,
    mixer_channel: u8,
) {
    let map_channel_to_active: HashMap<u8, u8> = mapping.channel_maps.clone().sliders_to_buttons.active;
    let event_led_active: u8 = match map_channel_to_active.get(&mixer_channel) {
        Some(led) => led.to_owned(),
        None => panic!(
            "\n!! Problem getting LED:\n mixer_channel: {:?}\n map: {:?}",
            mixer_channel, map_channel_to_active,
        ),
    };
    switch_led(
        event_led_active,
        midir_out
            .clone(),
        &mapping
            .mixer_map,
        LedState::Off,
    );
    let map_channel_to_mute: HashMap<u8, u8> = mapping.channel_maps.sliders_to_buttons.mute;
    let event_led_mute: u8 = match map_channel_to_mute.get(&mixer_channel) {
        Some(led) => led.to_owned(),
        None => panic!(
            "\n!! Problem getting LED:\n mixer_channel: {:?}\n map: {:?}",
            mixer_channel, map_channel_to_mute,
        ),
    };
    switch_led(
        event_led_mute,
        midir_out
            .clone(),
        &mapping
            .mixer_map,
        LedState::Off,
    );
}
