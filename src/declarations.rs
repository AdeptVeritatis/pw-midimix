
// Constants:
pub const OFFSET_BUTTON_ACTIVE: u64 = 2000000; // in usec
pub const OFFSET_SLIDER: u64 = 40000; //10000; // in usec
pub const SLEEP_MILLIS_FLASH_LEDS: u64 = 40; // in msec
pub const SLEEP_MILLIS_RELOAD_PORTS: u64 = 100; // in msec
pub const TAB_SIZE: usize = 16;

// ----------------------------------------------------------------------------

pub const MEDIA_NAME_MPV: &str = "mpv";
pub const MEDIA_NAME_TWITCH: &str = "https://twitch.tv/";

// ----------------------------------------------------------------------------

// Constants - midir:
pub const MESSAGE_BUTTON: u8 = 144; // Note on
pub const MESSAGE_RELEASE: u8 = 128; // Note off
pub const MESSAGE_SLIDER: u8 = 176; // Controller

pub const MIDIR_MIDIMIX_NAME: &str = "MIDI Mix:MIDI Mix MIDI 1";
pub const MIDIR_NODE_IN: &str = "pw-midimix"; // ???
pub const MIDIR_NODE_OUT: &str = "pw-midimix"; // ???
pub const MIDIR_PORT_IN: &str = "pw-midimix in";
pub const MIDIR_PORT_OUT: &str = "pw-midimix out";
