
use std::{
    error,
    fmt,
};

// ----------------------------------------------------------------------------

#[derive(Debug)]
pub enum DumpError {
    EmptyString,
    InvalidObject,
    NotArray,
}

impl error::Error for DumpError {}

impl fmt::Display for DumpError {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        match
            *self
        {
            DumpError::EmptyString => write!(
                f,
                "empty string from dump",
            ),
            DumpError::InvalidObject => write!(
                f,
                "invalid object in dump",
            ),
            DumpError::NotArray => write!(
                f,
                "is not an array",
            ),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug)]
pub enum GetObjectError {
    ChannelFromID,
    // IdFromProps,
    // GetStreamNode,
    // GetStreamPort,
}

impl error::Error for GetObjectError {}

impl fmt::Display for GetObjectError {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        match
            *self
        {
            GetObjectError::ChannelFromID => write!(
                f,
                "id not in map_id_to_channel",
            ),
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug)]
pub enum HandlePortsError {
    NoStreamNode,
    UnknownSourceNode,
    WrongPortPosition,
}

impl error::Error for HandlePortsError {}

impl fmt::Display for HandlePortsError {
    fn fmt(
        &self,
        f: &mut fmt::Formatter,
    ) -> fmt::Result {
        match
            *self
        {
            HandlePortsError::NoStreamNode => write!(
                f,
                "StreamNode not found",
            ),
            HandlePortsError::UnknownSourceNode => write!(
                f,
                "StreamNode.node different from source node",
            ),
            HandlePortsError::WrongPortPosition => write!(
                f,
                "LinkSide from port does not match",
            ),
        }
    }
}
