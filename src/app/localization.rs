
// Will be replaced by fluent.
// String constants:
pub const ACTIVE_CHANNEL: &str = "active channel";
pub const ACTIVE_METHOD: &str = "active method";
pub const ACTIVE_STREAMS: &str = "active streams";
pub const AUTOSELECTING: &str = "autoselecting";
pub const AVAILABLE_INPUT_PORTS: &str = "Available input ports";
pub const AVAILABLE_OUTPUT_PORTS: &str = "Available output ports";
pub const CHANNEL_VOLUME: &str = "channel volume";
pub const CONNECTED_IN: &str = "connected in";
pub const CONNECTED_OUT: &str = "connected out";
pub const DEVICE_REMOVED: &str = "device removed";
pub const DONE: &str = "done";
pub const ERROR: &str = "Error";
pub const GREETER: &str = "greeter";
pub const LEDS: &str = "LEDs";
pub const MAIN_DEVICE: &str = "main device";
pub const MAIN_VOLUME: &str = "main volume";
pub const MAP_DIRECTION: &str = "map direction";
pub const MEDIA_TITLES: &str = "media titles";
pub const MOVE_CHANNEL: &str = "move channel";
pub const MUTE_STATUS: &str = "mute status";
pub const NEW_DEVICE: &str = "new device";
pub const NEW_MIDI_IN: &str = "new midi in";
pub const NEW_MIDI_NODE: &str = "new midi node";
pub const NEW_MIDI_OUT: &str = "new midi out";
pub const NEW_SINK: &str = "new sink";
pub const NEW_STREAM: &str = "new stream";
pub const SELECT_INPUT: &str = "Select input";
pub const SELECT_OUTPUT: &str = "Select output";
pub const SINK_CONNECTED: &str = "sink connected";
pub const SINK_REMOVED: &str = "sink removed";
pub const SINK_SWITCHED: &str = "sink switched";
pub const STREAM_REMOVED: &str = "stream removed";
pub const SWAP_CHANNELS: &str = "swap channels";
pub const USING_MAP: &str = "using map";
pub const WARNING: &str = "Warning";

pub const MIDI_FEEDBACK_WARNING: &str = "quitting to avoid feedback loops";
pub const MIDI_FEEDBACK_HINT: &str = "try to start with --select";
