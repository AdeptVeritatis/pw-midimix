
use crate::declarations::TAB_SIZE;
use std::{
    cell::RefCell,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// Print state:
#[derive(Debug, Clone, PartialEq)]
pub enum PrintState {
    Channel,
    Main,
    Off,
}

// ----------------------------------------------------------------------------

// PrintTab:
// All data for printing and correct aligning.
#[derive(Debug, Clone)]
pub struct PrintTab {
    pub full_str: String,
    pub prefix_len: usize,
    pub prefix_str: String,
    // pub suffix_len: usize, // never read
    pub suffix_str: String,
}

impl Default for PrintTab {
    // Create the initial PrintTab object:
    fn default() -> Self {
        let full_str: String = " "
            .repeat(
                TAB_SIZE + 3
            );
        let prefix_len: usize = match
            TAB_SIZE
                .to_string()
                .parse::<f32>()
        {
            Err(error) => {
                println!("!! print_tab error: {error}");
                3
            },
            Ok(num) => {
                // (x+3-1)/2-1 -> x / 2;
                (num / 2.0)
                    .round()
                    as usize

                // match
                //     (num / 2.0)
                //         .round()
                //         .to_string()
                //         .parse::<usize>()
                // {
                //     Ok(len) => len,
                //     Err(error) => {
                //         println!("!! print_tab error: {error}");
                //         3
                //     }
                // }
            },
        };
        let prefix_str: String = " "
            .repeat(
                prefix_len
            );
        let suffix_len: usize = TAB_SIZE - prefix_len + 2; // x+3-1;
        let suffix_str: String = " "
            .repeat(
                suffix_len
            );

        Self {
            full_str,
            prefix_len,
            prefix_str,
            // suffix_len,
            suffix_str,
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

// Print a NL after print!("\r") was used:
pub fn next_line(
    next_line: Rc<RefCell<PrintState>>,
) {
    if
        next_line
            .borrow()
            .to_owned()
        !=
        PrintState::Off
    {
        println!();
        next_line
            .replace(
                PrintState::Off
            );
    }
}

// Create spaces depending on string length:
// If string is too long, continue on same line nonetheless.
pub fn tab_filler(
    print_string: &str,
) -> String {
    match
        print_string
            .len()
    {
        len
            if
                TAB_SIZE
                >
                len + 1
        => " "
            .repeat(
                TAB_SIZE - len - 1
            ),
        _ => String::from(" "),
    }
}

// Create spaces and NLs depending on string length:
// pub fn _tab_sizer(
//     print_string: &str,
// ) -> String {
//     match
//         print_string
//             .len()
//     {
//         len
//             if
//                 TAB_SIZE
//                 >
//                 len + 1
//         => " "
//             .repeat(
//                 TAB_SIZE - len - 1
//             ),
//         _ => format!(
//             "\n   {}",
//             " "
//                 .repeat(
//                     TAB_SIZE
//                 ),
//         ),
//     }
// }
