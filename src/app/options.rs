
use crate::{
    app::{
        help::Help,
        localization::{
            ACTIVE_METHOD,
            GREETER,
            LEDS,
            MAP_DIRECTION,
            MEDIA_TITLES,
            USING_MAP,
        },
        print::tab_filler,
        App,
        AppStatus,
    },
    declarations::OFFSET_BUTTON_ACTIVE,
    hardware::{
        MixerMapName,
        MIXER_MAP_APC_KEY_25,
        MIXER_MAP_MIDIMIX,
        MIXER_MAP_VMPK,
    },
    impls::midir_impl::{
        create_midi_port_in,
        create_midi_port_out,
        list_midi_ports_in,
        list_midi_ports_out,
    },
};
use midir::{
    MidiInput,
    MidiOutput,
};

// ----------------------------------------------------------------------------

// ActiveMethod:
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ActiveMethod {
    Combination,
    Timer,
}

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct AppOptions {
    pub active_method: ActiveMethod,
    pub channels_reversed: bool,
    pub greeter_off: bool,
    pub leds_off: bool,
    pub map_string: String,
    pub ports_select: bool,
    pub title_off: bool,
}

impl Default for AppOptions {
    fn default() -> Self {
        Self {
            active_method: ActiveMethod::Combination,
            channels_reversed: false,
            greeter_off: false,
            leds_off: false,
            map_string: String::from(MIXER_MAP_MIDIMIX),
            ports_select: false,
            title_off: false,
        }
    }
}

// Arguments:
impl AppOptions {
    pub fn handle_arguments_single(
        &mut self,
        app: &mut App,
        mut args: Vec<String>,
    ) -> Vec<String> {
        // println!(" args: {:?}", args);
        let mut removed_counter: usize = 0;
        for
            (
                pos,
                arg,
            )
        in
            args
                .clone()
                .iter()
                .enumerate()
        {
            // let program_status: AppStatus =
            match
                arg
                    .as_str()
            {
                // Options which end in AppStatus::Quit:
                // Removing option from vec would prevent invalid option and char detection in handle_arguments_multi()!
                // Break from for construct prevents '-i -o' combinations! But that makes '-d -h -i -o' possible!
                // Print help option:
                "h" | "-h" | "--help" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    Help::show();
                    app.state = AppStatus::Quit;
                },
                "d" | "-d" | "--devices" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    // Show available device names for map option:
                    // println!();
                    println!("List of device names for --map <name>:");
                    println!("                   {:?}", MIXER_MAP_APC_KEY_25,);
                    // println!("                                  {:?}", MIXER_MAP_MIDIMIX); // other spacing?
                    println!("                   {:?}", MIXER_MAP_MIDIMIX);
                    println!("                   {:?}", MIXER_MAP_VMPK);
                    println!();
                    app.state = AppStatus::Quit;
                },
                "i" | "-i" | "--inputs" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    // Show midi inputs:
                    let midir_input: MidiInput = create_midi_port_in();
                    // println!();
                    list_midi_ports_in(
                        midir_input,
                        app
                            .print_tab
                            .clone(),
                    );
                    println!();
                    app.state = AppStatus::Quit;
                },
                "io" | "-io" | "oi" | "-oi" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    // Show midi in- and outputs:
                    let midir_input: MidiInput = create_midi_port_in();
                    let midir_output: MidiOutput = create_midi_port_out();
                    // println!();
                    list_midi_ports_in(
                        midir_input,
                        app
                            .print_tab
                            .clone(),
                    );
                    println!();
                    list_midi_ports_out(
                        midir_output,
                        app
                            .print_tab
                            .clone(),
                    );
                    println!();
                    app.state = AppStatus::Quit;
                },
                "o" | "-o" | "--outputs" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    // Show midi outputs:
                    let midir_output: MidiOutput = create_midi_port_out();
                    // println!();
                    list_midi_ports_out(
                        midir_output,
                        app
                            .print_tab
                            .clone()
                    );
                    println!();
                    app.state = AppStatus::Quit;
                },
                // Options which can be combined:
                // Keep it here to catch the --long_arguments.
                "a" | "-a" | "--active-timer" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    self.active_method = ActiveMethod::Timer;
                },
                "g" | "-g" | "--greeter-off" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    self.greeter_off = true;
                },
                "l" | "-l" | "--leds-off" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    self.leds_off = true;
                },
                "r" | "-r" | "--reversed" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    self.channels_reversed = true;
                },
                "s" | "-s" | "--select" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    self.ports_select = true;
                },
                "t" | "-t" | "--title-off" => {
                    (args, removed_counter) = remove_argument(
                        args,
                        pos,
                        removed_counter,
                    );
                    self.title_off = true;
                },
                _ => (),
            };
        };
        app.options = self.to_owned();
        // println!(" args: {:?}", args);
        args
    }

    pub fn handle_arguments_multi(
        &mut self,
        app: &mut App,
        args: Vec<String>,
    ) {
        let mut found_next_map: bool = false;
        for argument in args {
            match
                found_next_map
            {
                true => {
                    // Read 2nd part of selected map option (-m <map>):
                    self.map_string = argument;
                }
                false => {
                    // Check for incorrect --long_arguments:
                    match
                        argument
                            .contains(
                                "--"
                            )
                    {
                        false => {
                            // Check for (in)valid chars and set options:
                            match
                                self
                                    .check_chars(
                                        argument
                                            .clone(),
                                    )
                            {
                                // Contains only valid chars and returns map_option:
                                (true, map_option) => {
                                    found_next_map = map_option;
                                },
                                // Contains invalid chars:
                                (false, _) => {
                                    println!("!! argument contains invalid char: {argument:?}\n");
                                    // Print help and quit:
                                    Help::show();
                                    app.state = AppStatus::Quit;

                                    break;
                                }
                            }
                        },
                        true => {
                            println!("!! argument contains invalid option: {argument:?}\n");
                            // Print help and return AppStatus::Quit:
                            Help::show();
                            app.state = AppStatus::Quit;

                            break;
                        },
                    }
                }
            };
        };
        app.options = self
            .to_owned();
    }
}

// Options:
impl AppOptions {
    pub fn show_options(
        &self,
        app: &mut App,
    ) {
        // not bool but own types!!!
        // Get mixer_maps from found_map_option:
        match
            self
                .map_string
                .as_str()
        {
            MIXER_MAP_APC_KEY_25 => {
                app.mixer_map_name = MixerMapName::APCKey25;
            },
            MIXER_MAP_MIDIMIX => {
                app.mixer_map_name = MixerMapName::MIDImix;
            },
            MIXER_MAP_VMPK => {
                app.mixer_map_name = MixerMapName::VMPK;
            },
            string => {
                println!("!! unknown mapping argument used: {string:?}");
                println!();
                println!("Try --help or --devices for more infos.");
                println!();

                app.state = AppStatus::Quit;
            }
        };
    // Print options:
        match
            app
                .state
        {
            AppStatus::Quit => (),
            AppStatus::Running => {
                // Print selected map option:
                // println!();
                println!(
                    "<> {USING_MAP}:{tab}{name:?}",
                    name = app
                        .mixer_map_name,
                    tab = tab_filler(USING_MAP),
                );
                // Get direction of mapping:
                // Print direction option:
                match
                    self
                        .channels_reversed
                {
                    true => {
                        println!(
                            "<> {MAP_DIRECTION}:{tab}reversed",
                            tab = tab_filler(MAP_DIRECTION),
                        );
                        println!(
                            "{tab}from right to left",
                            tab = app
                                .print_tab
                                .full_str,
                        );
                    },
                    false => {
                        println!(
                            "<> {MAP_DIRECTION}:{tab}default",
                            tab = tab_filler(MAP_DIRECTION),
                        );
                        println!(
                            "{tab}from left to right",
                            tab = app
                                .print_tab
                                .full_str,
                        );
                    },
                };
                // Print selected greeter option:
                if
                    self
                        .active_method
                    ==
                    ActiveMethod::Timer
                {
                    println!(
                        "<> {ACTIVE_METHOD}:{tab}2nd button press is timer based\n{tab_full}within a time frame of {seconds} s ({milliseconds} ms)\n{tab_full}!! not available at the moment !!",
                        tab = tab_filler(ACTIVE_METHOD),
                        tab_full = app
                            .print_tab
                            .full_str,
                        seconds = OFFSET_BUTTON_ACTIVE / 1000000,
                        milliseconds = OFFSET_BUTTON_ACTIVE / 1000,
                    );
                };
                // Print selected greeter option:
                if
                    self
                        .greeter_off
                {
                    println!(
                        "<> {GREETER}:{tab}off",
                        tab = tab_filler(GREETER),
                    );
                };
                // Print selected leds option:
                if
                    self
                        .leds_off
                {
                    println!(
                        "<> {LEDS}:{tab}off",
                        tab = tab_filler(LEDS),
                    );
                };
                // Print selected title option:
                if
                    self
                        .title_off
                {
                    println!(
                        "<> {MEDIA_TITLES}:{tab}off",
                        tab = tab_filler(MEDIA_TITLES),
                    );
                };
            },
        };
    }

    fn check_chars(
        &mut self,
        argument: String,
    ) -> (bool, bool) {
        // Check for (in)valid chars and set options:
        let check_result: bool = argument
            .chars()
            // All chars must return true or it will return false:
            .all(|char| {
                match char {
                    // Check for single '-' to ignore it:
                    '-' => true, // cut it before?
                    // Check for combinable arguments:
                    'a' => {
                        self.active_method = ActiveMethod::Timer;
                        true
                    }
                    'g' => {
                        self.greeter_off = true;
                        true
                    }
                    'l' => {
                        self.leds_off = true;
                        true
                    }
                    'm' => true,
                    'r' => {

                        self.channels_reversed = true;
                        true
                    }
                    's' => {
                        self.ports_select = true;
                        true
                    }
                    't' => {
                        self.title_off = true;
                        true
                    }
                    // Check for the rest:
                    _ => {
                        println!("!! invalid char: {:?}", char,);
                        false
                    }
                }
            });
        // Check for map argument to prevent checking for chars in next argument:
        let map_option: bool = argument
            .contains(
                "m"
            );

        (
            check_result,
            map_option,
        )
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

fn remove_argument(
    mut args: Vec<String>,
    pos: usize,
    mut removed_counter: usize,
) -> (
    Vec<String>,
    usize,
) {
    args
        .remove(
            pos - removed_counter
        );
    removed_counter += 1;
    (
        args,
        removed_counter,
    )
}
