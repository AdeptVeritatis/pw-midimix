
pub struct Help {}

impl Help {
    pub fn show(
    ) {
        //     println!();
        //     println!("pw-midimix [options] [FILE]:");
        // println!();
        println!("pw-midimix is a wrapper for events from a MIDI mixer to pipewire. ");
        println!("Detected audio streams are assigned to a channel of the mixer");
        println!("to control volume, mute status and the output sink.");
        println!("It is optimized for the \"AKAI MIDImix\".");
        println!();
        println!("pw-midimix [option]:");
        //     println!();
        //     println!("  -a, --auto <name>               Mixer device autoconnect");
        println!("  -a, --active-timer              Timer based move / swap channels");
        println!("  -m, --map <name>                Mixer device mapping");
        println!("  -r, --reversed                  Channel mapping reversed");
        println!("  -s, --select                    Use MIDI I/O selection menu");
        println!();
        println!("  -g, --greeter-off               Do NOT show LED greeter on startup");
        println!("  -l, --leds-off                  Do NOT use LEDs");
        println!("  -t, --title-off                 Do NOT show media titles");
        println!();
        println!("  -d, --devices                   List mixer device names");
        println!("  -i, --inputs                    List midi input ports");
        println!("  -o, --outputs                   List midi output ports");
        println!();
        println!("  -h, --help                      Print this help");
        println!();
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
