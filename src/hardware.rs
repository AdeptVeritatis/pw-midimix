// All in one place and not spread in declarations:

// Don't forget to change main.rs > midir_start()!
// And change '--devices' in declarations/help.rs, too.

pub mod button;
pub mod led;
pub mod mapping;
pub mod midi;
pub mod slider;

// ----------------------------------------------------------------------------

pub const MIXER_MAP_APC_KEY_25: &str = "apc-key-25";
pub const MIXER_MAP_MIDIMIX: &str = "midimix";
pub const MIXER_MAP_VMPK: &str = "vmpk";

// ----------------------------------------------------------------------------

#[derive(Debug, Clone, Copy)]
pub enum MixerMapName {
    APCKey25,
    MIDImix,
    VMPK,
}

// ----------------------------------------------------------------------------

// MixerMap:
// Map for all mixer elements. Is generated from mapping option.
// Only needed in the beginning to collect all different mappings.
// _state_ are the different types of leds and their colours depending on the device.
#[derive(Debug, Clone)]
pub struct MixerMap {
    pub name: MixerMapName,
    pub button_device_next: u8, // BANK LEFT
    pub button_device_prev: u8, // BANK RIGHT
    pub button_shift: u8,       // SOLO
    pub button_state_active: u8,
    pub button_state_mute: u8,
    pub button_state_off: u8,
    pub buttons_active: [u8; 8], // REC ARM
    pub buttons_mute: [u8; 8],   // MUTE
    pub buttons_sink: [u8; 8],   // SOLO + MUTE
    pub channels_num: [u8; 8],   // display name
    pub slider_master: u8,
    pub slider_channels: [u8; 8],
}

impl MixerMap {
    pub fn new(
        name: MixerMapName,
    ) -> Self {
        match
            name
        {
            // APC Key 25 - notes & controller numbers:
            MixerMapName::APCKey25 => Self {
                name,
                button_device_next: 64, // Arrow up
                button_device_prev: 65, // Arrow down
                button_shift: 98,       // SHIFT
                // led state: 0=off, 1=green, 3=red, 5=yellow (+1=blink)
                button_state_active: 3,
                button_state_mute: 5,
                button_state_off: 0,
                buttons_active: [24, 25, 26, 27, 28, 29, 30, 31], // 2nd row
                buttons_mute: [32, 33, 34, 35, 36, 37, 38, 39],   // 1st row
                buttons_sink: [16, 17, 18, 19, 20, 21, 22, 23],   // 3rd row
                channels_num: [1, 2, 3, 4, 5, 6, 7, 8],           // display name
                slider_master: 0,                                 // not available?
                slider_channels: [48, 49, 50, 51, 52, 53, 54, 55], // knobs
            },
            // MIDImix - notes & controller numbers:
            MixerMapName::MIDImix => Self {
                name,
                button_device_next: 25, // BANK LEFT
                button_device_prev: 26, // BANK RIGHT
                button_shift: 27,       // SOLO
                // led state: 0=off, 1-127=on
                // button_state_sink: 0,
                button_state_active: 1,
                button_state_mute: 1,
                button_state_off: 0,
                // button_state_off: 64,
                buttons_active: [3, 6, 9, 12, 15, 18, 21, 24], // REC ARM
                buttons_mute: [1, 4, 7, 10, 13, 16, 19, 22],   // MUTE
                buttons_sink: [2, 5, 8, 11, 14, 17, 20, 23],   // SOLO + MUTE
                channels_num: [1, 2, 3, 4, 5, 6, 7, 8],        // display name
                slider_master: 62,
                slider_channels: [19, 23, 27, 31, 49, 53, 57, 61],
            },
            // VMPK - notes & controller numbers:
            // D-notes are active channels
            // C#-notes are mutes
            // D#-notes are sink functions
            // only main volume available, no channel volumes
            MixerMapName::VMPK => Self {
                name,
                button_device_next: 11, // B0
                button_device_prev: 9,  // A0
                button_shift: 10,       // A#0
                // led state: 0=off, 127=on // 127 or 1???
                button_state_active: 127, // led switches off again, when button pressed
                button_state_mute: 127,
                button_state_off: 0,
                buttons_active: [14, 26, 38, 50, 62, 74, 86, 98], // D0, D1, ... D7
                buttons_mute: [13, 25, 37, 49, 61, 73, 85, 97],   // C#0, C#1, ... C#7
                buttons_sink: [15, 27, 39, 51, 63, 75, 87, 99],   // D#0, D#1, ... D#7
                channels_num: [1, 2, 3, 4, 5, 6, 7, 8],           // display name
                slider_master: 1,                                 // Value (modulation)
                slider_channels: [1, 2, 3, 4, 5, 6, 7, 8],        // not available! (needs bogus values)
            },
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
