
use crate::{
    app::{
        localization::{
            AVAILABLE_INPUT_PORTS,
            AVAILABLE_OUTPUT_PORTS,
            CONNECTED_IN,
            CONNECTED_OUT,
        },
        print::{
            PrintTab,
            tab_filler,
        },
        AppStatus,
    },
    declarations::{
        MESSAGE_BUTTON,
        MESSAGE_RELEASE,
        MESSAGE_SLIDER,
        MIDIR_NODE_IN,
        MIDIR_NODE_OUT,
        MIDIR_PORT_IN,
        MIDIR_PORT_OUT,
    },
    hardware::midi::{
        MessageClass,
        MidiMessage,
    },
};
use midir::{
    Ignore,
    MidiInput,
    MidiInputConnection,
    MidiInputPort,
    MidiOutput,
    MidiOutputConnection,
    MidiOutputPort,
};
use pipewire::channel::Sender;
use std::io::{
    stdin,
    stdout,
    Write,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn create_midi_port_in() -> MidiInput {
    // Create midir input port:
    match
        MidiInput::new(
            MIDIR_NODE_IN
        )
    {
        Err(error) => panic!("\n!! problem creating midi input:\n   error: {error:?}"),
        Ok(mut port) => {
            port
                .ignore(
                    Ignore::None
                );
            port
        },
    }
}

pub fn create_midi_port_out() -> MidiOutput {
    // Create midir output port:
    match
        MidiOutput::new(
            MIDIR_NODE_OUT
        )
    {
        Err(error) => panic!("\n!! problem creating midi output:\n   error: {error:?}"),
        Ok(port) => port,
    }
}

pub fn list_midi_ports_in(
    midir_input: MidiInput,
    print_tab: PrintTab,
) -> (
    MidiInput,
    Vec<MidiInputPort>,
) {
    // println!();
    println!("!! {AVAILABLE_INPUT_PORTS}:");
    let midir_input_ports: Vec<MidiInputPort> = midir_input
        .ports();
    for
        (
            channel,
            port,
        )
    in
        midir_input_ports
            .iter()
            .enumerate()
    {
        println!(
            "{tab_pre}{channel}{tab_suf}{name}",
            tab_pre = print_tab
                .prefix_str,
            tab_suf = print_tab
                .suffix_str,
            name = match
                midir_input
                    .port_name(
                        port
                    )
            {
                Err(error) => {
                    println!("\n!! problem reading port name:\n   error: {error:?}");
                    String::from("error")
                },
                Ok(name) => name,
            },
        );
    };

    (
        midir_input,
        midir_input_ports,
    )
}

pub fn list_midi_ports_out(
    midir_output: MidiOutput,
    print_tab: PrintTab,
) -> (
    MidiOutput,
    Vec<MidiOutputPort>,
) {
    // println!();
    println!("!! {AVAILABLE_OUTPUT_PORTS}:"); // Midir output port:
                                              // let midir_output = MidiOutput::new(MIDIR_NODE_OUT)?;
    let midir_output_ports: Vec<MidiOutputPort> = midir_output
        .ports();
    for
        (
            channel,
            port,
        )
    in
        midir_output_ports
            .iter()
            .enumerate()
    {
        println!(
            "{tab_pre}{channel}{tab_suf}{name}",
            tab_pre = print_tab
                .prefix_str,
            tab_suf = print_tab
                .suffix_str,
            name = match
                midir_output
                    .port_name(
                        port
                    )
            {
                Err(error) => {
                    println!("\n!! problem reading port name:\n   error: {error:?}");
                    String::from("error")
                },
                Ok(name) => name,
            },
        );
    };

    (
        midir_output,
        midir_output_ports,
    )
}

pub fn select_midi_port(
    ports_list_len: usize,
) -> (
    usize,
    AppStatus,
) {
    // Why need to flush stdout???
    match
        stdout()
            .flush()
    {
        Err(error) => panic!("\n!! problem flushing stout:\n  error: {error:?}"),
        Ok(_) => (),
    }
    // match stdin().lines().next() {} not working here!!! why???
    let mut select_input: String = String::new();
    // select_input
    //     .clear();
    match
        stdin()
            .read_line(
                &mut select_input
            )
    {
        Err(error) => panic!("\n!! problem reading number:\n   error: {error:?}"),
        Ok(_) => match
            select_input
                .trim()
        {
            // Quit programm:
            // LEDs are still off as midi isn't yet connected.
            "q" | "Q" | "\"q\"" | "'q'" | "`q`" | "quit" => {
                println!("\n.. quitting");
                (
                    0,
                    AppStatus::Quit,
                )
            }
            // Autoselect first element after MIDI through port, if no number is selected:
            "" => (
                1,
                AppStatus::Running
            ),
            // Parsing input:
            trimmed => match
                trimmed
                    .parse::<usize>()
            {
                // Is not a number, ask again:
                Err(error) => {
                    println!();
                    println!("!! Error reading number: {error:?}");
                    // println!("select_input: {:?}", select_input);
                    println!("!! Enter 'q' or press 'CTRL + c' to quit.");
                    print!(
                        "?? Please enter a valid number between 0 and {} [1]: ",
                        ports_list_len - 1,
                    );
                    select_midi_port(ports_list_len)
                }
                // Is a number:
                Ok(number) => match
                    number
                        .lt(
                            &ports_list_len
                        )
                {
                    // Number NOT valid, ask again:
                    false => {
                        println!();
                        println!("!! {number:?} is out of range.");
                        println!("!! Enter 'q' or press 'CTRL + c' to quit.");
                        print!(
                            "?? Please enter a valid number between 0 and {} [1]: ",
                            ports_list_len - 1,
                        );
                        select_midi_port(
                            ports_list_len
                        )
                    },
                    // Number is valid, use it:
                    true => (
                        number,
                        AppStatus::Running,
                    ),
                },
            },
        },
    }
}

pub fn connect_midi_port_in(
    midir_input: MidiInput,
    midir_in_ports: Vec<MidiInputPort>,
    print_tab: PrintTab,
    pw_sender: Sender<MidiMessage>,
    select_input: usize,
) -> MidiInputConnection<Sender<MidiMessage>> {
    let midir_in_port: &MidiInputPort = match
        midir_in_ports
            .get(
                select_input
            )
    {
        None => panic!("\n!! problem selecting port number:\n   input: {select_input:?}"),
        Some(port) => port,
    };
    let midir_in_name: String = match
        midir_input
            .port_name(
                midir_in_port
            )
    {
        Err(error) => panic!("\n!! problem getting port in name:\n   error: {error:?}"),
        Ok(name) => name,
    };
    // Connecting midi input:
    match
        midir_input
            .connect(
                midir_in_port,
                MIDIR_PORT_IN,
                |
                    stamp,
                    message,
                    sender,
                | {
                    // println!(" message: {:?}", message);
                    let class: MessageClass = match
                        message[0]
                    {
                        MESSAGE_BUTTON => MessageClass::Button,
                        MESSAGE_RELEASE => MessageClass::Release,
                        MESSAGE_SLIDER => MessageClass::Slider,
                        _ => MessageClass::Other,
                    };

                    let midi_message: MidiMessage = MidiMessage {
                        channel: message[1],
                        class,
                        stamp,
                        value: message[2],
                    };
                    match
                        sender
                            .send(
                                midi_message
                            )
                    {
                        Err(error) => panic!("\n!! problem sending midi message:\n   error: {error:?}"),
                        Ok(_) => (),
                    };
                },
                // Attaching pipewire sender to midi connection:
                pw_sender,
            )
    {
        Err(error) => panic!("\n!! problem connectiong midi input:\n   error: {error:?}"),
        Ok(connection) => {
            //     println!("\n>< connected in:   <- {}\n                   -> {}",
            println!(
                "\n>< {CONNECTED_IN}:{tab}-> {MIDIR_PORT_IN}",
                tab = tab_filler(CONNECTED_IN),
            );
            println!(
                "{tab}<- {midir_in_name}",
                tab = print_tab
                    .full_str,
            );

            connection
        },
    }
}

pub fn connect_midi_port_out(
    midir_io: MidiOutput,
    midir_ports: Vec<MidiOutputPort>,
    print_tab: PrintTab,
    select_input: usize,
) -> MidiOutputConnection {
    let midir_port: &str = MIDIR_PORT_OUT;
    let mixer_port: &MidiOutputPort = match
        midir_ports
            .get(
                select_input
            )
    {
        None => panic!("\n!! problem selecting port number:\n   input: {select_input:?}"),
        Some(port) => port,
    };
    let mixer_name: String = match
        midir_io
            .port_name(
                mixer_port
            )
    {
        Err(error) => panic!("\n!! problem getting port out name:\n   error: {error:?}"),
        Ok(name) => name,
    };
    match
        midir_io
            .connect(
                mixer_port,
                midir_port,
            )
    {
        Err(error) => panic!("\n!! problem connecting midi output:\n   error: {error:?}"),
        Ok(connection) => {
            println!(
                ">< {CONNECTED_OUT}:{tab}<- {midir_port}",
                tab = tab_filler(CONNECTED_OUT),
            );
            println!(
                "{tab}-> {mixer_name}",
                tab = print_tab
                    .full_str,
            );
            connection
        },
    }
}
