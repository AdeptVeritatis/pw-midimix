// move to objects.rs ???

use crate::{
    errors::GetObjectError,
    impls::pipewire_impl::objects::{
        link::LinkSide,
        node::stream::AudioStream,
        port::port_out::StreamPort,
    },
};
use pipewire::{
    // prelude::ReadableDict,
    // spa::ForeignDict,
    spa::utils::dict::DictRef,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn get_channel_from_id(
    id: u32,
    map_id_to_channel: Rc<RefCell<HashMap<u32, u8>>>,
) -> Result<u8, Box<dyn std::error::Error>> {
    match
        map_id_to_channel
            .borrow()
            .get(
                &id
            )
    {
        // All other ids:
        None => {
            // println!("!! Problem getting channel:\n    id:            {id:?}\n    map_id_to_channel:\n                   {:?}", map_id_to_channel.borrow());
            // 0 // bogus empty channel
            Err(
                Box::new(
                    GetObjectError::ChannelFromID
                )
            )
        },
        // Valid id:
        Some(channel) => Ok(
            channel
                .to_owned()
        ),
    }
}

pub fn get_id_from_props(
    object: &str,
    props: &DictRef,
) -> u32 {
    match
        props
            .get(
                object
            )
    {
        None => panic!("\n!! problem getting {object:?}:\n   props: {props:?}\n"),
        Some(answer) => match
            answer
                .parse::<u32>()
        {
            Err(error) => panic!("\n!! problem parsing {object:?}:\n   answer: {answer:?}\n   error: {error:?}\n"),
            Ok(id) => id,
        },
    }
}

pub fn get_stream_node(
    audio_streams: Rc<RefCell<HashMap<u8, AudioStream>>>,
    channel: u8,
) -> AudioStream {
    match
        audio_streams
            .borrow()
            .get(
                &channel
            )
    {
        None => panic!("\n!! Problem getting StreamNode from audio_streams.\n channel: {channel:?}\n audio_streams: {:?}\n", audio_streams.borrow()),
        Some(audio_stream) => audio_stream
            .to_owned(),
    }
}

pub fn get_stream_port(
    link_side: LinkSide,
    ports: HashMap<LinkSide, StreamPort>
) -> StreamPort {
    match
        ports
            .get(
                &link_side
            )
    {
        None => panic!("\n!! Problem getting StreamPort from ports.\n link_side: {link_side:?}\n ports: {ports:?}\n"),
        Some(stream_port) => stream_port
            .to_owned(),
    }
}
