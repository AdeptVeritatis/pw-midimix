
use crate::{
    app::{
        localization::MUTE_STATUS,
        print::{
            PrintState,
            PrintTab,
            next_line,
        },
    },
    hardware::led::LedState,
    impls::pipewire_impl::strings::{
        DUMP_PARAMS,
        NODE_PARAMS_MUTE,
        NODE_PARAMS_PROPS,
    },
};
use serde_json::Value;
use std::{
    cell::RefCell,
    process::{
        Command,
        Stdio,
    },
    rc::Rc,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn get_mute(info: Value, next_line_rc: Rc<RefCell<PrintState>>) -> LedState {
    // Get mute state from info:
    // "Spa:Pod:Object:Param:Props:mute"
    // println!(" info: {:#}", info);
    match info[DUMP_PARAMS][NODE_PARAMS_PROPS][0][NODE_PARAMS_MUTE].as_bool() {
        Some(true) => LedState::Mute,
        Some(false) => LedState::Off,
        None => {
            // better panic???
            next_line(next_line_rc);
            println!(
                "!! Problem with irregular or empty mute value.\n dump: {:#}",
                info,
            );
            LedState::Off
        }
    }
}

pub fn set_mute(
    channel_num: u8,
    event_node: u32,
    mute_state: bool,
    mute_string: String,
    next_line_rc: Rc<RefCell<PrintState>>,
    print_tab: PrintTab,
) -> Result<(), Box<dyn std::error::Error>> {
    match Command::new("pw-cli")
        .arg("set-param")
        .arg(&event_node.to_string())
        .arg("Props")
        .arg("{")
        .arg("mute:")
        .arg(&mute_state.to_string())
        .arg("}")
        // Use piped stdout to check result???
        .stdout(Stdio::piped())
        .spawn()
    {
        Ok(mut process) => match process.wait() {
            Ok(_) => {
                next_line(next_line_rc);
                println!("<> {MUTE_STATUS}:");
                println!(
                    "{tab_pre}{channel_num}{tab_suf}{mute_string}",
                    // "{tab_pre}{channel_num}{tab_suf}< {mute_string} >",
                    // "{tab_pre}{channel_num}{tab_suf}[ {mute_string} ]",
                    tab_pre = print_tab.prefix_str,
                    tab_suf = print_tab.suffix_str,
                );
            }
            Err(error) => panic!("\n!! Problem finishing command:\n error: {:?}", error,),
        },
        Err(error) => panic!(
            // need to panic?
            "\n!! Problem sending command:\n node: {:?}\n state: {:?}\n error: {:?}",
            event_node, mute_state, error,
        ),
    }
    Ok(()) // sending result? send result elsewhere?
}
