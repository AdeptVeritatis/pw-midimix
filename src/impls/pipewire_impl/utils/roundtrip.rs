
use pipewire::{
    core::{
        Core,
        Listener,
    },
    main_loop::MainLoop,
    spa::utils::result::AsyncSeq,
};
use std::{
    cell::Cell,
    rc::Rc,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn do_roundtrip(
    core: &Core,
    mainloop: MainLoop,
) {
    let done: Rc<Cell<bool>> = Rc::new(
        Cell::new(
            false
        )
    );
    let done_clone: Rc<Cell<bool>> = done
        .clone();
    let mainloop_clone: MainLoop = mainloop
        .clone();

    let pending: AsyncSeq = match
        core
            .sync(
                0
            )
    {
        Err(error) => panic!("\n!! problem syncing pipewire core:\n   error: {error:?}"),
        Ok(pending) => pending,
    };

    let _listener_core: Listener = core
        .add_listener_local()
        .done(
            move |
                id,
                seq,
            | {
                if
                    id
                    ==
                    pipewire::core::PW_ID_CORE

                    &&

                    seq
                    ==
                    pending
                {
                    done_clone
                        .set(
                            true
                        );
                    mainloop_clone
                        .quit();
                };
            },
        )
        .register();

    while
        !done
            .get()
    {
        // println!("mainloop started");
        mainloop
            .run();
    }
    // println!("mainloop done");
}
