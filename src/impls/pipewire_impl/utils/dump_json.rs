
use crate::{
    errors::DumpError,
    impls::pipewire_impl::strings::{
        DUMP_ID,
        DUMP_INFO,
    },
};
use serde_json::Value;
use std::process::Command;

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

// pub fn get_json_info(object: u32) -> Result<JsonValue, Box<dyn std::error::Error>> {
pub fn get_json_info(
    object: u32
) -> Result<Value, Box<dyn std::error::Error>> {
    // pub fn get_json_info(object: u32) {
    // Try out SPA methods???
    // Better check for serial than id? (https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/2875)???
    // Get dump from pw-dump:
    match
        Command::new(
            "pw-dump"
        )
            .arg(
                object
                    .to_string()
            )
            .output()
    {
        Err(error) => Err(
            Box::new(
                error
            )
        ),
        Ok(output) => {
            // Convert utf8 output to string:
            match
                String::from_utf8(
                    output
                        .stdout
                )
            {
                Err(error) => Err(
                    Box::new(
                        error
                    )
                ),
                Ok(dump_string) => {
                    // println!("{:#}", dump_string);
                    // Parse string:
                    match
                        dump_string
                            .is_empty()
                    {
                        true => Err(
                            Box::new(
                                DumpError::EmptyString
                            )
                        ),
                        false => {
                            match
                                serde_json::from_str::<Value>(
                                    dump_string
                                        .as_str()
                                )
                            {
                                Err(error) => Err(
                                    Box::new(
                                        error
                                    )
                                ),
                                Ok(value) => {
                                    // Check for id to get correct object from pw-dump:
                                    match
                                        value
                                            .as_array()
                                    {
                                        None => Err(
                                            Box::new(
                                                DumpError::NotArray
                                            )
                                        ),
                                        Some(array) => {
                                            for
                                                dump_object
                                            in
                                                array
                                            {
                                                match
                                                    dump_object
                                                        [DUMP_ID]
                                                        .as_u64()
                                                {
                                                    None => println!("!! Debug - no string found in array"),
                                                    Some(id)
                                                        if
                                                            id
                                                                .to_string()
                                                            ==
                                                            object
                                                                .to_string()
                                                    => {
                                                        return Ok(
                                                            dump_object
                                                                [DUMP_INFO]
                                                                .to_owned()
                                                        );
                                                    },
                                                    Some(_id) => {
                                                        // println!("!! Debug - other object found:\n   id: {id}");
                                                    },
                                                };
                                            };

                                            Err(
                                                Box::new(
                                                    DumpError::InvalidObject
                                                )
                                            )
                                        },
                                    }
                                },
                            }
                        },
                    }
                },
            }
        },
    }
}
