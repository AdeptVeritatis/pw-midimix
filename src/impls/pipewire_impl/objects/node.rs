pub mod sink;
pub mod stream;

use crate::{
    app::print::PrintTab,
    impls::pipewire_impl::{
        mainloop::mainloop_audio::MainLoopAudio,
        objects::node::{
            sink::AudioSink,
            stream::AudioStream,
        },
    },
    hardware::mapping::Mapping,
    impls::pipewire_impl::strings::{
        MEDIA_CLASS_AUDIO_SINK,
        MEDIA_CLASS_AUDIO_SOURCE_VIRTUAL,
        MEDIA_CLASS_MIDI_BRIDGE,
        MEDIA_CLASS_STREAM_OUTPUT_AUDIO,
        NODE_PROPS_MEDIA_CLASS,
    },
};
use pipewire::spa::utils::dict::DictRef;

// ----------------------------------------------------------------------------

pub struct NodeObj {}

impl NodeObj {
    pub fn get(
        global_id: u32,
        mainloop_audio: MainLoopAudio,
        mapping: Mapping,
        option_leds_off: bool,
        option_title_off: bool,
        print_tab: PrintTab,
        props: &DictRef,
    ) {
        match
            props
                .get(
                    NODE_PROPS_MEDIA_CLASS
                )
        {
            // Nodes without a media.class:
            // e.g. JACK nodes, Bitwig, easyeffects, ...
            // When adding it, don't forget that inputs may not be sinks!
            None => {
                // match props.get(NODE_PROPS_MEDIA_TYPE) {
                //     Some(MEDIA_TYPE_AUDIO) => {
                //         let node_name: String = match props.get(NODE_PROPS_NAME) {
                //                 None => String::from("unknown"),
                //                 Some(name) => name.to_string(),
                //             };
                //         println!(
                //             "!! New node found with {} {:?}:\n    id:            {}\n    name:          {}",
                //                 NODE_PROPS_MEDIA_TYPE, MEDIA_TYPE_AUDIO, global.id, node_name,
                //         );
                //         println!("!! Probably a DAW or other complex audio software using JACK.");
                //         println!("!! {node_name} is NOT added to the channels list of the mixer!");
                //     },
                //     _ => (),
                // }
            },
            // Audio sink:
            Some(MEDIA_CLASS_AUDIO_SINK) => {
                // println!("Sink: {:?}", global);
                // println!("Sink props: {:?}", props);
                AudioSink::check(
                    global_id,
                    mainloop_audio,
                    props,
                );
            },
            // Audio stream output:
            Some(MEDIA_CLASS_STREAM_OUTPUT_AUDIO) => {
                // println!("Stream out: {:?}", global);
                // println!("Stream out props: {:?}", props);
                AudioStream::check(
                    global_id,
                    mainloop_audio
                        .clone(),
                    mapping
                        .clone(),
                    option_leds_off,
                    option_title_off,
                    print_tab
                        .clone(),
                );
            },
            // Placeholder for other nodes:
            Some(MEDIA_CLASS_AUDIO_SOURCE_VIRTUAL) => (),
            Some(MEDIA_CLASS_MIDI_BRIDGE) => (),
            // Node with unknown media.class:
            Some(_) => {
                // Some(media_class) => {
                // should be some kind of debugging ...!!!
                // println!(
                //     "!! New node with unhandled {} {:?}:\n    id:            {}\n    name:          {}",
                //     NODE_PROPS_MEDIA_CLASS,
                //     media_class,
                //     global.id,
                //     match props
                //         .get(
                //             NODE_PROPS_NAME
                //         ){
                //             Some(name) => name,
                //             None => "unknown",
                //         },
                // );
            },
        };
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
