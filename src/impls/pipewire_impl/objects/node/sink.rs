
use crate::{
    app::{
        localization::{
            NEW_SINK,
            SINK_REMOVED,
        },
        print::{
            PrintState,
            next_line,
            tab_filler,
        },
    },
    impls::pipewire_impl::{
        mainloop::mainloop_audio::MainLoopAudio,
        objects::ObjectClass,
        strings::{
            NODE_PROPS_DESCRIPTION,
            NODE_PROPS_NICK,
        },
    },
};
use pipewire::spa::utils::dict::DictRef;
use std::{
    cell::{
        RefCell,
        RefMut,
    },
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Debug, Clone)]
pub struct AudioSink {
    pub id_node: u32,
    pub name: String,
    pub port_left: u32,
    pub port_right: u32,
}

impl AudioSink {
    pub fn new(
        id_node: u32,
        props: &DictRef,
    ) -> Self {
        let name: String = match
            props
                .get(
                    NODE_PROPS_NICK
                )
        {
            None => match
                props
                    .get(
                        NODE_PROPS_DESCRIPTION
                    )
            {
                None => String::from("error"),
                Some(description) => description
                    .to_string(),
            },
            Some(nick) => nick
                .to_string(),
        };

        Self {
            id_node,
            name,
            port_left: 0,
            port_right: 0,
        }
    }
}

impl AudioSink {
    pub fn check(
        id_node: u32,
        mainloop_audio: MainLoopAudio,
        props: &DictRef,
    ) {
        Self::get(
            id_node,
            mainloop_audio,
            props,
        );
    }

    pub fn get(
        id_node: u32,
        mainloop_audio: MainLoopAudio,
        props: &DictRef,
    ) {
        let audio_sink: Self = Self::new(
            id_node,
            props,
        );

        next_line(
            mainloop_audio
                .next_line
        );
        println!(
            "-> {NEW_SINK}:{tab}{{ {name} }}",
            // "-> {NEW_SINK}:{tab}< {name} |",
            name = audio_sink
                .name,
            tab = tab_filler(NEW_SINK),
        );

        mainloop_audio
            .map_id_to_class
            .borrow_mut()
            .insert(
                id_node,
                ObjectClass::Sink,
            );

        mainloop_audio
            .audio_sinks
            .borrow_mut()
            .push(
                audio_sink
            );
    }

    pub fn remove(
        audio_sinks: Rc<RefCell<Vec<Self>>>,
        id: u32,
        next_line_rc: Rc<RefCell<PrintState>>,
    ) {
        let mut audio_sinks_borrow: RefMut<Vec<Self>> = audio_sinks
            .borrow_mut();
        for
            (
                pos,
                audio_sink,
            )
        in
            audio_sinks_borrow
                .iter()
                .enumerate()
        {
            let id_node: u32 = audio_sink
                .id_node;
            // println!(" sink removed id {:?} dev {:?}", id, sink);
            if
                id
                ==
                id_node
            {
                next_line(next_line_rc);
                println!(
                    "<- {SINK_REMOVED}:{tab}{{ {name} }}",
                    tab = tab_filler(SINK_REMOVED),
                    name = audio_sink
                        .name,
                );

                audio_sinks_borrow
                    .remove(
                        pos
                    );
                break;
            };
        };
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
