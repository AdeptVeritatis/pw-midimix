
use crate::{
    app::{
        localization::{
            NEW_STREAM,
            STREAM_REMOVED,
        },
        print::{
            PrintState,
            PrintTab,
            next_line,
            tab_filler,
        },
    },
    declarations::{
        MEDIA_NAME_MPV,
        MEDIA_NAME_TWITCH,
    },
    hardware::{
        led::{
            LedState,
            handle_stream_leds,
            removed_stream_leds,
        },
        mapping::Mapping,
    },
    impls::pipewire_impl::{
        mainloop::mainloop_audio::MainLoopAudio,
        objects::{
            link::LinkSide,
            port::port_out::StreamPort,
            ObjectClass,
        },
        strings::{
            DUMP_PARAMS,
            DUMP_PROPS,
            PORT_SIDE_FL,
            PORT_SIDE_FR,
            NODE_PARAMS_FORMAT,
            NODE_PARAMS_PORTCONFIG,
            NODE_PARAMS_POSITION,
            NODE_PROPS_APPLICATION_NAME,
            NODE_PROPS_APPLICATION_PROCESS_BINARY,
            NODE_PROPS_MEDIA_NAME,
            NODE_PROPS_N_OUTPUT_PORTS,
        },
        utils::{
            dump_json::get_json_info,
            mute::get_mute,
        },
    },
};
use midir::MidiOutputConnection;
use serde_json::Value;
use std::{
    cell::{
        RefCell,
        RefMut,
    },
    collections::HashMap,
    rc::Rc,
    vec::IntoIter,
};

// ----------------------------------------------------------------------------

// Is stored in the HashMap AudioStreams with the channel id.
// Contains data from the output node.
// Ports contains data from a port.
#[derive(Debug, Clone)]
pub struct AudioStream {
// pub struct StreamNode {
    pub id_node: u32,    // output node
    pub mute: LedState,
    pub name: String, // no copy trait
    pub port_position: Vec<LinkSide>,
    pub ports: HashMap<LinkSide, StreamPort>, // no copy trait
    pub sink_name: String,                    // no copy trait
    // Channel volume stamp:
    pub stamp: u64,
}

impl AudioStream {
    pub fn new(
        id_node: u32,
        info: Value,
        mainloop_audio: MainLoopAudio,
        option_title_off: bool,
    ) -> Self {
        // println!(" info:\n {:#}", info);
    // Get media name:
        let name: String = get_props(
            info
                .clone(),
            option_title_off,
        );
        // println!("{:?}", name);
    // Get mute state:
        let mute: LedState = get_mute(
            info
                .clone(),
            mainloop_audio
                .next_line
                .clone()
        );
    // Get port positions (e.g. FL, FR):
        let port_position: Vec<LinkSide> = Self::get_port_position(
            info
        );
        // println!("port_position: {:?}", port_position);

        // To get sink_name, wait for the links. The node does not know about the links!

        Self {
            id_node,
            mute,
            name,
            port_position,
            ports: HashMap::new(),
            sink_name: String::from(""),
            stamp: 0,
        }
    }
}

impl AudioStream {
    pub fn check(
        id_node: u32,
        mainloop_audio: MainLoopAudio,
        mapping: Mapping,
        option_leds_off: bool,
        option_title_off: bool,
        print_tab: PrintTab,
    ) {
        // Get mute state and name from pw-dump:
        match
            get_json_info(
                id_node
            )
        {
            Err(error) => {
                println!(
                    "!! warning: could not get params from stream node {id_node}\n{tab}error: {error}",
                    tab = print_tab
                        .full_str,
                );
            },
            Ok(info) => {
                // println!("info: {:#}", info);

                // Get first free event_channel:
                let audio_streams: HashMap<u8, Self> = mainloop_audio
                    .audio_streams
                    .borrow()
                    .to_owned();
                let slider_channels: IntoIter<u8> = mapping
                    .channel_maps
                    .slider_channels
                    .clone()
                    .into_iter();
                for
                    mixer_channel
                in
                    slider_channels
                {
                    if
                        !audio_streams
                            .contains_key(
                                &mixer_channel
                        )
                    {
                        // else same stream again???
                        // On new stream dicovered:
                        Self::get(
                            audio_streams,
                            id_node,
                            info,
                            mainloop_audio
                                .clone(),
                            mapping
                                .clone(),
                            mixer_channel,
                            option_leds_off,
                            option_title_off,
                            print_tab
                                .clone(),
                        );

                        break;
                    };
                };
            },
        };
    }

    pub fn get(
        audio_streams: HashMap<u8, Self>,
        id_node: u32,
        info: Value,
        mainloop_audio: MainLoopAudio,
        mapping: Mapping,
        mixer_channel: u8,
        option_leds_off: bool,
        option_title_off: bool,
        print_tab: PrintTab,
    ) {
        let stream_node: Self = Self::new(
            id_node,
            info,
            mainloop_audio
                .clone(),
            option_title_off,
        );

    // Print new stream and old streams:
        Self::print(
            audio_streams
                .iter()
                .collect(),
            mainloop_audio
                .clone(),
            mapping
                .clone(),
            mixer_channel,
            stream_node
                .name
                .clone(),
            print_tab,
        );

    // Switch LED:
        if
            !option_leds_off
        {
            handle_stream_leds(
                mapping,
                mainloop_audio
                    .midir_out,
                mixer_channel,
                stream_node
                    .mute,
            );
        };

        mainloop_audio
            .map_id_to_channel
            .borrow_mut()
            .insert(
                id_node,
                mixer_channel,
            );

        mainloop_audio
            .map_id_to_class
            .borrow_mut()
            .insert(
                id_node,
                ObjectClass::Stream,
            );

        mainloop_audio
            .audio_streams
            .borrow_mut()
            .insert(
                mixer_channel,
                stream_node,
            );
    }

    pub fn remove(
        audio_streams: Rc<RefCell<HashMap<u8, Self>>>,
        // buttons_state: ButtonsState,
        // channel_maps: ChannelMaps,
        id: u32,
        map_id_to_channel: Rc<RefCell<HashMap<u32, u8>>>,
        mapping: Mapping,
        midir_out: Rc<RefCell<MidiOutputConnection>>,
        next_line_rc: Rc<RefCell<PrintState>>,
        option_leds_off: bool,
    ) {
        let mut audio_streams_borrow: RefMut<HashMap<u8, Self>> = audio_streams
            .borrow_mut();
        let slider_channels: Vec<u8> = mapping
            .channel_maps
            .slider_channels
            .clone();
        if
            !audio_streams_borrow
                .is_empty()
        {
            // change to .keys()?, but cannot remove key in own for branch?
            // for mixer_channel in audio_streams_borrow.keys() {
            for
                mixer_channel
            in
                slider_channels
                    .into_iter()
            {
                if
                    audio_streams_borrow
                        .contains_key(
                            &mixer_channel
                        )
                {
                    let stream_node: &Self = match
                        audio_streams_borrow
                            .get(
                                &mixer_channel
                            )
                    {
                        None => panic!("\n!! problem getting channel:\n   mixer_channel: {mixer_channel:?}\n   audio_streams: {audio_streams_borrow:?}"),
                        Some(channel) => channel,
                    };
                    let id_node: u32 = stream_node
                        .id_node;
                    // println!(" stream removed id {:?} node {:?}", id, node);
                    if
                        id
                        ==
                        id_node
                    {
                        // On stream removed:
                        let map_channel_to_num: HashMap<u8, u8> = mapping
                            .channel_maps
                            .sliders_to_buttons
                            .num
                            .clone();
                        next_line(
                            next_line_rc
                        );
                        println!(
                            "<- {STREAM_REMOVED}:{tab}{channel}",
                            tab = tab_filler(STREAM_REMOVED),
                            channel = match
                                map_channel_to_num
                                    .get(
                                        &mixer_channel
                                    )
                            {
                                None => String::from("error"),
                                Some(num) => num
                                    .to_string(),
                            },
                        );
                        match
                            option_leds_off
                        {
                            true => (),
                            false => removed_stream_leds(
                                mapping,
                                midir_out,
                                mixer_channel,
                            ),
                        };

                        audio_streams_borrow
                            .remove(
                                &mixer_channel
                            );
                        map_id_to_channel
                            .borrow_mut()
                            .remove(
                                &id
                            );

                        break;
                    };
                };
            };
        };
    }
}

impl AudioStream {
    pub fn print(
        mut audio_streams_vec: Vec<(&u8, &Self)>,
        mainloop_audio: MainLoopAudio,
        mapping: Mapping,
        mixer_channel: u8,
        name: String,
        print_tab: PrintTab,
    ) {
    // Print new stream:
        let map_channel_to_num: HashMap<u8, u8> = mapping
            .channel_maps
            .sliders_to_buttons
            .num
            .clone();

        next_line(
            mainloop_audio
                .next_line
                .clone()
        );
        println!(
            "-> {NEW_STREAM}:"
        );
        println!(
            "{tab_pre}+ {channel}{tab_suf}{name}",
            tab_pre = " "
                .repeat(
                    print_tab.prefix_len - 2
                ),
            tab_suf = print_tab
                .suffix_str,
            channel = match
                map_channel_to_num
                    .get(
                        &mixer_channel
                    )
            {
                None => String::from("error"),
                Some(num) => num
                    .to_string(),
            },
        );
    // Print other streams:
        // use button::print_channels function ???
        audio_streams_vec
            .sort_by(
                |a, b|
                a
                    .0
                    .cmp(
                        b
                            .0
            )
        ); // needs better way to express a.0 and b.0
        for
            (
                channel,
                stream_node,
            )
        in
            audio_streams_vec
        {
            println!(
                "{tab_pre}{channel}{tab_suf}{name}",
                tab_pre = print_tab
                    .prefix_str,
                tab_suf = print_tab
                    .suffix_str,
                channel = match
                    map_channel_to_num
                        .get(
                            channel
                        )
                {
                    None => String::from("error"),
                    Some(num) => num
                        .to_string(),
                },
                name = stream_node
                    .name,
            );
        };
    }
}

impl AudioStream {
    pub fn get_port_position(
        info: Value
    ) -> Vec<LinkSide> {
        let mut ports_vec: Vec<LinkSide> = Vec::new();
        let port_count: usize = match
            info
                [NODE_PROPS_N_OUTPUT_PORTS]
                .as_u64()
        {
            None => {
                println!("!! problem getting n-output-ports.");
                0
            },
            Some(count) => count as usize,
        };
        // Props/channelMap can be different from PortConfig/format/position: info[DUMP_PARAMS][NODE_PARAMS_PROPS][0][NODE_PARAMS_CHANNELMAP]
        // Mono streams channelMap [FC] are mapped to format/position [FL, FR].
        match
            info
                [DUMP_PARAMS]
                [NODE_PARAMS_PORTCONFIG]
                [0]
                [NODE_PARAMS_FORMAT]
                [NODE_PARAMS_POSITION]
                .as_array()
        {
            None => (),
            Some(array) => {
                if
                    port_count
                    ==
                    array
                        .len()
                {
                    for
                        position
                    in
                        array
                    {
                        match
                            position
                                .as_str()
                        {
                            None => println!("!! problem with empty port positions."),
                            Some(PORT_SIDE_FL) => ports_vec
                                .push(
                                    LinkSide::FL
                                ),
                            Some(PORT_SIDE_FR) => ports_vec
                                .push(
                                    LinkSide::FR
                                ),
                            Some(_) => {
                                println!("!! problem with unknown port position: {position:?}")
                            },
                        }
                    }
                } else {
                    println!(
                        "!! problem with number of ports:\n   port_count: {port_count:?}\n   channel_map_len: {:?}",
                        array
                            .len(),
                    );
                };
            },
        };
        ports_vec
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

fn get_props(
    info: Value,
    // node: u32,
    option_title_off: bool,
) -> String {
    // Get name from props: // rust API not complete atm!
    // println!(" application.name - {:?}", props.get("application.name").unwrap());
    // println!(" node.name                  - {:?}", props.get("node.name"));
    // example at pw-viz -> mod.rs -> fn handle_node -> let name

    // Get name from pw-dump:
    // println!(" dump_info: {:#}", dump_info);
    let app_name: &str = match
        info
            [DUMP_PROPS]
            [NODE_PROPS_APPLICATION_PROCESS_BINARY]
            .as_str()
    {
        None => match
            info
                [DUMP_PROPS]
                [NODE_PROPS_APPLICATION_NAME]
                .as_str()
        {
            None => "---",
            Some(application_name) => application_name,
        },
        Some(process_name) => process_name,
    };
    let media_name: String = match
        option_title_off
    {
        true => String::new(),
        false => handle_media_name(
            app_name,
            match
                info
                    [DUMP_PROPS]
                    [NODE_PROPS_MEDIA_NAME]
                    .as_str()
            {
                None => "error",
                Some(media_name) => media_name,
            },
        ),
    };
    format!("[ {app_name} ] {media_name}")
}

fn handle_media_name(
    app_name: &str,
    dump_media_name: &str,
) -> String {
    match
        app_name
    {
        // mpv player:
        MEDIA_NAME_MPV => {
            // remove trailing " - mpv":
            let media_name_suffix: Vec<&str> = dump_media_name
                .split_terminator(
                    " - mpv"
                )
                .collect();
            match
                media_name_suffix
                    [0]
            {
                // is a twitch stream in mpv player:
                media_name
                    if
                        media_name
                            .contains(
                                MEDIA_NAME_TWITCH
                            )
                => {
                    // remove url from title:
                    let media_name_prefix: Vec<&str> = media_name
                        .split(
                            MEDIA_NAME_TWITCH
                        )
                        .collect();
                    format!(
                        "twitch: {}",
                        media_name_prefix
                            [1]
                    )
                },
                // mpv player - print media name:
                media_name => String::from(
                    media_name
                ),
            }
        }
        // all other apps - print media name:
        _ => String::from(
            dump_media_name
        ),
    }
}
