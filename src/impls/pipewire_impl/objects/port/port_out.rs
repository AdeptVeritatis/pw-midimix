
use crate::{
    app::{
        localization::{
            ERROR,
            WARNING,
        },
        print::{
            PrintTab,
            tab_filler,
        },
    },
    declarations::SLEEP_MILLIS_RELOAD_PORTS,
    errors::HandlePortsError,
    impls::pipewire_impl::{
        objects::{
            link::{
                LinkSide,
                StreamLink,
            },
            node::stream::AudioStream,
            ObjectClass,
        },
        strings::{
            PORT_AUDIO_CHANNEL,
            PORT_NAME,
            PORT_NAME_MONITOR_FL,
            PORT_NAME_MONITOR_FR,
            PORT_NODE_ID,
            PORT_SIDE_FL,
            PORT_SIDE_FR,
            PORT_SIDE_MONO,
        },
        utils::{
            dump_json::get_json_info,
            get_object::{
                get_channel_from_id,
                get_id_from_props,
            },
        },
    },
};
// use json::JsonValue;
use pipewire::{
    // prelude::ReadableDict,
    // spa::ForeignDict,
    spa::utils::dict::DictRef,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
    thread::sleep,
    time::Duration,
};

// ----------------------------------------------------------------------------

// StreamPort:
// Belongs to a port of a stream node.
// LinkSide is already in the HashMap as key for port_position.
// port is the output port of the stream node.
// links is the collection of links to this port.
// links contains the port and node of the sink connected to.
#[derive(Debug, Clone)]
pub struct StreamPort {
    pub links: Vec<StreamLink>,
    pub port_out: u32,
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

// Port of an audio stream node:
pub fn added_port_audio_out(
    audio_streams: Rc<RefCell<HashMap<u8, AudioStream>>>,
    map_id_to_channel: Rc<RefCell<HashMap<u32, u8>>>,
    map_id_to_class: Rc<RefCell<HashMap<u32, ObjectClass>>>,
    port_out: u32,
    print_tab: PrintTab,
    props: &DictRef,
) {
    // println!("!! added port audio out");
    // Check if port is output or monitor:
    let port_output: bool = match props.get(PORT_NAME) {
        Some(PORT_NAME_MONITOR_FL) => false,
        Some(PORT_NAME_MONITOR_FR) => false,
        Some(_) => true,
        None => {
            println!("!! unknown port.name: {:?}", props);
            false
        }
    };
    if port_output {
        // Get LinkSide of port:
        let audio_channel: LinkSide = match props.get(PORT_AUDIO_CHANNEL) {
            Some(PORT_SIDE_FL) => LinkSide::FL,
            Some(PORT_SIDE_FR) => LinkSide::FR,
            Some(PORT_SIDE_MONO) => {
                // println!("!! port side: {PORT_SIDE_MONO}\n   may be a source output");
                LinkSide::FL // or panic???
            }
            Some(side) => {
                println!("!! unknown port side: {:?}", side);
                LinkSide::FL // or panic???
            }
            // None => panic!("!! Problem getting audio.channel of port"),
            None => {
                // Bug with Bitwig and other JACK nodes!!!
                // They don't have an audio.channel.
                // Is the port used either???
                // [Edit:] from https://gitlab.freedesktop.org/pipewire/pipewire/-/tags/0.3.64:
                // "It is now possible to assign custom port names to the ports from an adapter."
                // "This feature is helpful to those who use a multichannel interface with long-term connections."
                // "This way they can label each port with its designation, such as an instrument name or anything else to be displayed in a patchbay or DAW."
                // "It is now possible to assign custom port names to the ports from an adapter with the PW_KEY_NODE_CHANNELNAMES."
                // Can be even everything now???
                // println!(
                //     "!! Problem getting {:?} of port.\n props: {:?}",
                //     PORT_PROPS_AUDIO_CHANNEL,
                //     props,
                // );
                match props.get(PORT_NAME) {
                    Some(name) => match name.ends_with("L") {
                        true => LinkSide::FL,
                        false => match name.ends_with("R") {
                            true => LinkSide::FR,
                            false => LinkSide::FL, // default value???
                        },
                    },
                    None => LinkSide::FL, // default value???
                }
                // LinkSide::FL // default value???
            }
        };
        // Get node from new port props:
        let node: u32 = get_id_from_props(PORT_NODE_ID, props);
        // Get channel from node of the new port:
        match get_channel_from_id(node, Rc::clone(&map_id_to_channel)) {
            Ok(channel) => {
                let check_key: bool = audio_streams.borrow().contains_key(&channel);
                match check_key {
                    true => {
                        match update_stream_node(
                            audio_channel,
                            Rc::clone(&audio_streams),
                            channel,
                            node,
                            port_out,
                            print_tab.clone(),
                        ) {
                            Ok(stream_node) => {
                                Rc::clone(&audio_streams)
                                    .borrow_mut()
                                    .insert(channel.to_owned(), stream_node);
                                map_id_to_class
                                    .borrow_mut()
                                    .insert(port_out, ObjectClass::AudioPortOut);
                                map_id_to_channel.borrow_mut().insert(port_out, channel);
                            }
                            Err(error) => {
                                // println!(
                                //     "!! {ERROR}:{tab}StreamNode not updated\n{tab_full}{error}",
                                //     tab = tab_filler(ERROR),
                                //     tab_full = print_tab.full_str,
                                // );
                                // Better report bug!!!
                                // retry loading:
                                println!(
                                    "!! {ERROR}:{tab}StreamNode\n{tab_full}{error}\n!! trying to update StreamNode again",
                                    tab = tab_filler(ERROR),
                                    tab_full = print_tab.full_str,
                                );
                                // waiting necessary???
                                sleep(Duration::from_millis(SLEEP_MILLIS_RELOAD_PORTS));
                                let port_position: Vec<LinkSide> = match get_json_info(node) {
                                    Ok(info) => AudioStream::get_port_position(info),
                                    Err(error) => {
                                        panic!(
                                            "!! Warning: could not get params from stream node {node}\n{tab}error: {error}",
                                            tab = print_tab.full_str,
                                        );
                                    }
                                };
                                let audio_stream_new: AudioStream = match
                                    Rc::clone(&audio_streams).borrow().get(&channel)
                                {
                                    Some(audio_stream_new) => {
                                        println!("!! StreamNode updated");
                                        AudioStream {
                                            port_position,
                                            ..audio_stream_new.to_owned()
                                        }
                                    }
                                    None => panic!(""),
                                };
                                Rc::clone(&audio_streams)
                                    .borrow_mut()
                                    .insert(channel.to_owned(), audio_stream_new);
                                match update_stream_node(
                                    audio_channel,
                                    Rc::clone(&audio_streams),
                                    channel,
                                    node,
                                    port_out,
                                    print_tab.clone(),
                                ) {
                                    Ok(stream_node) => {
                                        println!(
                                            "!! new StreamNode.port_position: {port_position:?}",
                                            port_position = stream_node.port_position
                                        );
                                        Rc::clone(&audio_streams)
                                            .borrow_mut()
                                            .insert(channel.to_owned(), stream_node);
                                        map_id_to_class
                                            .borrow_mut()
                                            .insert(port_out, ObjectClass::AudioPortOut);
                                        map_id_to_channel.borrow_mut().insert(port_out, channel);
                                    }
                                    Err(error) => {
                                        println!(
                                            "!! {WARNING}:{tab}StreamNode not updated\n{tab_full}{error}",
                                            tab = tab_filler(WARNING),
                                            tab_full = print_tab.full_str,
                                        );
                                    }
                                }
                            }
                        };
                    }
                    false => (),
                }
            }
            Err(_error) => {
                // println!("!! ignore port: {error}");
            },
        }
    }
}

pub fn removed_port_audio_out(port_out: u32, map_id_to_channel: Rc<RefCell<HashMap<u32, u8>>>) {
    // println!("!! Port removed.");
    map_id_to_channel.borrow_mut().remove(&port_out);
}

pub fn update_stream_node(
    audio_channel: LinkSide,
    audio_streams: Rc<RefCell<HashMap<u8, AudioStream>>>,
    channel: u8,
    id_node: u32,
    port_out: u32,
    print_tab: PrintTab,
) -> Result<AudioStream, Box<dyn std::error::Error>> {
    match audio_streams.borrow().get(&channel) {
        Some(audio_stream) => {
            // Check the source node:
            if audio_stream.id_node == id_node {
                // Check the LinkSide:
                // better way to describe this???
                match audio_stream.port_position.contains(&audio_channel) {
                    true => {
                        let mut ports: HashMap<LinkSide, StreamPort> = audio_stream.to_owned().ports;
                        ports.insert(
                            audio_channel,
                            StreamPort {
                                links: Vec::new(),
                                port_out,
                            },
                        );
                        Ok(AudioStream {
                            ports,
                            ..audio_stream.to_owned()
                        })
                    }
                    false => {
                        // remove if solved
                        println!(
                            "!! Problem finding valid port position\n{tab}Linkside from port props: {:?}\n{tab}StreamNode.port_position: {:?}",
                            audio_channel,
                            audio_stream.port_position,
                            tab = print_tab.full_str,
                        );
                        Err(Box::new(HandlePortsError::WrongPortPosition))
                    }
                }
            } else {
                Err(Box::new(HandlePortsError::UnknownSourceNode))
            }
        }
        None => Err(Box::new(HandlePortsError::NoStreamNode)),
    }
}
