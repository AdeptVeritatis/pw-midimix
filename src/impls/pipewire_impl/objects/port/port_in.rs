
use crate::impls::pipewire_impl::{
    objects::{
        node::sink::AudioSink,
        ObjectClass,
    },
    strings::{
        PORT_AUDIO_CHANNEL,
        PORT_NODE_ID,
        PORT_SIDE_FL,
        PORT_SIDE_FR,
    },
    utils::get_object::get_id_from_props,
};
use pipewire::spa::utils::dict::DictRef;
use std::{
    cell::{
        RefCell,
        RefMut,
    },
    collections::HashMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// // StreamPort:
// // Belongs to a port of a stream node.
// // LinkSide is already in the HashMap as key for port_position.
// // port is the output port of the stream node.
// // links is the collection of links to this port.
// // links contains the port and node of the sink connected to.
// #[derive(Debug, Clone)]
// pub struct StreamPort {
//     pub links: Vec<StreamLink>,
//     pub port_out: u32,
// }

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

// Port of an audio sink:
// Writes its port id value to the port_left/port_right keys of an AudioSink struct.
// Does one port at a time with each side/audio_channel separately.
pub fn added_port_audio_in(
    audio_sinks: Rc<RefCell<Vec<AudioSink>>>,
    map_id_to_class: Rc<RefCell<HashMap<u32, ObjectClass>>>,
    port: u32,
    props: &DictRef,
) {
    // Get node from new port props:
    let id_node: u32 = get_id_from_props(PORT_NODE_ID, props);
    // Get audio_sinks:
    let mut audio_sinks_borrow: RefMut<Vec<AudioSink>> = audio_sinks.borrow_mut();
    for (i_sink, audio_sink) in audio_sinks_borrow.iter().enumerate() {
        // Check, if node from port is same as node from AudioSink:
        if id_node == audio_sink.id_node {
            // Get side/audio_channel of new port from props:
            let audio_sink_new: AudioSink = match props.get(PORT_AUDIO_CHANNEL) {
                // Left audio_channel:
                Some(PORT_SIDE_FL) => {
                    // println!("-> new port_in:    FL {}", audio_sink.to_owned().name);
                    // Create new AudioSink with new port assigned to left side:
                    AudioSink {
                        port_left: port,
                        ..audio_sink.to_owned()
                    }
                }
                // Right audio_channel:
                Some(PORT_SIDE_FR) => {
                    // println!("-> new port_in:    FR {}", audio_sink.to_owned().name);
                    // Create new AudioSink with new port assigned to right side:
                    AudioSink {
                        port_right: port,
                        ..audio_sink.to_owned()
                    }
                }
                // Unknown audio_channel:
                Some(audio_channel) => {
                    println!(
                        "!! Problem with unknown {:?} of new port:\n audio_channel: {:?}",
                        PORT_AUDIO_CHANNEL, audio_channel,
                    );
                    // Take old AudioSink:
                    audio_sink.to_owned()
                }
                None => {
                    // No audio_channel found:
                    println!(
                        "!! Problem getting {:?} from new port:\n props: {:?}",
                        PORT_AUDIO_CHANNEL, props,
                    );
                    // Take old AudioSink:
                    audio_sink.to_owned()
                }
            };
            // Remove old AudioSink from audio_sinks:
            audio_sinks_borrow.remove(i_sink);
            // Store new AudioSink in audio_sinks:
            // Changing positions in list?
            audio_sinks_borrow.push(audio_sink_new);
            map_id_to_class
                .borrow_mut()
                .insert(port, ObjectClass::AudioPortIn);
            // println!("-> new port:       inFL {:?} of node {:?}", port, node);
            break;
        }
    }
}

// When a sink is removed, its input ports get removed.
// But there may be other reasons to remove a port.
pub fn removed_port_audio_in(audio_sinks: Rc<RefCell<Vec<AudioSink>>>, port: u32) {
    // Get audio_sinks:
    let mut audio_sinks_borrow: RefMut<Vec<AudioSink>> = audio_sinks.borrow_mut();
    for (i_sink, audio_sink) in audio_sinks_borrow.iter().enumerate() {
        // Check, if port is same as port from AudioSink:
        match port {
            // Left audio_channel:
            port if port == audio_sink.port_left => {
                // Create a new AudioSink with default port_left value:
                let audio_sink_new: AudioSink = AudioSink {
                    port_left: 0,
                    ..audio_sink.to_owned()
                };
                // Remove old AudioSink from audio_sinks:
                audio_sinks_borrow.remove(i_sink);
                // Store new AudioSink in audio_sinks:
                audio_sinks_borrow.push(audio_sink_new);
                break;
            }
            // Right audio_channel:
            port if port == audio_sink.port_right => {
                // Create a new AudioSink with default port_right value:
                let audio_sink_new: AudioSink = AudioSink {
                    port_right: 0,
                    ..audio_sink.to_owned()
                };
                // Remove old AudioSink from audio_sinks:
                audio_sinks_borrow.remove(i_sink);
                // Store new AudioSink in audio_sinks:
                audio_sinks_borrow.push(audio_sink_new);
                break;
            }
            // Port is not from this audio_sink:
            _ => {
                // println!(
                //     "!! sink port {port} not from {name}",
                //     name = audio_sink.name,
                // );
            }
        }
    }
}
