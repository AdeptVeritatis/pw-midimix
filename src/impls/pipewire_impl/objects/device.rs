
use crate::{
    app::{
        localization::{
            DEVICE_REMOVED,
            NEW_DEVICE,
        },
        print::{
            PrintState,
            next_line,
            tab_filler,
        },
    },
    impls::pipewire_impl::strings::{
        DEVICE_PARAMS_DEVICE,
        DEVICE_PARAMS_DIRECTION,
        DEVICE_PARAMS_INDEX,
        DEVICE_PARAMS_INPUT,
        DEVICE_PARAMS_OUTPUT,
        DEVICE_PARAMS_ROUTE,
        DEVICE_PROPS_DESCRIPTION,
        DEVICE_PROPS_NICK,
        DUMP_PARAMS,
    },
};
use pipewire::spa::utils::dict::DictRef;
use serde_json::Value;
use std::{
    cell::{
        RefCell,
        RefMut,
    },
    rc::Rc,
};

// ----------------------------------------------------------------------------

// AudioDevice:
#[derive(Debug)]
pub struct AudioDevice {
    pub device: u32,
    pub name: String, // no copy trait
    pub param_device: u32,
    pub param_index: u32,
    // Main volume stamp:
    pub stamp: u64,
}

impl AudioDevice {
    pub fn get_name(&self) -> &String {
        &self.name
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn added_device(
    audio_devices: Rc<RefCell<Vec<AudioDevice>>>,
    device: u32,
    info: Value,
    next_line_rc: Rc<RefCell<PrintState>>,
    props: &DictRef,
) {
    let name: String = get_device_name(
        props
    );
    next_line(next_line_rc);
    println!(
        "-> {NEW_DEVICE}:{tab}( {name} )",
        tab = tab_filler(NEW_DEVICE),
    );
    // println!(" props: {:?}", props);
    // let device = props.get(NODE_PROPS_DEVICE_ID).unwrap().parse::<u32>().unwrap();

    // ????????????????????????????????????????????????????!!!!!!!!!!!!!!!!!!!
    let device_rc: Rc<RefCell<u32>> = Rc::new(RefCell::new(0));
    let index_rc: Rc<RefCell<u32>> = Rc::new(RefCell::new(0));

    get_params(Rc::clone(&device_rc), Rc::clone(&index_rc), info);

    let param_device: u32 = device_rc.borrow().to_owned();
    let param_index: u32 = index_rc.borrow().to_owned();
    let stamp: u64 = 0;

    let audio_device: AudioDevice = AudioDevice {
        device,
        name,
        param_device,
        param_index,
        stamp,
    };
    audio_devices
        .borrow_mut()
        .push(
            audio_device
        );
}

fn get_device_name(
    props: &DictRef,
) -> String {
    match
        props
            .get(
                DEVICE_PROPS_NICK
            )
    {
        None => match
            props
                .get(
                    DEVICE_PROPS_DESCRIPTION
                )
        {
            None => {
                // println!(" props: {:?}", props);
                String::from("empty name")
            },
            Some(name) => name
                .to_string(),
        },
        Some(name) => name
            .to_string(),
    }
}

fn get_params(device_rc: Rc<RefCell<u32>>, index_rc: Rc<RefCell<u32>>, info: Value) {
    // Search for index and device params of output route:
    // "params": {
    //     "Route": [
    //         {
    //             "index": u32
    //             "direction": "Output"
    //             "device": u32
    //         }
    //     ]
    // }

    // Get whole dump from node with props and params:
    // println!(" info: {:#}", info);
    match
        info
            [DUMP_PARAMS]
            [DEVICE_PARAMS_ROUTE]
            .as_array()
    {
        None => (),
        Some(array) => {
            for
                route_item
            in
                array
            {
                // println!(" route_item: {:?}", route_item);
                match
                    route_item
                        [DEVICE_PARAMS_DIRECTION]
                        .as_str()
                {
                    None => println!("!! Problem: empty port direction"),
                    Some(DEVICE_PARAMS_OUTPUT) => {
                        // println!("output: {:?}", route_item[DEVICE_PARAMS_DIRECTION]);
                        let route_device: u32 = match
                            route_item
                                [DEVICE_PARAMS_DEVICE]
                                .as_u64()
                        {
                            None => panic!("\n!! Problem reading device param"), // "\n!! Problem reading device param:\n item: {:#}", route_item,),
                            Some(device) => device as u32,
                        };
                        device_rc
                            .replace(
                                route_device
                            );
                        let route_index: u32 = match
                            route_item
                                [DEVICE_PARAMS_INDEX]
                                .as_u64()
                        {
                            None => panic!("\n!! Problem reading index param"), // "\n!! Problem reading index param:\n item: {:#}", route_item),
                            Some(index) => index as u32,
                        };
                        index_rc
                            .replace(
                                route_index
                            );
                        break;
                    },
                    Some(DEVICE_PARAMS_INPUT) => {
                        // println!("input: {:?}", route_item[DEVICE_PARAMS_DIRECTION])
                    },
                    Some(direction) => println!("!! Problem: unknown port direction: {direction:?}"),
                };
            }
        },
    };
}

pub fn removed_device(
    audio_device_main: Rc<RefCell<usize>>,
    audio_devices: Rc<RefCell<Vec<AudioDevice>>>,
    id: u32,
    next_line_rc: Rc<RefCell<PrintState>>,
) {
    let index_main: usize = audio_device_main
        .borrow()
        .to_owned();

    let mut audio_devices_borrow: RefMut<Vec<AudioDevice>> = audio_devices
        .borrow_mut();
    for
        (
            index,
            audio_device,
        )
    in
        audio_devices_borrow
            .iter()
            .enumerate()
    {
        let device: u32 = audio_device
            .device;
        //         println!(" device removed id {:?} dev {:?}", id, device);
        if
            id
            ==
            device
        {
            next_line(next_line_rc);
            println!(
                "<- {DEVICE_REMOVED}:{tab}( {device} )",
                tab = tab_filler(DEVICE_REMOVED),
                device = audio_device.get_name(),
            );
            audio_devices_borrow
                .remove(
                    index
                );
            if
                index
                ==
                index_main
            {
                audio_device_main
                    .replace(
                        0
                    );
            }
            break;
        }
    }
}
