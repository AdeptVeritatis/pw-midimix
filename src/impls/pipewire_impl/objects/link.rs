
use crate::impls::pipewire_impl::{
    objects::{
        node::{
            sink::AudioSink,
            stream::AudioStream,
        },
        port::port_out::StreamPort,
        ObjectClass,
    },
    strings::{
        FACTORY_LINK,
        LINK_NODE_INPUT,
        LINK_NODE_OUTPUT,
        LINK_PORT_INPUT,
        LINK_PORT_OUTPUT,
    },
    utils::get_object::{
        get_channel_from_id,
        get_id_from_props,
        get_stream_node,
        get_stream_port,
    },
};
use pipewire::{
    core::Core,
    registry::Registry,
    spa::utils::dict::DictRef,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------

// LinkSide:
#[derive(Debug, Clone, Copy, Eq, Hash, PartialEq)]
pub enum LinkSide {
    FL,
    FR,
}

// ----------------------------------------------------------------------------

// StreamLink:
// Belongs to a link connected to a port of a stream node.
#[derive(Debug, Clone, Copy)]
pub struct StreamLink {
    pub id_link: u32,
    pub id_node_in: u32,
    // pub node_out: u32, // already in StreamNode, need to go back???
    // pub port_in: u32, // never read
    // pub port_out: u32,
    // pub side: LinkSide, // already in StreamNode.ports, need to go back???
}

impl StreamLink {
    pub fn new() -> Self {
        let id_link: u32 = 0; // remove later !!!
        let id_node_in: u32 = 0; // remove later !!!
        Self {
            id_link,
            id_node_in,
        }
    }
}


// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn added_link_audio(
    audio_sinks: Rc<RefCell<Vec<AudioSink>>>,
    audio_streams: Rc<RefCell<HashMap<u8, AudioStream>>>,
    id_link: u32,
    map_id_to_channel: Rc<RefCell<HashMap<u32, u8>>>,
    map_id_to_class: Rc<RefCell<HashMap<u32, ObjectClass>>>,
    props: &DictRef,
) {
    // Get stream node of link from props:
    let node_out: u32 = get_id_from_props(LINK_NODE_OUTPUT, props);
    // Get channel of node from map_id_to_channel:
    match get_channel_from_id(node_out, Rc::clone(&map_id_to_channel)) {
        Ok(channel) => {
            // Check if link is connected to the channel.
            let check_key: bool = audio_streams.borrow().contains_key(&channel);
            match check_key {
                true => {
                    // Get other props from link:
                    let port_out: u32 = get_id_from_props(LINK_PORT_OUTPUT, props);
                    // Get old StreamNode of the known channel:
                    let audio_stream: AudioStream =
                        get_stream_node(Rc::clone(&audio_streams), channel);
                    // Get StreamPort and LinkSide of different sides from StreamNode.ports:
                    let audio_stream_rc: Rc<RefCell<AudioStream>> =
                        Rc::new(RefCell::new(audio_stream.clone()));
                    for (link_side, stream_port) in audio_stream.ports.iter() {
                        // Check if port from StreamNode is same as port_out from link:
                        if stream_port.port_out == port_out {
                            // Check if link is already stored: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            handle_link_audio(
                                audio_sinks,
                                audio_stream_rc
                                    .clone(),
                                id_link,
                                link_side.to_owned(),
                                // node_out,
                                // port_out,
                                props,
                            );
                            break;
                        }
                    }
                    // Get new StreamNode:
                    let audio_stream_new: AudioStream = audio_stream_rc.borrow().to_owned();
                    // Store new StreamNode:
                    Rc::clone(&audio_streams)
                        .borrow_mut()
                        .insert(channel, audio_stream_new);
                    map_id_to_channel.borrow_mut().insert(id_link, channel);
                    map_id_to_class.borrow_mut().insert(id_link, ObjectClass::Link);
                }
                false => (),
            }
        }
        Err(_error) => (),
    }
}

pub fn removed_link_audio(
    audio_streams: Rc<RefCell<HashMap<u8, AudioStream>>>,
    id_link: u32,
    map_id_to_channel: Rc<RefCell<HashMap<u32, u8>>>,
) {
    // Get channel from link:
    // Link id was added to map_id_to_channel.
    match get_channel_from_id(id_link, Rc::clone(&map_id_to_channel)) {
        Ok(channel) => {
            // Check if link is connected to the channel.
            let check_key: bool = audio_streams.borrow().contains_key(&channel);
            match check_key {
                true => {
                    // Get old StreamNode of the channel:
                    let audio_stream: AudioStream =
                        get_stream_node(Rc::clone(&audio_streams), channel);
                    // Get StreamNode.ports from StreamNode:
                    let ports: HashMap<LinkSide, StreamPort> = audio_stream.to_owned().ports;
                    let audio_stream_rc: Rc<RefCell<AudioStream>> =
                        Rc::new(RefCell::new(audio_stream));
                    // Get StreamPort and LinkSide of different sides from StreamNode.ports:
                    for (link_side, stream_port) in ports.iter() {
                        // Get StreamPort.links from StreamPort:
                        let links: Vec<StreamLink> = stream_port.to_owned().links;
                        // Get StreamLink and its position in the vec:
                        for (pos, stream_link) in links.iter().enumerate() {
                            // Check if StreamLink.link is the same as the removed link:
                            if stream_link.id_link == id_link {
                                handle_link_audio_removed(
                                    audio_stream_rc
                                        .clone(),
                                    link_side.to_owned(),
                                    pos,
                                );
                                break;
                            }
                        }
                    }
                    // Get new StreamNode:
                    let audio_stream_new: AudioStream = audio_stream_rc.borrow().to_owned();
                    // Store new StreamNode:
                    audio_streams.borrow_mut().insert(channel, audio_stream_new);
                    map_id_to_channel.borrow_mut().remove(&id_link);
                }
                false => (),
            }
        }
        Err(_error) => (),
    }
}

// ----------------------------------------------------------------------------

// core and registry necessary?
// get_registry() here?
pub fn switch_link_audio(
    core: &Rc<Core>,
    link_old: u32,
    node_in: u32,
    node_out: u32,
    port_in: u32,
    port_out: u32,
    registry: Rc<Registry>,
) {
    // Create new link:
    match core.create_object::<pipewire::link::Link>(
        FACTORY_LINK,
        &pipewire::properties::properties! {
            LINK_PORT_INPUT => port_in.to_string(),
            LINK_PORT_OUTPUT => port_out.to_string(),
            LINK_NODE_INPUT => node_in.to_string(),
            LINK_NODE_OUTPUT => node_out.to_string(),
            "object.linger" => "1"
        },
    ) {
        Ok(_) => {
            // Destroy old link:
            match registry.destroy_global(link_old).into_result() {
                Ok(_) => (),
                Err(error) => {
                    println!(
                        "\n!! Problem destroying old link:\n link: {:?}\n error: {:?}\n",
                        link_old, error,
                    );
                }
            };
        }
        Err(error) => {
            println!(
                    "\n!! Problem adding new link:\n port_in: {:?}\n port_out: {:?}\n node_in: {:?}\n node_out: {:?}\n error: {:?}\n",
                    port_in,
                    port_out,
                    node_in,
                    node_out,
                    error,
                );
        }
    };
    // registry.destroy_object(link_old);
}

pub fn create_link_audio(
    core: &Rc<Core>,
    node_in: u32,
    node_out: u32,
    port_in: u32,
    port_out: u32,
) {
    // Create new link:
    match core.create_object::<pipewire::link::Link>(
        FACTORY_LINK,
        &pipewire::properties::properties! {
            LINK_PORT_INPUT => port_in.to_string(),
            LINK_PORT_OUTPUT => port_out.to_string(),
            LINK_NODE_INPUT => node_in.to_string(),
            LINK_NODE_OUTPUT => node_out.to_string(),
            "object.linger" => "1"
        },
    ) {
        Ok(_) => {}
        Err(error) => {
            println!(
                    "\n!! Problem adding new link:\n port_in: {:?}\n port_out: {:?}\n node_in: {:?}\n node_out: {:?}\n error: {:?}\n",
                    port_in,
                    port_out,
                    node_in,
                    node_out,
                    error,
                );
        }
    };
}

// Struct pipewire::CoreInner // pub fn create_object<P: ProxyT, D: ReadableDict>(&self, factory_name: &str, properties: &D) -> Result<P, Error> //
// Struct pipewire::CoreInner // pub fn destroy_object<P: ProxyT>(&self, proxy: P) -> Result<AsyncSeq, Error> //
// Struct pipewire::registry::Registry // pub fn destroy_global(&self, global_id: u32) -> SpaResult //

// ----------------------------------------------------------------------------

pub fn handle_link_audio(
    audio_sinks: Rc<RefCell<Vec<AudioSink>>>,
    audio_stream_rc: Rc<RefCell<AudioStream>>, // ????? mut ?????
    id_link: u32,
    link_side: LinkSide,
    // node_out: u32,
    // port_out: u32,
    props: &DictRef,
) {
    // Get old StreamNode:
    let audio_stream: AudioStream = audio_stream_rc.borrow().to_owned();
    // Get StreamNode.ports from StreamNode:
    let mut ports: HashMap<LinkSide, StreamPort> = audio_stream.ports;
    // Get old StreamPort from StreamNode.ports:
    let stream_port: StreamPort = get_stream_port(link_side, ports.clone());
    // Get StreamPort.links from StreamPort:
    let mut links: Vec<StreamLink> = stream_port.links;
    // Get rest of the new link props:
    let id_node_in: u32 = get_id_from_props(LINK_NODE_INPUT, props);
    // let port_in: u32 = get_id_from_props(LINK_PORT_INPUT, props);
    // Create new StreamLink from link props:
    let stream_link_new: StreamLink = {
        StreamLink {
            id_link,
            id_node_in,
            // node_out,
            // port_in,
            // port_out,
            // side: link_side,
        }
    };
    // Stack it all back together:
    // Update StreamPort.links with new StreamLink:
    links.push(stream_link_new);
    // Create new StreamPort with updated StreamPort.links:
    let stream_port_new: StreamPort = {
        StreamPort {
            links,
            ..stream_port
        }
    };
    // Update StreamNode.ports with new StreamPort:
    ports.insert(link_side, stream_port_new);
    // Get StreamNode.sink_name from audio_sinks:
    let sink_name_rc: Rc<RefCell<String>> = Rc::new(RefCell::new(String::new()));
    for audio_sink in audio_sinks.borrow().iter() {
        if audio_sink.id_node == id_node_in {
            sink_name_rc.replace(audio_sink.to_owned().name);
            break;
        }
    }
    let sink_name: String = sink_name_rc.borrow().to_owned();
    // Create new StreamNode with updated StreamNode.ports and StreamNode.sink_name:
    let audio_stream_new: AudioStream = {
        AudioStream {
            ports,
            sink_name,
            ..audio_stream
        }
    };
    // dbg!(&stream_node_new);
    // Store new StreamNode:
    audio_stream_rc.replace(audio_stream_new);
}

pub fn handle_link_audio_removed(
    audio_stream_rc: Rc<RefCell<AudioStream>>,
    link_side: LinkSide,
    pos: usize,
) {
    // Get old StreamNode:
    let audio_stream: AudioStream = audio_stream_rc.borrow().to_owned();
    // Get StreamNode.ports from StreamNode:
    let mut ports: HashMap<LinkSide, StreamPort> = audio_stream.ports;
    // Get old StreamPort from StreamNode.ports:
    let stream_port: StreamPort = get_stream_port(link_side, ports.clone());
    // Get StreamPort.links from StreamPort:
    let mut links: Vec<StreamLink> = stream_port.to_owned().links;
    // Remove link from StreamPort.links:
    links.remove(pos);
    // Stack it all back together:
    // Create new StreamPort with updated StreamPort.links:
    let stream_port_new: StreamPort = {
        StreamPort {
            links,
            ..stream_port
        }
    };
    // Update StreamNode.ports with new StreamPort:
    ports.insert(link_side, stream_port_new);
    // Create new StreamNode with updated StreamNode.ports:
    // what about sink name???
    let audio_stream_new: AudioStream = {
        AudioStream {
            ports,
            ..audio_stream
        }
    };
    // dbg!(&stream_node_new);
    // Store new StreamNode:
    audio_stream_rc.replace(audio_stream_new);
}
