
use crate::{
    app::print::PrintTab,
    impls::pipewire_impl::{
        utils::dump_json::get_json_info,
        mainloop::mainloop_audio::MainLoopAudio,
        objects::{
            device::{
                added_device,
                removed_device,
            },
            link::{
                added_link_audio,
                removed_link_audio,
            },
            node::{
                sink::AudioSink,
                stream::AudioStream,
                NodeObj,
            },
            port::{
                port_in::{
                    added_port_audio_in,
                    removed_port_audio_in,
                },
                port_out::{
                    added_port_audio_out,
                    removed_port_audio_out,
                },
            },
            ObjectClass,
        },
    },
    hardware::mapping::Mapping,
    impls::pipewire_impl::strings::{
        DEVICE_PARAMS,
        DEVICE_PARAMS_NAME,
        DEVICE_PARAMS_OFF,
        DEVICE_PARAMS_PROFILE,
        FORMAT_DSP_MONO_AUDIO,
        PORT_DIRECTION,
        PORT_DIRECTION_IN,
        PORT_DIRECTION_OUT,
        PORT_FORMAT_DSP,
    },
};
use pipewire::{
    registry::{
        Listener,
        Registry,
    },
    spa::utils::dict::DictRef,
    types::ObjectType,
};
use std::{
    collections::HashMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

// move all ObjectType content to separate functions !!!
pub fn listener_audio(
    mainloop_audio: MainLoopAudio,
    mapping: Mapping,
    option_leds_off: bool,
    option_title_off: bool,
    print_tab: PrintTab,
    registry: Rc<Registry>,
) -> Listener {
    registry
        .add_listener_local()
        // Called when a global object is added:
        .global(
            move
                |global|
            {
                match
                    global
                        .props
                {
                    None => (),
                    // Props found:
                    Some(ref props) => {
                        // Look for types:
                        match
                            global
                                .type_
                        {
                            // ObjectType::Client => println!("Client: {:?}", global),
                            // ObjectType::ClientEndpoint => println!("ClientEndpoint: {:?}", global),
                            // ObjectType::ClientNode => println!("ClientNode: {:?}", global),
                            // ObjectType::ClientSession => println!("ClientSession: {:?}", global),
                            // ObjectType::Core => println!("Core: {:?}", global),
                            // Shows all devices, even deactivated:
                            ObjectType::Device => {
                                // println!("Device: {:?}", global);
                                // println!("Device props: {:?}", props);
                                handle_device(
                                    global
                                        .id,
                                    mainloop_audio
                                        .clone(),
                                    print_tab
                                        .clone(),
                                    props,
                                );
                            },
                            // ObjectType::Endpoint => println!("Endpoint: {:?}", global),
                            // ObjectType::EndpointLink => println!("EndpointLink: {:?}", global),
                            // ObjectType::EndpointStream => println!("EndpointStream: {:?}", global),
                            // ObjectType::Factory => println!("Factory: {:?}", global),
                            // Links:
                            ObjectType::Link => {
                                // println!("Link: {:?}", global);
                                // println!("Link props: {:?}", props);
                                handle_link(
                                    global
                                        .id,
                                    mainloop_audio
                                        .clone(),
                                    props,
                                );
                                // println!(" audio_streams after: {:?}", audio_streams.borrow());
                                // println!(" audio streams after:  {:?}", Rc::clone(&map_id_to_class).borrow());
                            },
                            // ObjectType::Metadata => println!("Metadata: {:?}", global),
                            // ObjectType::Module => println!("Module: {:?}", global),
                            // Nodes:
                            ObjectType::Node => {
                                // println!("Node: {:?}", global);
                                // println!("Node props: {:?}", props);
                                NodeObj::get(
                                    global
                                        .id,
                                    mainloop_audio
                                        .clone(),
                                    mapping
                                        .clone(),
                                    option_leds_off,
                                    option_title_off,
                                    print_tab
                                        .clone(),
                                    props,
                                );
                            },
                            // Ports:
                            ObjectType::Port => {
                                // println!("Port: {:?}", global);
                                // println!("Port props: {:?}", props);
                                handle_port(
                                    global
                                        .id,
                                    mainloop_audio
                                        .clone(),
                                    print_tab
                                        .clone(),
                                    props,
                                );
                            },
                            // ObjectType::Profiler => println!("Profiler: {:?}", global),
                            // ObjectType::Registry => println!("Registry: {:?}", global),
                            // ObjectType::Session => println!("Session: {:?}", global),
                            _ => (),
                        };
                    },
                };
            },
        )
        .register()
}

// ----------------------------------------------------------------------------

pub fn listener_audio_removed(
    mainloop_audio: MainLoopAudio,
    mapping: Mapping,
    option_leds_off: bool,
    registry: Rc<Registry>,
) -> Listener {
    registry
        .add_listener_local()
        // Called when a global object is removed:
        .global_remove(
            move
                |id|
            {
                // println!("<- Object removed:\n    id             {}", id);
                let map_id_to_class: HashMap<u32, ObjectClass> = mainloop_audio
                    .map_id_to_class
                    .borrow()
                    .to_owned();
                match
                    map_id_to_class
                        .get(
                            &id
                        )
                {
                    // Port(sink) removed:
                    Some(ObjectClass::AudioPortIn) => {
                        removed_port_audio_in(
                            mainloop_audio
                                .audio_sinks
                                .clone(),
                            id,
                        );
                        mainloop_audio
                            .map_id_to_class
                            .borrow_mut()
                            .remove(
                                &id
                            );
                    },
                    // Port(stream) removed:
                    Some(ObjectClass::AudioPortOut) => {
                        removed_port_audio_out(
                            id,
                            mainloop_audio
                                .map_id_to_channel
                                .clone(),
                        );
                        mainloop_audio
                            .map_id_to_class
                            .borrow_mut()
                            .remove(
                                &id
                            );
                    },
                    // Device(audio) removed:
                    Some(ObjectClass::Device) => {
                        removed_device(
                            mainloop_audio
                                .audio_device_main
                                .clone(),
                            mainloop_audio
                                .audio_devices
                                .clone(),
                            id,
                            mainloop_audio
                                .next_line
                                .clone(),
                        );
                        mainloop_audio
                            .map_id_to_class
                            .borrow_mut()
                            .remove(
                                &id
                            );
                    },
                    // Link(stream) removed:
                    Some(ObjectClass::Link) => {
                        removed_link_audio(
                            mainloop_audio
                                .audio_streams
                                .clone(),
                            id,
                            mainloop_audio
                                .map_id_to_channel
                                .clone(),
                        );
                        mainloop_audio
                            .map_id_to_class
                            .borrow_mut()
                            .remove(
                                &id
                            );
                    },
                    // Sink(audio) removed:
                    Some(ObjectClass::Sink) => {
                        AudioSink::remove(
                            mainloop_audio
                                .audio_sinks
                                .clone(),
                            id,
                            mainloop_audio
                                .next_line
                                .clone(),
                        );
                        mainloop_audio
                            .map_id_to_class
                            .borrow_mut()
                            .remove(
                                &id
                            );
                    },
                    // Stream(audio) removed:
                    Some(ObjectClass::Stream) => {
                        AudioStream::remove(
                            mainloop_audio
                                .audio_streams
                                .clone(),
                            id,
                            mainloop_audio
                                .map_id_to_channel
                                .clone(),
                            mapping
                                .clone(),
                            mainloop_audio
                                .midir_out
                                .clone(),
                            mainloop_audio
                                .next_line
                                .clone(),
                            option_leds_off,
                        );
                        mainloop_audio
                            .map_id_to_class
                            .borrow_mut()
                            .remove(
                                &id
                            );
                    },
                    _ => (),
                };
            },
        )
        .register()
}

// ----------------------------------------------------------------------------

fn handle_device(
    global_id: u32,
    mainloop_audio: MainLoopAudio,
    print_tab: PrintTab,
    props: &DictRef,
) {
    // Get profile name from pw-dump:
    match
        get_json_info(
            global_id
        )
    {
        // if error, ignore device:
        Err(error) => {
            println!(
                "!! warning: could not get params from device {device}\n{tab}error: {error}",
                device = global_id,
                tab = print_tab
                    .full_str,
            );
        },
        Ok(info) => {
            // println!("info: {:#}", info);
            // Profile "off" is in params not props:
            // Spa:Pod:Object:Param:Profile:name
            match
                info
                    [DEVICE_PARAMS]
                    [DEVICE_PARAMS_PROFILE]
                    [0]
                    [DEVICE_PARAMS_NAME]
                    .as_str()
            {
                None => (),
                Some(DEVICE_PARAMS_OFF) => (),
                Some(_) => {
                    added_device(
                        mainloop_audio
                            .audio_devices,
                        global_id,
                        info,
                        mainloop_audio
                            .next_line,
                        props,
                    );
                    mainloop_audio
                        .map_id_to_class
                        .borrow_mut()
                        .insert(
                            global_id,
                            ObjectClass::Device,
                        );
                },
            };
        },
    };
}

// ----------------------------------------------------------------------------

fn handle_link(
    global_id: u32,
    mainloop_audio: MainLoopAudio,
    props: &DictRef,
) {
    added_link_audio(
        mainloop_audio
            .audio_sinks,
        mainloop_audio
            .audio_streams,
        global_id,
        mainloop_audio
            .map_id_to_channel,
        mainloop_audio
            .map_id_to_class,
        props,
    );
}

// ----------------------------------------------------------------------------

fn handle_port(
    global_id: u32,
    mainloop_audio: MainLoopAudio,
    print_tab: PrintTab,
    props: &DictRef,
) {
    // select left/right: sink in ports, stream out ports
    match
        props
            .get(
                PORT_FORMAT_DSP
            )
    {
        None => (),
        Some(FORMAT_DSP_MONO_AUDIO) => {
            match
                props
                    .get(
                        PORT_DIRECTION
                    )
            {
                None => (),
                // Sink ports:
                Some(PORT_DIRECTION_IN) => {
                    // println!("Port in: {:?}", global);
                    // println!("Port in props: {:?}", props);
                    added_port_audio_in(
                        mainloop_audio
                            .audio_sinks
                            .clone(),
                        mainloop_audio
                            .map_id_to_class
                            .clone(),
                        global_id,
                        props,
                    );
                    // println!(" after:\n  device: {:?}\n  sinks: {:?}\n  class: {:?}\n",
                    //     Rc::clone(&audio_devices).borrow(),
                    //     Rc::clone(&audio_sinks).borrow(),
                    //     Rc::clone(&map_id_to_class).borrow(),
                    // );
                },
                // Stream ports:
                Some(PORT_DIRECTION_OUT) => {
                    // println!("Port out: {:?}", global);
                    // println!("Port out props: {:?}", props);
                    added_port_audio_out(
                        mainloop_audio
                            .audio_streams
                            .clone(),
                        mainloop_audio
                            .map_id_to_channel
                            .clone(),
                        mainloop_audio
                            .map_id_to_class
                            .clone(),
                        global_id,
                        print_tab
                            .clone(),
                        props,
                    );
                    // println!(" audio_streams after: {:?}", audio_streams.borrow());
                },
                _ => (),
            };
        },
        _ => (),
    };
}
