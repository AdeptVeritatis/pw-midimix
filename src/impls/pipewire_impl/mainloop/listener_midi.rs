
use crate::{
    app::{
        localization::{
            NEW_MIDI_IN,
            NEW_MIDI_NODE,
            NEW_MIDI_OUT,
        },
        print::tab_filler,
    },
    impls::pipewire_impl::strings::{
        FORMAT_DSP_RAW_MIDI,
        MEDIA_CLASS_MIDI_BRIDGE,
        NODE_PROPS_MEDIA_CLASS,
        NODE_PROPS_NAME,
        PORT_ALIAS,
        PORT_DIRECTION,
        PORT_DIRECTION_IN,
        PORT_DIRECTION_OUT,
        PORT_FORMAT_DSP,
    },
};
use pipewire::{
    registry::{
        Listener,
        Registry,
    },
    types::ObjectType,
};
use std::rc::Rc;

pub fn listener_midi(registry_midi: Rc<Registry>) -> Listener {
    registry_midi
        .add_listener_local()
        // Called when a global object is added:
        .global(move |global| {
            if let Some(ref props) = global.props {
                // match!!!
                match global.type_ {
                    ObjectType::Node => {
                        // println!(" node found: {:?}", global);
                        match props.get(NODE_PROPS_MEDIA_CLASS) {
                            Some(MEDIA_CLASS_MIDI_BRIDGE) => {
                                // only call function!
                                println!(
                                    "-> {NEW_MIDI_NODE}:{tab}{name}",
                                    tab = tab_filler(NEW_MIDI_NODE),
                                    name = match props.get(NODE_PROPS_NAME) {
                                        Some(name) => name.to_string(),
                                        None => String::from("error"),
                                    },
                                );
                                // create MIDI ports:
                                // let global_clone = global.clone();
                                // let midi_node: pipewire::node::Node = registry_midi_clone.bind(global_clone).expect("Failed to bind link proxy");
                                // create_midi_ports(
                                //     midi_node,
                                // );
                            }
                            _ => (),
                        }
                    }
                    ObjectType::Port => {
                        // println!(" port props: {:?}",props);
                        match props.get(PORT_FORMAT_DSP) {
                            Some(FORMAT_DSP_RAW_MIDI) => {
                                // use new struct MidiPorts!!!
                                match props.get(PORT_DIRECTION) {
                                    Some(PORT_DIRECTION_IN) => {
                                        // only call function?
                                        // count up to add selection num here?
                                        println!(
                                            "-> {NEW_MIDI_IN}:{tab}{name}",
                                            tab = tab_filler(NEW_MIDI_IN),
                                            name = match props.get(PORT_ALIAS) {
                                                Some(alias) => alias.to_string(),
                                                None => String::from("error"),
                                            },
                                        );
                                    }
                                    Some(PORT_DIRECTION_OUT) => {
                                        // only call function?
                                        // count up to add selection num here?
                                        println!(
                                            "-> {NEW_MIDI_OUT}:{tab}{name}",
                                            tab = tab_filler(NEW_MIDI_OUT),
                                            name = match props.get(PORT_ALIAS) {
                                                Some(alias) => alias.to_string(),
                                                None => String::from("error"),
                                            },
                                        );
                                    }
                                    _ => (),
                                }
                            }
                            _ => (),
                        }
                    }
                    _ => (),
                }
            };
        })
        .register()
}
