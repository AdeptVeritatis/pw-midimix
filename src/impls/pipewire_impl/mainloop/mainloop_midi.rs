
use crate::impls::pipewire_impl::{
    mainloop::listener_midi::listener_midi,
    utils::roundtrip::do_roundtrip,
};
use pipewire::{
    context::Context,
    core::Core,
    main_loop::MainLoop,
    registry::{
        Listener,
        Registry,
    },
};
use std::rc::Rc;

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------

pub fn create_midi_mainloop() -> Result<(), Box<dyn std::error::Error>> {
    println!(); // style
    let mainloop_midi: MainLoop = match MainLoop::new(None) {
        Ok(mainloop) => mainloop,
        Err(error) => panic!(
            "\n!! Problem creating pipewire mainloop:\n error: {:?}",
            error,
        ),
    };
    let context_midi: Context = match Context::new(&mainloop_midi) {
        Ok(context) => context,
        Err(error) => panic!(
            "\n!! Problem creating pipewire context:\n error: {:?}",
            error,
        ),
    };
    let core_midi: Rc<Core> = Rc::new(match context_midi.connect(None) {
        Ok(core) => core,
        Err(error) => panic!(
            "\n!! Problem connecting to pipewire core:\n error: {:?}",
            error,
        ),
    });
    let registry_midi: Rc<Registry> = Rc::new(match core_midi.get_registry() {
        Ok(registry) => registry,
        Err(error) => panic!("\n!! Problem getting registry:\n error: {:?}", error,),
    });
    // Listener for MIDI objects:
    let reg_listener_midi: Listener = listener_midi(registry_midi);

    // Get the data:
    do_roundtrip(&core_midi, mainloop_midi);

    // Deinit listener:
    std::mem::drop(reg_listener_midi);

    // Ok((midir_input_ports, midir_output_ports))
    Ok(())
}

pub fn _create_midi_ports(midi_node: pipewire::node::Node) {
    println!(" midi_node_bind: {:?}", midi_node);
    // Create MIDI port in:
    // let props_in = &pipewire::properties! {
    //     "port.name" => "pw-midimix MIDI test",
    //     "port.description" => "test",
    //     "direction" => "in",
    //     "media.type" => "application/control",
    //     // "media.class" => "Midi",
    //     "object.linger" => "1"
    // };
    // midi_node.add_port(props_in);
    // Create MIDI port out:
    // let props_out = &pipewire::properties! {
    //     "port.name" => "pw-midimix MIDI test",
    //     "port.description" => "test",
    //     "direction" => "out",
    //     "media.type" => "application/control",
    //     // "media.class" => "Midi",
    //     "object.linger" => "1"
    // };
    // midi_node.add_port(props_out);
}
