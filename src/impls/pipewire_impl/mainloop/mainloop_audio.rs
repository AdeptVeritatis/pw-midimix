
use crate::{
    app::{
        options::AppOptions,
        print::{
            PrintState,
            PrintTab,
        },
    },
    impls::pipewire_impl::{
        utils::roundtrip::do_roundtrip,
        mainloop::listener_audio::{
            listener_audio,
            listener_audio_removed,
        },
        objects::{
            device::AudioDevice,
            node::{
                sink::AudioSink,
                stream::AudioStream,
            },
            ObjectClass,
        },
    },
    hardware::{
        button::{
            ButtonActive,
            button_pressed,
            button_released,
        },
        led::{
            LED,
            LedState,
            switch_all_leds,
        },
        mapping::Mapping,
        midi::{
            MessageClass,
            MidiMessage,
        },
        slider::slider_moved,
        MixerMapName,
    },
};
use midir::MidiOutputConnection;
use pipewire::{
    channel::AttachedReceiver,
    registry::{
        Listener,
        Registry,
    },
    context::Context,
    core::Core,
    main_loop::MainLoop,
};
use std::{
    cell::RefCell,
    collections::HashMap,
    rc::Rc,
};

// ----------------------------------------------------------------------------

#[derive(Clone)]
pub struct MainLoopAudio {
    // Select main audio device.
    pub audio_device_main: Rc<RefCell<usize>>,
    // Available audio devices (main volume).
    pub audio_devices: Rc<RefCell<Vec<AudioDevice>>>,
    // Available audio sinks (change output).
    pub audio_sinks: Rc<RefCell<Vec<AudioSink>>>,
    // Audio streams (channel volume & co).
    pub audio_streams: Rc<RefCell<HashMap<u8, AudioStream>>>,
    // First button of pressing two active buttons.
    pub button_active: Rc<RefCell<ButtonActive>>,
    // pub button_active: Rc<RefCell<Option<ButtonActive>>>,
    // Remove objects from audio_streams without need to loop.
    pub map_id_to_channel: Rc<RefCell<HashMap<u32, u8>>>,
    // Remove objects, store type when added.
    pub map_id_to_class: Rc<RefCell<HashMap<u32, ObjectClass>>>,
    // Midir output to send to midi device.
    pub midir_out: Rc<RefCell<MidiOutputConnection>>,
    // Printing and formatting.
    pub next_line: Rc<RefCell<PrintState>>,
}

impl MainLoopAudio {
    pub fn new(
        midir_out: MidiOutputConnection,
    ) -> Self {
        // Prepare values:
        let audio_device_main: usize = 0;
        let button_active: ButtonActive = ButtonActive::default();
        // let button_active: Option<ButtonActive> = None;
        let next_line: PrintState = PrintState::Off;

        Self {
            audio_device_main: Rc::new(
                RefCell::new(
                    audio_device_main
                )
            ),
            audio_devices: Rc::new(
                RefCell::new(
                    Vec::new()
                )
            ),
            audio_sinks: Rc::new(
                RefCell::new(
                    Vec::new()
                )
            ),
            audio_streams: Rc::new(
                RefCell::new(
                    HashMap::new()
                )
            ),
            button_active: Rc::new(
                RefCell::new(
                    button_active
                )
            ),
            map_id_to_channel: Rc::new(
                RefCell::new(
                    HashMap::new()
                )
            ),
            map_id_to_class: Rc::new(
                RefCell::new(
                    HashMap::new()
                )
            ),
            midir_out: Rc::new(
                RefCell::new(
                    midir_out
                )
            ),
            next_line: Rc::new(
                RefCell::new(
                    next_line
                )
            ),
        }
    }

    pub fn run(
        &self,
        app_options: &AppOptions,
        mixer_map_name: MixerMapName,
        print_tab: PrintTab,
        receiver: pipewire::channel::Receiver<MidiMessage>,
    // ) -> Result<(), Box<dyn std::error::Error>> {
    ) {
        // Mapping:
        let mapping: Mapping = Mapping::new(
            app_options,
            mixer_map_name,
        );

        // Greeter:
        LED::greeter(
            app_options,
            self
                .midir_out
                .clone(),
            &mapping
                .mixer_map
        );

        // Create mainloop for global objects:
        let pw_mainloop_audio: MainLoop = match
            MainLoop::new(
                None
            )
        {
            Err(error) => panic!("\n!! problem creating pipewire mainloop:\n   error: {error:?}"),
            Ok(mainloop) => mainloop,
        };
        let pw_context: Context = match
            Context::new(
                &pw_mainloop_audio
        ) {
            Err(error) => panic!("\n!! problem creating pipewire context:\n   error: {error:?}"),
            Ok(context) => context,
        };
        let pw_core: Rc<Core> = Rc::new(
            match
                pw_context
                    .connect(
                        None
                    )
            {
                Err(error) => panic!("\n!! problem connecting to pipewire core:\n   error: {error:?}"),
                Ok(core) => core,
            }
        );
        let pw_registry: Rc<Registry> = Rc::new(
            match
                pw_core
                    .get_registry()
            {
                Err(error) => panic!("\n!! problem getting registry:\n   error: {error:?}"),
                Ok(registry) => registry,
            }
        );

        // Global object added:
        let pw_listener_audio: Listener = listener_audio(
            self
                .to_owned(),
            mapping
                .clone(),
            app_options
                .leds_off,
            app_options
                .title_off,
            print_tab
                .clone(),
            pw_registry
                .clone(),
        );

        // Global object removed:
        let pw_listener_audio_removed: Listener = listener_audio_removed(
            self
                .to_owned(),
            mapping
                .clone(),
            app_options
                .leds_off,
            pw_registry
                .clone(),
        );

        do_roundtrip(
            &pw_core,
            pw_mainloop_audio
                .clone(),
        );

        // Receive data & process it:
        let _pw_receiver: AttachedReceiver<MidiMessage> = receiver
            .attach(
                &pw_mainloop_audio
                    .loop_(),
                {
                    let app_options_clone = app_options
                        .clone();
                    let pw_core_clone: Rc<Core> = pw_core
                        .clone();
                    let pw_mainloop_audio_clone: MainLoop = pw_mainloop_audio
                        .clone();
                    let mainloop_audio_clone: Self = self
                        .clone();

                    move |message| {
                        // println!{" pw_thread: stamp: {:?} ch: {:?} class: {:?} val: {:?}", message.get_stamp(), message.get_channel(), message.get_class(), message.get_value()};
                        match
                            message
                                .class
                        {
                            MessageClass::Slider => {
                                slider_moved(
                                    mainloop_audio_clone
                                        .clone(),
                                    mapping
                                        .clone(),
                                    message,
                                    print_tab
                                        .clone(),
                                );
                            }
                            MessageClass::Button => {
                                button_pressed(
                                    &pw_core_clone,
                                    mainloop_audio_clone
                                        .clone(),
                                    mapping
                                        .clone(),
                                    message,
                                    app_options_clone
                                        .active_method,
                                    app_options_clone
                                        .leds_off,
                                    print_tab
                                        .clone(),
                                    pw_registry
                                        .clone(),
                                );
                            }
                            MessageClass::Release => {
                                button_released(
                                    mainloop_audio_clone
                                        .button_active
                                        .clone(),
                                    mapping
                                        .channel_maps
                                        .clone(),
                                    message,
                                );
                            }
                            MessageClass::Quit => {
                                println!("\n.. quitting");
                                // Switch off all leds:
                                switch_all_leds(
                                    LedState::Off,
                                    mainloop_audio_clone
                                        .midir_out
                                        .clone(),
                                    &mapping
                                        .mixer_map,
                                );
                                // midir_out.close();
                                // Quit mainloop:
                                pw_mainloop_audio_clone
                                    .quit();
                            }
                            MessageClass::Other => {
                                // println!(" button release or other stuff");
                            }
                        };
                    }
                }
            );

        pw_mainloop_audio
            .run();

        // Destroy all objects:
        // println!(" quitting pipewire thread");
        std::mem::drop(
            pw_listener_audio_removed
        );
        std::mem::drop(
            pw_listener_audio
        );

        // core.destroy_object(midi_link_in).expect("destroy object failed");
        // Rc::clone(&midir_out_rc).borrow().close();
        // midir_out.close();

        do_roundtrip(
            &pw_core,
            pw_mainloop_audio
                .clone()
        );

        // Deinit everything: ?
    }

}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
