pub mod device;
pub mod link;
pub mod node;
pub mod port;

// ----------------------------------------------------------------------------

// ObjectClass:
#[derive(Debug, Clone)]
pub enum ObjectClass {
    AudioPortIn,
    AudioPortOut,
    Device,
    Link,
    Sink,
    Stream,
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
