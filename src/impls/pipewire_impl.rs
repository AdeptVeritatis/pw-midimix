pub mod mainloop;
pub mod objects;
pub mod strings;
pub mod utils;

use crate::hardware::midi::MidiMessage;
// use pipewire as pw;
use pipewire::channel::{
    channel,
    Receiver,
    Sender,
};

// ----------------------------------------------------------------------------

pub struct PipeWireChannels { // name ???
    pub receiver: Receiver<MidiMessage>,
    pub sender: Sender<MidiMessage>,
}

impl PipeWireChannels {
    pub fn new(
    ) -> Self {
        // Create pw::channel for the threads to communicate:
        let (sender, receiver): (Sender<MidiMessage>, Receiver<MidiMessage>) = channel();

        Self {
            receiver,
            sender,
        }
    }
}

// ----------------------------------------------------------------------------
// Functions:
// ----------------------------------------------------------------------------
