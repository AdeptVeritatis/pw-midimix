// working version with midir
// basic features complete

// #![allow(clippy::too_many_arguments, clippy::single_match)]
// #![allow(clippy::type_complexity)]

pub mod app;
pub mod declarations;
pub mod errors;
pub mod hardware;
pub mod impls;

use crate::{
    app::{
        localization::DONE,
        App,
        AppStatus,
    },
    hardware::midi::MidiDevice,
    impls::pipewire_impl::PipeWireChannels,
};

// ----------------------------------------------------------------------------

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Prepare app and options:
    let mut app: App = App::default();
    // Set and show options:
    app
        .set_options();

    match
        app
            .state
    {
        AppStatus::Quit => (),
        AppStatus::Running => {
            // Initialize PipeWire:
            pipewire::init();

            // Prepare pipewire channels:
            let pipewire_channels: PipeWireChannels =
                PipeWireChannels::new(
                    // app,
                );

            // Connect midi:
            // check app.state again ???
            match
                MidiDevice::new(
                    &mut app,
                    &pipewire_channels,
                )
            {
                None => (),
                Some(midi_device) => {
            // Start app:
                    app
                        .run(
                            midi_device
                                .output,
                            pipewire_channels,
                        );

                    // Close midir_in connection before quitting:
                    midi_device
                        .input
                        .close();
                    // Close midir_out connection should be in audio.rs, but value is moved to button.rs where it can not be closed on button press!
                    // Cannot move out of dereference of `Ref there, anyway! ???
                    // midir_out.close();
                    // Close midi ports: necessary???
                },
            };
        },
    };

    // Quit program:
    match
        app
            .state
    {
        AppStatus::Running => {
            // Status 'running' while closing:
            println!("!! Problem quitting program.");
            Err(
                Box::new(
                    std::fmt::Error
                )
            ) //define own errors / error traits? ???
        },
        AppStatus::Quit => {
            // Close pw_sender and pw_receiver: ???
            // Deinit pipewire: ???
            // unsafe { pipewire::deinit() };
            println!(".. {DONE}");
            Ok(())
        },
    }
}
